<?php

$user = 'orimari';
$password = '';
$db = 'orimari';
$host = 'localhost';
$backup_file = 'orimari.sql';

$program = 'mysql';
$drop_sql = "DROP DATABASE IF EXISTS $db";
$create_sql = "CREATE DATABASE IF NOT EXISTS $db CHARACTER SET utf8 COLLATE utf8_general_ci";

$commands = array(
	"$program -u$user -p$password -h $host -e '$drop_sql'",
	"$program -u$user -p$password -h $host -e '$create_sql'",
	"$program -u$user -p$password -h $host $db --default-character-set=utf8 -e 'SET NAMES utf8;SET autocommit=0;SET unique_checks=0;SET foreign_key_checks=0;SOURCE $backup_file;COMMIT;'",
);

foreach ($commands as $command) {
	echo "Executing: $command\n";
	exec($command, $out)."\n\n";
	print_r($out);
}
