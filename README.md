# Orimari #

A web page built with custom CMS made for Orimari Foundation that was previously used at [http://orimari.com/](http://orimari.com/).

### Demo ###

Take a look at [demo page](http://orimari.milosz.ch/) and see [this pdf in Polish](http://orimari.milosz.ch/uploads/AdminPages-dokumentacja.pdf) for more details about the web page.

### How do I get set up? ###

* Edit ```import_sql.php``` with database parameters
* Run ```php import_sql.php```
* Run ```cp public/files/db_data.php.dev public/files/db_data.php```
* Edit ```public/files/db_data.php``` with database parameters
* Run ```cp public/admin/db_data.php.dev public/admin/db_data.php```
* Edit ```public/admin/db_data.php``` with database parameters
* Set Apache or Nginx to serve PHP at the folder where you cloned the repository

### Administration page access ###

* got to ```/admin```
* on login provide ```administrator``` as username and password