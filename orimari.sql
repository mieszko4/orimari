/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `languageID` int(3) NOT NULL AUTO_INCREMENT,
  `language` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`languageID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `languages` (`languageID`, `language`) VALUES (1,'pl');
INSERT INTO `languages` (`languageID`, `language`) VALUES (2,'en');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mainpages` (
  `pageID` int(3) NOT NULL DEFAULT '0',
  `title` varchar(500) DEFAULT NULL,
  `titleSubMenuPosition` enum('left','center') DEFAULT 'left',
  `filename` varchar(50) DEFAULT NULL,
  `languageID` int(3) DEFAULT NULL,
  PRIMARY KEY (`pageID`),
  UNIQUE KEY `filename` (`filename`,`languageID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (1,' ','center','index.php',1);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (4,'O fundacji','left','fundacja.php',1);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (18,'Działalność Fundacji','left','dzialalnosc.php',1);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (21,'Kontakt','left','kontakt.php',1);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (91,NULL,'left','index.php',2);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (9,'Kochasz, Chronisz, Zapobiegasz','left','kampania.php',1);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (29,'Social campaign','left','kampania.php',2);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (31,'Foundation\'s activities','left','dzialalnosc.php',2);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (34,'Contact','left','kontakt.php',2);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (35,'About foundation','left','fundacja.php',2);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (63,'Partnerzy','left','partnerzy.php',1);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (77,'Partners','left','partnerzy.php',2);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (134,'Music For Life','left','musicforlife.php',1);
INSERT INTO `mainpages` (`pageID`, `title`, `titleSubMenuPosition`, `filename`, `languageID`) VALUES (160,NULL,'left','pomoglismy.php',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menupages` (
  `menuID` int(3) NOT NULL DEFAULT '0',
  `pageID` int(3) DEFAULT NULL,
  `externalLink` varchar(100) DEFAULT NULL,
  `itemName` varchar(100) DEFAULT NULL,
  `orderNumber` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menuID`,`orderNumber`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (3,21,'NULL','Kontakt',8);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (2,29,'NULL','Social campaign',3);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (2,31,'NULL','Foundation\'s activities',2);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (4,31,'NULL','Foundation\'s activities',2);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (1,63,'NULL','Partnerzy',6);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (3,63,'NULL','Partnerzy',7);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (4,29,'NULL','Social campaign',3);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (1,18,'NULL','Działalność fundacji',4);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (2,35,'NULL','About foundation',1);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (4,35,'NULL','About foundation',1);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (1,9,'NULL','Kochasz, Chronisz, Zapobiegasz',5);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (3,165,'NULL','Zbiórki publiczne',6);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (3,18,'NULL','Działalność Fundacji',4);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (2,77,'NULL','Partners',4);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (2,34,'NULL','Contact',5);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (3,9,'NULL','Kochasz, Chronisz, Zapobiegasz',5);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (4,77,'NULL','Partners',4);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (4,34,'NULL','Contact',5);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (1,134,'NULL','Music For Life',3);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (1,160,'NULL','Pomogliśmy',2);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (1,4,'NULL','O fundacji',1);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (1,21,'NULL','Kontakt',7);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (3,134,'NULL','Music For Life',3);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (3,4,'NULL','O fundacji',2);
INSERT INTO `menupages` (`menuID`, `pageID`, `externalLink`, `itemName`, `orderNumber`) VALUES (3,1,'NULL','Strona główna',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `menuID` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `languageID` int(2) DEFAULT NULL,
  PRIMARY KEY (`menuID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `menus` (`menuID`, `name`, `languageID`) VALUES (1,'mainMenu',1);
INSERT INTO `menus` (`menuID`, `name`, `languageID`) VALUES (2,'mainMenu',2);
INSERT INTO `menus` (`menuID`, `name`, `languageID`) VALUES (3,'bottomMenu',1);
INSERT INTO `menus` (`menuID`, `name`, `languageID`) VALUES (4,'bottomMenu',2);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT,
  `pageIDHolder` int(3) DEFAULT NULL,
  `textContent` text,
  `movieName` varchar(40) DEFAULT NULL,
  `galleryDir` varchar(40) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `isMenuPage` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pageID`),
  FULLTEXT KEY `textContent` (`textContent`)
) ENGINE=MyISAM AUTO_INCREMENT=161 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (1,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (4,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (9,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (18,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (21,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (29,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (31,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (34,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (35,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (63,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (77,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (91,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (134,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
INSERT INTO `pages` (`pageID`, `pageIDHolder`, `textContent`, `movieName`, `galleryDir`, `description`, `isMenuPage`) VALUES (160,NULL,'<p>Comming soon</p>',NULL,NULL,NULL,0);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagesettings` (
  `settingID` int(3) NOT NULL AUTO_INCREMENT,
  `property` varchar(200) DEFAULT NULL,
  `value` text,
  `languageID` int(3) DEFAULT NULL,
  PRIMARY KEY (`settingID`),
  UNIQUE KEY `property` (`property`,`languageID`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `pagesettings` (`settingID`, `property`, `value`, `languageID`) VALUES (1,'Description','Fundacja ORIMARI została powołana do życia w 2006 r. Jest finansowana ze środków własnych i pozyskanych od darczyńców. Fundacja prowadzi działalność w zakresie szeroko rozumianej opieki nad dziećmi, w szczególności dziećmi dotkniętymi poważnymi chorobami.',1);
INSERT INTO `pagesettings` (`settingID`, `property`, `value`, `languageID`) VALUES (2,'Keywords','orimari, fundacja, Dom Spełnionych Marzeń, kampania społeczna, onko-olimpiada, marzenna szustkowska, dom dziecka, \'kochasz, chronisz, zapobiegasz\', \'Marzenna Szustkowska\'',1);
INSERT INTO `pagesettings` (`settingID`, `property`, `value`, `languageID`) VALUES (3,'Description','Non-profit ORIMARI Foundation was established in 2006. It is financed out of its own means and those obtained from the donors. According to statute assumptions, the Foundation maintains a widely-understood care of children, in particular those affected by neoplasm diseases.',2);
INSERT INTO `pagesettings` (`settingID`, `property`, `value`, `languageID`) VALUES (4,'Keywords','orimari, foundation, Home of Fulfilled Dreams, social campaign, onco-olympics, children\'s home, \'you love, you protect, you prevent\', \'Marzenna Szustkowska\'',2);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `questionID` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `surname` varchar(30) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `question` text,
  `answer` text,
  `datetimeQ` datetime DEFAULT NULL,
  `datetimeA` datetime DEFAULT NULL,
  `showOnPage` tinyint(1) DEFAULT NULL,
  `authorA` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`questionID`),
  FULLTEXT KEY `answer` (`answer`)
) ENGINE=MyISAM AUTO_INCREMENT=224 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `questions` (`questionID`, `name`, `surname`, `mail`, `question`, `answer`, `datetimeQ`, `datetimeA`, `showOnPage`, `authorA`) VALUES (223,'Miłosz','Chmura','mieszko@gmail.com','What time is it?','It depends','2014-09-17 01:26:43','2014-09-17 01:26:45',1,'administrator');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submenupages` (
  `pageIDHolder` int(3) NOT NULL DEFAULT '0',
  `pageIDLink` int(3) DEFAULT NULL,
  `externalLink` varchar(100) DEFAULT NULL,
  `itemName` varchar(100) DEFAULT NULL,
  `orderNumber` int(3) NOT NULL DEFAULT '0',
  `title` varchar(70) DEFAULT NULL,
  `summary` varchar(700) DEFAULT NULL,
  `uploadedPicture` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`pageIDHolder`,`orderNumber`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersadmin` (
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(32) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `usersadmin` (`username`, `password`, `surname`, `name`) VALUES ('administrator','200ceb26807d6bf99fd6f4f0d1ca54d4','Chmura','Miłosz');
