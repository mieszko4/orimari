<?php
//Info-Main: Part of Orimari.com Must be stored in $serverName/ folder (2008.09.30:mieszko4GmailCom)
//script finds the most close language and redirects it to: >>>$language/index.php<<< through Location header
$availableLanguages = array("en","pl");
$defaultLanguage = "en";

$language=$defaultLanguage;
if( isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]) )
{
	$acceptLanguages = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
	$acceptLanguages = str_replace(' ','',$acceptLanguages);
	$acceptLanguages = explode(",", $acceptLanguages);
	
	foreach ($acceptLanguages as $languageAc)
	{
		$languageAc=substr($languageAc,0,2); //get only major
		
		foreach($availableLanguages as $languageAv)
		{
			if($languageAv==$languageAc)
			{
				$language=$languageAc;
				break(2);
			}
		}
	}
}

#REDIRECT RIGHT AWAY!
#http_redirect("$language/index.php");
#exit;

//get all settings
include("files/db_data.php");
if($connection = @mysql_connect($server,$username,$password))
{
	if($db_select= @mysql_select_db($database))
	{
		mysql_query("SET CHARACTER SET 'utf8'");
		
		//description and keywords
		$query="SELECT property,value FROM pagesettings NATURAL JOIN languages WHERE language='$language' LIMIT 2";
		$settings=@mysql_query($query);
		$settingsArray=array();
		while($row=@mysql_fetch_array($settings,MYSQL_ASSOC))
		{
			$settingsArray[$row["property"]]=$row["value"];
		}
	}
}

if($language=="pl")
{
	$slogan = "Tylko życie poświęcone innym jest warte przeżycia.";
	$sloganImg = "files/orimariSloganPl.png";
}
else //default
{
	$slogan = "Only a life lived for others is a life worthwhile.";
	$sloganImg = "files/orimariSloganEn.png";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta http-equiv="Refresh" content="4; URL=<?php echo "$language/index.php"; ?>"/>

<meta name="Description" content="<?php echo htmlspecialchars($settingsArray["Description"]); ?>"/>
<meta name="Keywords" content="<?php echo htmlspecialchars($settingsArray["Keywords"]); ?>"/>
	
<title>Orimari</title>

<link rel="shortcut icon" href="files/orimari.ico"/>

<style type="text/css">
body
{
	background-color:#ffffff;
	text-align:center;
	color:#88888A;
	font-family:Arial;
	font-size:16px;
	
	margin:0px;
}
	img{border-style:none}
</style>
</head>

<body>
<div style="background-color:#F8F8F8;border-bottom:1px #EC9C4C solid;padding-top:100px;padding-bottom:50px">
	<a href="<?php echo $language; ?>/index.php" onmouseover="window.location=this.href"><img src="files/orimariMainLogo.png" alt="ORIMARI"/></a>
</div>
<div>
	<p style="margin-top:15px;margin-bottom:0px"><img src="<?php echo $sloganImg; ?>" alt="<?php echo $slogan; ?>"/>
		<img style="position:fixed;margin-top:-200px;" src="files/orimariMainMaskotka.png" alt=""/>
	</p>
	<p style="margin-top:10px"><img src="files/orimariAlbertEinstein.png" alt="Albert Einstein"/></p>
</div>
</body>

<script type="text/javascript">
//	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
//	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
//	var pageTracker = _gat._getTracker("UA-4569524-1");
//	pageTracker._initData();
//	pageTracker._trackPageview();
</script>

</html>
