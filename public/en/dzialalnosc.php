<?php //Info-Main: Part of Orimari.com Generated from template stored in $serverName/files/template.php (2008.09.25:mieszko4GmailCom) ?>
<?php include("../files/header.php"); ?>

<?php //GET DATA
	$pageData=array();
	
	//get the mainpage: title, titleSubMenuPosition
	$query="SELECT pageID,title,titleSubMenuPosition,languageID FROM mainpages NATURAL JOIN languages WHERE language='$language' AND filename='$filename' LIMIT 1";
	$page=@mysql_query($query);
	while($row=@mysql_fetch_array($page,MYSQL_ASSOC))
	{
		foreach($row as $key=>$value)
		{
			$pageData[$key]=$value;
		}
	}
	
	//subMenu
	$query="SELECT pageIDLink AS pageID, pageIDHolder, externalLink, itemName, orderNumber,title, summary,uploadedPicture FROM submenupages WHERE pageIDHolder={$pageData["pageID"]} ORDER BY orderNumber";
	$subMenu=@mysql_query($query);
	
	//subMenu2 (this is a COPY)
	$query="SELECT pageIDLink AS pageID, pageIDHolder, externalLink, itemName, orderNumber,title, summary,uploadedPicture FROM submenupages WHERE pageIDHolder={$pageData["pageID"]} ORDER BY orderNumber";
	$subMenu2=@mysql_query($query);
	
	//change subpage if it is in the submenu, else ignore $id
	$query="SELECT pageIDLink AS pageID FROM submenupages WHERE pageIDHolder={$pageData["pageID"]} AND orderNumber=$id ORDER BY orderNumber LIMIT 1";
	$page=@mysql_query($query);
	if($row=@mysql_fetch_array($page,MYSQL_ASSOC))
	{
		$pageData["pageID"]=$row["pageID"];
	}
	//change subsubpage if it is in the subsubmenu, else ignore $subid
	$query="SELECT pageIDLink AS pageID FROM submenupages WHERE pageIDHolder={$pageData["pageID"]} AND orderNumber=$subid ORDER BY orderNumber LIMIT 1";
	$page=@mysql_query($query);
	if($row=@mysql_fetch_array($page,MYSQL_ASSOC))
	{
		$pageData["pageID"]=$row["pageID"];
	}
	
	//get the page: textContent...
	$query="SELECT textContent,movieName,galleryDir,isMenuPage FROM pages WHERE pageID={$pageData["pageID"]} LIMIT 1";
	$page=@mysql_query($query);
	
	while($row=@mysql_fetch_array($page,MYSQL_ASSOC))
	{
		foreach($row as $key=>$value)
		{
			$pageData[$key]=$value;
		}
	}
	
?>

<?php //TITLE
if($pageData["title"])
{
	if($pageData["titleSubMenuPosition"]=="center")
	{
		echo '<h1 style="text-align:center">'.strtr(htmlspecialchars($pageData["title"]),array("\n"=>"<br/>","\r"=>"")).'</h1>'."\n";
	}
	else
	{
		echo '<table class="mainTitle"><tr><td><img src="../files/verTitleLine.png" alt=""/></td><td class="separator"></td><td><h1>'.htmlspecialchars(strtr($pageData["title"],array("\n"=>" ","\r"=>""))).'</h1></td></tr></table>'."\n";
	}
}
?>

<?php //SUBMENI
	showSubMenu($subMenu,$filename,$id,$subid,($pageData["titleSubMenuPosition"]=="center"));
?>

<?php //TEXTCONTENT
if(!$pageData["isMenuPage"])
{
	if($pageData["textContent"])
	{
		echo '<div class="textContent">'."\n";
		echo $pageData["textContent"]; //no htmlspecialchars!
		echo '</div>'."\n";
	}
}
else //menuPage
{
	echo '<div class="textContent"><table class="tabsTable">'."\n";
	
	$isEven=true;
	while($row=@mysql_fetch_array($subMenu2,MYSQL_ASSOC))
	{
		$itemPath=$row["pageID"]?"$filename?id={$row["orderNumber"]}":$row["externalLink"]; //choose filename?id=x or external link
		$title=$row["title"];
		$summary=$row["summary"];
		$uploadedPicture=$row["uploadedPicture"];
		$uploadedPicture=$uploadedPicture?$uploadedPicture:'noPicture.jpg';
		$imgSrc=$isEven?'ver120Dark.png':'ver120.png';
		
		echo $isEven?'<tr style="background:#ffffff">':'<tr>';
		
		echo '
		<td style="width:300px"><img style="padding-left:20px;padding-right:6px" src="../files/'.$imgSrc.'" alt=""/><img src="../photos/menu/'.htmlspecialchars($uploadedPicture).'" alt=""/></td>
		<td>
			<h2>'.htmlspecialchars($title).'</h2>
			<p><a style="color:#646464" href="'.htmlspecialchars($itemPath).'">'.htmlspecialchars($summary).'</a></p>
		<p style="text-align:right"><a href="'.htmlspecialchars($itemPath).'"><img src="../files/'.(($language=="pl")?"czytajwiecej":"readmore").'.png" alt="'.(($language=="pl")?"czytaj więcej":"read more").'"/></a></p>
		</td>
		<td style="width:100px"> </td>';
		
		echo '</tr>';
		
		$isEven=!$isEven;
	}
	
	echo '</table></div>'."\n";
}
?>

<?php //MOVIE
	showFlvMovie($pageData["movieName"],$language,!empty($pageData["textContent"]));
?>

<?php //GALLERY
	showGallery($pageData["galleryDir"],$language,(!empty($pageData["textContent"]) || !empty($pageData["movieName"])) );
?>

<?php include("../files/footer.php"); ?>