<?php
//Info-Main: One of common pages. Must be stored in $serverName/$language folder (the page is in pl (polish))
//Info: This is a page on which you can ask a question which will be send to mail $reciever and stored in questions database
//Note (1): NO CHECKING IF TABLE queries EXIST. IT MUST EXIST FOR THE PAGE TO WORK
//Note (2): Save this file as UTF-8 without BOM, so it wont interfere with session check (needed for captcha)
//Last Update: 10.XII.2009.

session_start(); //session neededfor captcha
include("../files/captcha/securimage.php");

include("../files/htmlMimeMail5/htmlMimeMail5.php");

$questionLimit=15; //how many questions with answers show on one page
$reciever="orimari@localhost"; //the person who will recieve the mail
?>
<?php
//getting the FAQ from SQL
include("../files/db_data.php");

$pageNum=$_GET['page'];
$pageNum=$pageNum?$pageNum:0;
$fromNum=($pageNum*$questionLimit);

if($connection = @mysql_connect($server,$username,$password))
{
	if($db_select= @mysql_select_db($database))
	{
		mysql_query("SET CHARACTER SET 'utf8'");
		$query="SELECT question, answer FROM questions WHERE showOnPage=1 ORDER BY datetimeQ DESC";
		$questions=@mysql_query($query);
	}
}
@mysql_close($connection);
?>
<?php include("../files/header.php"); ?>
				<table class="mainTitle">
					<tr>
						<td><img src="../files/verTitleLine.png" alt=""/></td>
						<td class="separator"></td><td><h1>Zadaj pytanie</h1></td>
					</tr>
				</table>
<?php
$errorMark='class="error"';
$showForm=true;

if($_POST["submit"])
{
	$showForm=false;
	$name=$_POST["name"];
	$surname=$_POST["surname"];
	$email=$_POST["email"];
	$question=$_POST["question"];
	
	$securimage = new Securimage();
	if ($securimage->check($_POST['captcha_code']) == false)
	{
		$showForm=true;
		$codeError=true;
	}
	if(!$name || !$surname || !$email || !$question)
	{
		$showForm=true;
		$dataError=true;
	}
	
	if($dataError || $codeError)
	{
		$message='<p class="error">Proszę o wypełnienie wszystkich oznaczonych pól i o ponowne wpisanie kodu weryfikacyjnego.</p>';
	}

}
if($showForm)
{
?>
				<div class="textContent">		
					<p>Aby zadać pytanie ekspertowi, skorzystaj z formularza poniżej:</p>
					
					<form action="<?php echo $filename; ?>" method="post">
					<fieldset>
					
					<?php echo $message; ?>
						<table class="formTable">
							<tr>
								<td style="width:30%;text-align:right"><label for="name" <?php if(!$name && $dataError) echo $errorMark; ?>>Imię: </label></td>
								<td><input name="name" value="<?php echo htmlspecialchars($name); ?>"/></td>
							</tr>
							<tr>
								<td style="text-align:right"><label for="surname" <?php if(!$surname && $dataError) echo $errorMark; ?>>Nazwisko: </label></td>
								<td><input name="surname" value="<?php echo htmlspecialchars($surname); ?>"/></td>
							</tr>
							<tr>
								<td style="text-align:right"><label for="email" <?php if(!$email && $dataError) echo $errorMark; ?>>Adres e-mail: </label></td>
								<td><input name="email" value="<?php echo htmlspecialchars($email); ?>"/></td>
							</tr>
							<tr>
								<td style="text-align:right"><label for="question" <?php if(!$question && $dataError) echo $errorMark; ?>>Treść pytania: </label></td>
								<td><textarea cols="40" rows="7" name="question"><?php echo htmlspecialchars($question); ?></textarea></td>
							</tr>
							<tr>
								<td style="text-align:right"></td>
								<td><img id="captcha" src="../files/captcha/securimage_show.php" alt="CAPTCHA" style="border: 1px #aaaaaa solid"/>
								<a href="#" onclick="document.getElementById('captcha').src = '../files/captcha/securimage_show.php?' + Math.random(); return false"><img src="../files/captcha/refresh.gif" alt="odśwież"/></a></td>
							</tr>
							<tr>
								<td style="text-align:right"><label for="captcha_code" <?php if($codeError) echo $errorMark; ?>>Kod weryfikacyjny: </label></td>
								<td><input name="captcha_code" maxlength="6" value="" <?php if($dataError && !$codeError) echo 'style="background-color:#fffecc"'; ?>/></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="submit" value="Wyślij pytanie" style="color:#555555"/></td>
							</tr>
						</table>

					</fieldset>
					</form>
<?php
}
else
{
	$success=false;

	//making a query
	include("../files/db_data.php");
	if($connection = @mysql_connect($server,$username,$password))
	{
		if($db_select= @mysql_select_db($database))
		{
			mysql_query("SET CHARACTER SET 'utf8'");
			$query="INSERT INTO questions VALUES (NULL, '$name', '$surname', '$email', '$question', NULL, NOW(), NULL ,'0', NULL)";
			if(mysql_query($query))
			{
				$success=true;
			}
		}
	}
	@mysql_close($connection);
	//sending mail
	$mail = new htmlMimeMail5();
	$mail->setFrom("$name $surname <$email>");
	$mail->setReturnPath($email);
	$mail->setSubject("Wiadomość z formularza 'Zadaj pytanie'");
	
	if($success) $panelInfo="\n\nPrzejdź do panelu administracyjnego na http://www.orimari.com/admin/question.php";
	$mail->setText("Pytanie od $name $surname:\n$question$panelInfo");	

	if(@$mail->send(array($reciever)))
	{
		$success=true;
	}
	
	//if none succeeded then its a problem...
	if($success)
	{
		echo '<p>Wiadomość wysłana!</p>
			  <p>Przejdź do strony głównej <a href="http://www.orimari.com/pl/">www.orimari.com</a>.</p>';
	}
	else
	{
		echo '<p class="error">Wiadomość nie wysłana!</p>
			  <p>Spróbuj skontaktować się poprzez email <a href="mailto:orimar@localhost?subject=Wiadomość z formularza >>Zadaj pytanie<<&body='."Q: $question%0A%0A$name $surname".'">orimari@localhost</a>.</p>';
	}
}
?>
<?php
//FAQ
if($questions && mysql_num_rows($questions))
{
	echo '<hr class="horizontal"/><h2>NAJCZĘŚCIEJ ZADAWANE PYTANIA</h2><br/>';
	$rowsNum=0;
	while($row=@mysql_fetch_array($questions,MYSQL_ASSOC))
	{
		if($rowsNum>=$fromNum && $rowsNum<($fromNum+$questionLimit))
		{
			echo '<p><b><i>'.$row["question"].'</i></b></p>';
			echo '<p>'.$row["answer"].'</p><br/>';
		}
		$rowsNum++;
	}
}
//page chooser

if($rowsNum>$questionLimit)
{
	echo '<p style="text-align:center;font-size:14px">';
		$i=0;
		while($i<ceil($rowsNum/$questionLimit))
		{
			if($i!=$pageNum)
			{
				if($i!=0)
					echo ' <a href="'.$filename.'?page='.$i.'">'.($i+1).'</a> ';
				else
					echo ' <a href="'.$filename.'">'.($i+1).'</a> ';
			}	
			else
				echo ' <a style="font-size:18px">'.($i+1).'</a> ';
			
			$i++;
		}
	echo "</p>";
}

?>

				</div>

<?php include("../files/footer.php"); ?>
