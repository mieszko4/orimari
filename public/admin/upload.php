<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: On this page you may manage uploading any type of files (folder $serverName/uploads)
//Note (1): You may want to delete/change all CHMOD() according to your server options
//Note (2): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.23

include("login_check.php");

include("functions.php");
$uploadsFolder="uploads"; //folder to store uploads
$iconsFolder="icons"; //folder where file extensions icons are stored as $ext.gif

$uploadsPath="../$uploadsFolder";
if(!is_dir($uploadsPath))
{
	if(!mkdir($uploadsPath))
	{
		header("Location: index.php?message=".urlencode('<p class="error">Could not create <b>'.$uploadsFolder.'</b>!</p>'));
	}
	chmod($uploadsPath,0777);
}

$warningSize=10*1048576; //in bytes
$fileFilter=null; //array of allowed types, for all types use null
$prefix="all"; //used to mark files in tmp folder
?>
<?
	$message="";
	$uploadFile=stripslashes($_GET["uploadFile"]);
	$uploadName=pathinfo($uploadFile,PATHINFO_FILENAME);
	$uploadExt=strtolower(pathinfo($uploadFile,PATHINFO_EXTENSION));

	$picture=is_file("$uploadsPath/$uploadName.jpg");
	$tmpFile=getFromTemp($prefix);
	
	if($_GET["action"]=="download")
	{
		if(is_file("$uploadsPath/$uploadFile"))
		{
			if(in_array($uploadExt,array("php","htaccess")))
			{
				header("Content-type: text/plain");
				header('Content-Disposition: attachment; filename="'.$uploadFile.'"');
				readfile("$uploadsPath/$uploadFile");
				exit();
			}
			else
			{
				header("Location: $uploadsPath/$uploadFile");
			}
		}
		else
		{
			$action="show";
			$message.='<p class="error">Download failed! File <b>'.$uploadFile.'</b> not found.</p>';
		}
	}
	if($_GET["action"]=="edit")
	{
		$action="edit";
		
		if(is_file("$uploadsPath/$uploadFile"))
		{
			//save changes
			if($_POST["editUpload"])
			{
				$uploadFileNew=stripslashes($_POST["uploadFileNew"]);
				$uploadExtNew=strtolower(pathinfo($uploadFileNew,PATHINFO_EXTENSION));
				//ignore extension
				$uploadNameNew=($uploadExt==$uploadExtNew)?pathinfo($uploadFileNew,PATHINFO_FILENAME):$uploadFileNew;
				$uploadFileNew="$uploadNameNew.$uploadExt";
				
				if(!is_file("$uploadsPath/$uploadFileNew"))
				{
					//rename
					if($uploadName!=$uploadNameNew)
					{
						if(rename("$uploadsPath/$uploadFile","$uploadsPath/$uploadFileNew"))
						{
							$message.='<p class="success">File renamed.</p>';
							$message.='<p class="info">Refresh all pages which used the old name of this file.</p>';
							$uploadFile=$uploadFileNew;
						}
						else
						{
							$message.='<p class="error">There were problems with renaming file!</p>';
						}
					}
				}
				else
				{
					$message.='<p class="error">File <b>'.$uploadFileNew.'</b> exists already!</p>';
				}
			}
		}
		else
		{
			$action="show";
			$message.='<p class="error">File <b>'.$uploadFile.'</b> not found!</p>';
		}
	}
	else if($_GET["action"]=="delete")
	{
		$action="show";
		
		if($_GET["tmpFile"]=="yes")
		{
			if(deleteTmpFile($prefix))
			{
				$tmpFile=null;
				$message.='<p class="success">Temporary file deleted.</p>';
			}
			else
			{
				$message.='<p class="error">Could not delete temporary file! There is no possibility to upload new files until the temporary file is deleted or renamed!</p>';
			}
		}
		else if(is_file("$uploadsPath/$uploadFile"))
		{
			if(unlink("$uploadsPath/$uploadFile"))
			{
				$uploadFile=null;
				$message.='<p class="success">File deleted.</p>';
			}
			else
			{
				$message.='<p class="error">Deletion failed! Could not delete the file</p>';
			}
		}
		else
		{
			$message.='<p class="error">Deletion failed! file <b>'.$uploadFile.'</b> not found.</p>';
		}
	}
	else if($_POST["addUpload"])
	{
		$action="show";
		
		$uploadFile=stripslashes($_POST["uploadFile"]);
		
		//if there is a temp waiting
		if($tmpFile)
		{
			$upload=array("tmp_name"=>dirname($tmpFile)."/{$prefix}_".basename($tmpFile),"name"=>basename($tmpFile));
		}
		else
		{
			$upload=$_FILES["upload"];
		}
		
		if(is_uploaded_file($upload["tmp_name"]) || is_file($upload["tmp_name"]))
		{
			$uploadExt=strtolower(pathinfo($upload["name"],PATHINFO_EXTENSION));
			$uploadExtForm=strtolower(pathinfo($uploadFile,PATHINFO_EXTENSION));
			//ignore extension
			$uploadName=($uploadExt==$uploadExtForm)?pathinfo($uploadFile,PATHINFO_FILENAME):$uploadFile;
			$uploadFile=$uploadName?"$uploadName.$uploadExt":$upload["name"]; //if not given, use default
			$uploadName=pathinfo($uploadFile,PATHINFO_FILENAME); //if name changes
			
			if(!$fileFilter || in_array($uploadExt,$fileFilter))
			{
				if(!is_file("$uploadsPath/$uploadFile"))
				{
					//dont use move because of tmp file
					if(@copy($upload["tmp_name"],"$uploadsPath/$uploadFile")) 
					{
						$message.='<p class="success">File added.</p>';
						unlink($upload["tmp_name"]);
						$uploadFile=null; //dont show in form
					}
					else
					{
						$message.='<p class="error">Could not move the file to uploads folder!</p>';
					}
				}
				else
				{
					$message.='<p class="error">Name <b>'.$uploadFile.'</b> already exists! Please change the name.</p>';
					saveToTemp($upload,$prefix);
					$uploadFile="{$uploadName}_".time().".$uploadExt";
				}
			}
			else
			{
				$uploadFile=null; //dont show in form
				$message.='<p class="error">File not uploaded! Allowed file types are: '.implode(", ",$fileFilter).'!</p>';
			}		
		}
		else
		{
			$message.='<p class="error">No file uploaded!</p>';
		}
	}
	
	//preventing refresh
	if($message && !$_GET["message"])
	{
		$location="$filename?action=$action";
		$location.=$uploadFile?"&uploadFile=".urlencode($uploadFile):"";
		$location.="&message=".urlencode($message);
		header("Location: $location");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin.js"></script>

<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>File upload</h1>

	<?php 
		$message=stripslashes($_GET["message"]);
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
<?php
switch($action)
{
case "edit":
?>
	<p>On this page you are managing <b><?php echo $uploadFile;?></b>.<br/>
	File's link is: <b><?php echo "$uploadsPath/$uploadFile"; ?></b></p>
	
	<p>You may change the name of the file.<br/>
	You may also <a href="<?php echo "$filename?action=delete&uploadFile=".urlencode($uploadFile); ?>" onclick="linkConfirm('Are you sure you want to delete the file?',this.href);return false">delete</a> the file.</p>
	
	<form  name="editUpload" method="post" action="<?php echo "$filename?action=edit&uploadFile=$uploadFile";?>" enctype="multipart/form-data">
		<fieldset>
		<table>
			<tr>
				<td style="width:30%;text-align:right"><label for="uploadFileNew">Name: </label></td>
				<td><input name="uploadFileNew" type="text" size="20" value="<?php echo htmlspecialchars($uploadName); ?>"/></td>
			</tr>
		</table>
		<br/>
		<input type="submit" name="editUpload" value="Save"/> <input type="button" value="Return" onclick="document.location='<?php echo $filename; ?>'"/>
		</fieldset>
	</form>
<?php
break;
case "show":
default:
?>
	<p>On this page you may upload any file</b>.</p>
	<p>Uploaded files are located in folder: <b><?php echo "http://".$_SERVER["SERVER_NAME"]."/".$uploadsFolder."/"; ?></b>.<br/>
	<b>Note:</b> The upload limit is 20mb; please be <b>patient</b> while uploading a huge file!</p>
	<br/>
	
	<table class="display">
		<thead>
		<tr>
			<td>Filename</td><td>Information</td><td></td>
		</tr>
		</thead>
		<tbody>
			<tr class="addNew">
				<td>Add a new file:</td>
				<td>
				<form enctype="multipart/form-data" action="<?php echo $filename; ?>" method="post">
					<table>
						<tr>
							<td style="width:30%;text-align:right"><label for="uploadFile" <?php echo $tmpFile?'class="info"':""; ?>>Name: </label></td>
							<td><input name="uploadFile" type="text" size="20" value="<?php echo $uploadName?htmlspecialchars($uploadName):($tmpFile?pathinfo($tmpFile,PATHINFO_FILENAME)."_".time():""); ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="upload">File: </label></td>
							<td><?php echo $tmpFile?'<p><b>'.basename($tmpFile).'</b></p>':'<input name="upload" type="file" value="Browse..." />'; ?></td>
						</tr>
					</table>
					<br/>
					<input type="submit" name="addUpload" value="Add"/>
				</form>
				</td>
				<td>
					<?php echo $tmpFile?'<p class="info">There is a file on the server which needs to be <b>renamed</b> in order to be saved. <a href="'.$filename.'?action=delete&tmpFile=yes" onclick="linkConfirm(\'Are you sure you want to delete the temporary file?\',this.href);return false">Delete the file?</a></p>':''; ?>
					<p><b>Note: </b> Name field is the name of the file which will be created on the server. If it is left empty the original name of the file will be saved instead.</p>
				</td>
			</tr>		
<?php
//show all upload files from the folder
	if($theFiles=getFiles($uploadsPath,$fileFilter))
	{
		foreach($theFiles as $uploadFile)
		{
			$uploadName=pathinfo($uploadFile,PATHINFO_FILENAME);
			$uploadExt=pathinfo($uploadFile,PATHINFO_EXTENSION);
			
			if(is_file("$uploadsPath/$uploadFile"))
			{
				echo "<tr>\n";
				
				echo '<td><a href="'.$filename.'?action=edit&uploadFile='.urlencode($uploadFile).'"><b>'.$uploadFile.'</b><br/>[ manage ]</a></td>'."\n";
				
				$uploadSize=filesize("$uploadsPath/$uploadFile");
				
				//SizeWarning
				if($uploadSize>$warningSize)
				{
					printf("<td><p class=\"info\"><b>FileSize:</b> %.2fMB</p>\n",$uploadSize/1048576);
				}
				else
				{
					printf("<td><p><b>FileSize:</b> %.2fMB </p>\n",$uploadSize/1048576);
				}
				

				if(is_file("$iconsFolder/$uploadExt.gif"))
				{
					echo '<img src="'."$iconsFolder/$uploadExt.gif".'" alt="" style="max-height:150px;border-style:none"/>';
				}
				else if(is_file("$iconsFolder/default.gif"))//default
				{
					echo '<img src="'."$iconsFolder/default.gif".'" alt="" style="max-height:150px;border-style:none"/>';
				}
				
				echo '<p>[<a href="'."$filename?action=download&uploadFile=".urlencode($uploadFile).'"> download </a>| <a href="'.$filename.'?action=delete&uploadFile='.urlencode($uploadFile).'" onclick="linkConfirm(\'Are you sure you want to delete the file?\',this.href);return false"> delete </a>]</p></td>'."\n";
				echo "<td></td>\n";
				echo "</tr>\n";
			}
		}	
	}
?>	
		</tbody>
	</table>
<?php
break;
}
?>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]; ?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">UploadsPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>