<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder (2008.09.25:mieszko4GmailCom)
$filename=basename($_SERVER["SCRIPT_NAME"]);
?>
<?php
session_start();

if(empty($_SESSION["usernameAdmin"]))
{
	header("Location: index.php?redirect=$filename");
}
?>