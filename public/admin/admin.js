//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a set of common functions used in AdminPages
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.25
//UpdateDate: 2010.07.26 (menuPage update)

function uploadFile()
{
	var uploadedPicture=document.getElementById("uploadedPicture").src;
	uploadedPicture=uploadedPicture.split('/');
	uploadedPicture=uploadedPicture[uploadedPicture.length-1];
	uploadedPicture=(uploadedPicture=='noPicture.jpg')?'NULL':uploadedPicture;
	
	var _target=document.editPage.target;
	var _enctype=document.editPage.enctype;
	var _action=document.editPage.action;
	
	document.editPage.target='hiddenPictureFrame';
	document.editPage.enctype='multipart/form-data';
	document.editPage.action='uploadMenuPicture.php?src='+uploadedPicture;
	
	//block here
	isBlock=true;
	
	document.editPage.submit();
	document.getElementById("uploadedPicture").src = '../photos/menu/ajax-loader.gif';
	document.editPage.menuPicture.value="";
	
	document.editPage.target=_target;
	document.editPage.enctype=_enctype;
	document.editPage.action=_action;
}


//used for menuPage
function updateMenuPageInfo(isMenuPage,editObj)
{
	var menuPageDiv = document.getElementById('menuPageId');
	var textContentDiv = document.getElementById('textContentDiv');
	if(isMenuPage==1)
	{
		menuPageDiv.style['visibility']='collapse';
		textContentDiv.style['visibility']='visible';
		return 0;
	}
	else
	{
		menuPageDiv.style['visibility']='inherit';
		textContentDiv.style['visibility']='collapse';
		return 1;
	}
}


function linkConfirm(msg,link)
{
	if(confirm(msg))
	{
		document.location=link;
	}
}

//selectObj is the <select></select>
function moveUp(selectObj,indexEdited,i)
{
	if(typeof(isBlock)!='undefined' && isBlock) return
	
	var index=selectObj.selectedIndex;
	if(index!=-1 && index>0)
	{
		var tmpName=selectObj[index].text;
		var tmpValue=selectObj[index].value;
		var tmpColor=selectObj[index].style.color;
		
		selectObj[index].text=selectObj[index-1].text;
		selectObj[index].value=selectObj[index-1].value;
		selectObj[index].style.color=selectObj[index-1].style.color;
		
		selectObj[index-1].text=tmpName;
		selectObj[index-1].value=tmpValue;
		selectObj[index-1].style.color=tmpColor;
		
		selectObj.selectedIndex=index-1;
		if(indexEdited) indexEdited[i]=selectObj.selectedIndex;
	}
}
function moveDown(selectObj,indexEdited,i)
{	
	if(typeof(isBlock)!='undefined' && isBlock) return

	var index=selectObj.selectedIndex;
	if(index!=-1 && index<selectObj.length-1)
	{
		var tmpName=selectObj[index].text;
		var tmpValue=selectObj[index].value;
		var tmpColor=selectObj[index].style.color;
		
		selectObj[index].text=selectObj[index+1].text;
		selectObj[index].value=selectObj[index+1].value;
		selectObj[index].style.color=selectObj[index+1].style.color;

		selectObj[index+1].text=tmpName;
		selectObj[index+1].value=tmpValue;
		selectObj[index+1].style.color=tmpColor;
		
		selectObj.selectedIndex=index+1;
		if(indexEdited) indexEdited[i]=selectObj.selectedIndex;
	}
}

//FUNCTIONS FOR MENUS
function removeItem(selectObj,editObj,indexEdited,i)
{
	if(isBlock) return;
	
	var index=selectObj.selectedIndex;
	if(index!=-1)
	{
		selectObj.remove(index);
		selectObj.size=(selectObj.length>7)?selectObj.length:7;
		selectObj.selectedIndex=(index==selectObj.length)?(index-1):index;
		indexEdited[i]=-1;
	}
}

function editItem(selectObj,editObj,indexEdited,i,isMenuPage)
{
	if(isBlock) return;
	
	saveItem(selectObj,editObj,indexEdited,i,isMenuPage);
	
	if(selectObj.length==0)
	{
		editObj.style.visibility='hidden';
		return;
	}
	
	var index=selectObj.selectedIndex;
	indexEdited[i]=index;
	
	var name=editObj.getElementsByTagName("input")[0];
	var page=editObj.getElementsByTagName("select")[0];
	var externalLink=editObj.getElementsByTagName("input")[1];
	
	var title=editObj.getElementsByTagName("input")[2];
	var summary=editObj.getElementsByTagName("textarea")[0];
	var uploadedPicture=document.getElementById("uploadedPicture");
	
	var messageDiv=editObj.getElementsByTagName("div")[0];
	
	//fill the data
	name.value=selectObj[index].text;
	
	var valueData=selectObj[index].value.split('\t');
	
	if(valueData[0].substring(0,1)!="L") //page
	{
		//if value not chosen
		if(!valueData[0] || valueData[0]==' ')
		{
			page.selectedIndex=0; //first index
		}
		else
		{
			for(var i=0;i<page.length;i++) //find the index with value=valueData[0]
			{
				if(page[i].value==valueData[0])
				{
					page.selectedIndex=i;
					break;
				}
			}
		}
		externalLink.disabled='disabled';
		externalLink.value='';
	}
	else
	{
		page.selectedIndex=page.length-1; //the last is the option  (948859)
		externalLink.disabled='';
		externalLink.value=valueData[0].substring(1); //Skip L
	}
	
	//fill the title and the summary and uploadedPicture
	if(valueData.length==4)
	{
		title.value=valueData[1];
		summary.value=valueData[2];
		uploadedPicture.src=(valueData[3]=='NULL')?'../photos/menu/noPicture.jpg':'../photos/menu/'+valueData[3];
	}
	else
	{
		title.value='';
		summary.value='';
		uploadedPicture.src='../photos/menu/noPicture.jpg';
	}
	

	messageDiv.innerHTML='';
	editObj.style.visibility='visible';
	name.focus();			
	name.select();
}


//add an Item after the selected one, or to the end
function addItem(selectObj,editObj,indexEdited,i,isMenuPage)
{
	if(isBlock) return;
	
	var optionElement=document.createElement('option');
	
	var index=selectObj.selectedIndex;
	if(index==-1 || (index+1)==selectObj.length) //append when no select or selected last
	{
		selectObj.appendChild(optionElement);
		selectObj.selectedIndex=selectObj.length-1;
	}
	else //add after selected one
	{
		//ff and ie difference
		document.all?selectObj.add(optionElement,index+1):selectObj.add(optionElement,selectObj[index+1]);
		selectObj.selectedIndex=index+1;
	}
	//fill with info
	selectObj[selectObj.selectedIndex].text='New menu Item';
	selectObj[selectObj.selectedIndex].value='';
	
	selectObj.size=(selectObj.length>7)?selectObj.length:7;
	
	editItem(selectObj,editObj,indexEdited,i,isMenuPage);
}

//disables/enables the externalLink box according to the state of select
function showHideExternal(editDiv)
{
	var externalObj=editDiv.getElementsByTagName('input')[1];
	var selectObj=editDiv.getElementsByTagName('select')[0];
	if(selectObj.selectedIndex==(selectObj.length-1))
	{
		externalObj.disabled='';
	}
	else
	{
		externalObj.disabled='disabled';
	}
}

//saves the item into the selectList
function saveItem(selectObj,editObj,indexEdited,i,isMenuPage)
{
	if(indexEdited[i]==-1) return true;

	var name=editObj.getElementsByTagName("input")[0];
	var page=editObj.getElementsByTagName("select")[0];
	var externalLink=editObj.getElementsByTagName("input")[1];
	
	var title=editObj.getElementsByTagName("input")[2];
	var summary=editObj.getElementsByTagName("textarea")[0];
	var uploadedPicture=document.getElementById("uploadedPicture").src;
	uploadedPicture=uploadedPicture.split('/');
	uploadedPicture=uploadedPicture[uploadedPicture.length-1];
	uploadedPicture=(uploadedPicture=='noPicture.jpg')?'NULL':uploadedPicture;
	
	title.value=title.value.replace(/\t/g,'    ');
	summary.value=summary.value.replace(/\t/g,'    ');
	
	summary.value=summary.value?summary.value:' ';
	title.value=title.value?title.value:' ';
	
	var messageDiv=editObj.getElementsByTagName("div")[0];
	
	var index=(indexEdited[i]==-1 || indexEdited[i]>(selectObj.length-1))?selectObj.selectedIndex:indexEdited[i];
	
	selectObj[index].text=name.value;
	if( name.value && page.selectedIndex!=0 )
	{	
		if(page[page.selectedIndex].value)//if Not external
		{
			externalLink.value='';
			selectObj[index].value=page[page.selectedIndex].value;
		}
		else
		{
			selectObj[index].value='L'+externalLink.value; //to recognize externalLink
		}
		selectObj[index].value+='\t'+title.value+'\t'+summary.value+'\t'+uploadedPicture;
		if((title.value && summary.value && isMenuPage==1) || !isMenuPage) //without uploadPicture check!
		{
			selectObj[index].style.color='';
			return true;
		}
		else
		{
			selectObj[index].style.color='red';
			return false;
		}
	}
	else
	{
		selectObj[index].value=' \t'+title.value+'\t'+summary.value+'\t'+uploadedPicture; //not chosen still
		selectObj[index].style.color='red';
		return false;
	}
}
function saveItems(selectObj,editObj,itemOrder,indexEdited,i,isMenuPage)
{
	saveItem(selectObj,editObj,indexEdited,i,isMenuPage);

	itemOrder.value="";
	for(i=0;i<selectObj.length;i++)
	{
		itemOrder.value+=selectObj[i].text.replace(/\t/g,'    ')+"\t"+selectObj[i].value;
		itemOrder.value+=(i!=(selectObj.length-1))?"\t\t":"";
	}
}

//FUNCTIONS FOR MENUS (END)

function saveOrder(selectObj,itemOrder,separator)
{
	itemOrder.value="";
	separator=separator?separator:'-';
	for(i=0;i<selectObj.length-1;i++)
	{
		itemOrder.value+=selectObj[i].value+separator;
	}
	itemOrder.value+=selectObj[i].value;
}

function insertAtCursor(myField, myValue)
{
	if (document.selection)
	{
		myField.focus();
		sel = document.selection.createRange();
		sel.text = myValue;
		
	}
	else if (myField.selectionStart || myField.selectionStart == '0')
	{
		myField.focus();
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos) + myValue + myField.value.substring(endPos, myField.value.length);
		myField.setSelectionRange(endPos+myValue.length, endPos+myValue.length);
	}
	else
	{
		myField.value += myValue;
	}
}

