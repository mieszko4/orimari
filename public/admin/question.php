<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a page for AdminPages on which you can manage questions asked on the webpage in $serverName/pl/pytanie.php
//Note (1): NO CHECKING IF TABLE >>>questions<<< EXIST. IT MUST EXIST FOR THE PAGE TO WORK
//Note (2): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.25

include("login_check.php");

include("functions.php");
$separator="-";
$orderBy="ORDER BY ID, 'asked on'";
?>
<?php
	include("db_data.php");
	if($connection=@mysql_connect($server,$username,$password))
	{
		$db_select= @mysql_select_db($database);
		mysql_query("SET CHARACTER SET 'utf8'");
		if($db_select)
		{		
			$id=$_GET["id"];
			$action="show"; //default action
			$filter=$_GET["filter"];
			
			if($_GET["action"]=="edit")
			{
				$action="edit";
				
				//make changes if any
				if($_POST["editQuestion"])
				{
					//dont use stripslashes
					$question=$_POST["question"];
					$answer=$_POST["answer"];
					$showOnPage=$_POST["showOnPage"];
					
					if($question)
					{
						$showOnPage=$showOnPage?1:0;
						$answer=$answer?"'$answer'":'NULL';
						
						$query="UPDATE questions SET question='$question', answer=$answer, showOnPage=$showOnPage,datetimeA=NOW(),authorA='{$_SESSION["usernameAdmin"]}' WHERE questionID=$id LIMIT 1";
						
						if(@mysql_query($query))
						{
							$message.='<p class="success">Entry successfully saved!</p>';
						}
						else
						{
							$message.='<p class="error">Entry could not be saved!</p>';
						}
					}
					else
					{
						$message.='<p class="error">It does not make sense for the question to be empty. Use delete function instead.</p>';
					}
				}	
				
				
				//display
				$query="SELECT name,surname,question,answer,showOnPage,datetimeQ,datetimeA,authorA FROM questions WHERE questionID=$id LIMIT 1";
					if($question=@mysql_query($query))
					{
						if(!($questionData = @mysql_fetch_array($question,MYSQL_ASSOC)))
						{
							$message.='<p class="error">Entry with this id does not exist!</p>';
						}
					}
					else
					{
						$message.='<p class="error">Database error (3). Try again later.</p>';
					}
			}
			else if($_GET["action"]=="delete")
			{
					$action="show";
					$query = "DELETE FROM questions WHERE questionID=$id LIMIT 1";
					if(mysql_query($query))
					{
						$message.='<p class="success">Deletion succesfull.</p>';
					}
					else
					{
						$message.='<p class="error">Could not delete id='.$id.'!</p>';
					}
			}
			else if($_POST["changeOrder"])
			{
				$itemOrder=stripslashes($_POST["itemOrder"]);
				$itemOrder=split($separator,$itemOrder);
				$originalOrder=stripslashes($_POST["originalOrder"]);
				$originalOrder=split($separator,$originalOrder);
				
				if(count($originalOrder)==count($itemOrder))
				{
					$errors=0;
					//first change to negative
					foreach($originalOrder as $id)
					{
						$query="UPDATE questions SET questionID=-$id WHERE questionID=$id LIMIT 1";
						@mysql_query($query);
						if(mysql_affected_rows()!=1) {$errors++; break;}
					}
					
					if(!$errors)
					{
						$counter=0;
						//change to new order
						foreach($originalOrder as $id)
						{
							$query="UPDATE questions SET questionID=$id WHERE questionID=-{$itemOrder[$counter]} LIMIT 1";
							@mysql_query($query);
							if(mysql_affected_rows()!=1) {$errors++; break;}
							$counter++;
						}
						
						if(!$errors)
						{
							$message.='<p class="success">Reordering successful.</p>';
						}
						else
						{
							$message.='<p class="error">There were some errors ('.$errors.') while reordering!</p>';
						}
					}
					else
					{
						$message.='<p class="error">There were some errors ('.$errors.') while reordering!</p>';
					}
				}
			}
			
			//preventing refresh
			if($message && !$_GET["message"])
			{
				$location="$filename?action=$action";
				$location.=$filter?"&filter=$filter":"";
				$location.=$id?"&id=".urlencode($id):"";
				$location.="&message=".urlencode($message);
				header("Location: $location");
			}
			
			//if is show (must be after refreh prevention
			if($action=="show")
			{
				if($filter=="notAnswered") $where="WHERE answer IS NULL";
				if($filter=="notDisplayed") $where="WHERE showOnPage=0";
				if($filter=="displayed") $where="WHERE showOnPage=1";
				
				$query="SELECT questionID as ID,name,surname,mail,question,answer,datetimeQ AS 'asked on' FROM questions $where $orderBy";
				if(!($questions= @mysql_query($query)))
				{
					$message.='<p class="error">Database error (3). Try again later.</p>';
				}
			}
		}
		else
		{
			$message.='<p class="error">Database error (2). Try again later.</p>';
		}
	}
	else
	{
		$message.='<p class="error">Database error (1). Try again later.</p>';
	}
	@mysql_close($connection);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin.js"></script>
<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Questions from "Zadaj pytanie"</h1>

	<?php
		//ako ima bilo koja poruka
		$message=$_GET["message"]?stripslashes($_GET["message"]):$message;
		
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
<?php
switch($action)
{
case "edit":
?>
	<p>On this page you may alter the question and answer or <a <?php echo 'href="'.$filename.'?action=delete&id='.urlencode($id).'"'; ?> onclick="linkConfirm('Are you sure you want to delete?',this.href);return false">delete</a> the whole entry.</p>
	<p>You may also make the question to be visible on the site.</p>
	<p>After you finish, press the save button to save the changes.</p>
	<br/>
	<form  name="editQuestion" method="post" action="<?php echo "$filename?action=edit&id=$id"; ?>">
	<fieldset>
	<table>
		<tr>
			<td style="width:30%;text-align:right"><label for="question">Question by <?php echo htmlspecialchars($questionData["name"])." ".htmlspecialchars($questionData["surname"])." on ".htmlspecialchars($questionData["datetimeQ"]); ?>: </label></td>
			<td><textarea cols="40" rows="3" name="question"><?php echo $questionData["question"]; //already saved with specialchars?></textarea></td>
		</tr>
		<tr>
			<td style="text-align:right"><label for="answer">Answer: </label></td>
			<td><textarea cols="40" rows="7" name="answer"><?php echo htmlspecialchars($questionData["answer"]); ?></textarea></td>
		</tr>
		<tr>
			<td style="text-align:right"><label for="showOnPage">Show on page? </label></td>
			<td><input type="checkbox" name="showOnPage" <?php echo $questionData["showOnPage"]?'checked="checked"':""; ?>"/></td>
		</tr>
	</table>
	<?php echo $questionData["answer"]?'<p>Edited on '.htmlspecialchars($questionData["datetimeA"]).' by '.htmlspecialchars($questionData["authorA"]).'.</p>':""; ?>
	<input type="hidden" name="id" value="<?php echo $id; ?>"/>
	<input type="submit" name="editQuestion" value="Save"/> <input type="button" value="Return" onclick="document.location='<?php echo $filename; ?>'"/>
	</fieldset>
	</form>
	
<?php
break;
case "show":
default:
?>
	<p>On this page you can manage the questions and answers from "Zadaj pytanie" form.</p>
	<p>Entries use ordering option: <b><?php echo $orderBy; ?></b>.</p>
	<br/>
	<p>Display filter:</p>
	<form id="filterForm" method="get" action="<?php echo $filename; ?>">
		<input name="action" type="hidden" value="show">
		<select name="filter">
			<option value="all" onclick="document.getElementById('filterForm').submit();">Show all</option>
			<option value="notAnswered" <?php echo ($filter=="notAnswered")?'selected="selected"':""; ?> onclick="document.getElementById('filterForm').submit();">Show NOT answered</option>
			<option value="notDisplayed" <?php echo ($filter=="notDisplayed")?'selected="selected"':""; ?> onclick="document.getElementById('filterForm').submit();">Show NOT displayed</option>
			<option value="displayed" <?php echo ($filter=="displayed")?'selected="selected"':""; ?> onclick="document.getElementById('filterForm').submit();">Show displayed</option>
		</select>
		<input type="submit" name="filterSubmit" value="Go"/>
		
		<script type="text/javascript">
			document.getElementsByName("filterSubmit")[0].style.visibility="hidden";
		</script>
	</form>

	<br/>
	<br/>
<?php
	list($tableResponse,$idList,$summary)=printTable($questions,array("filename"=>$filename));
	echo $tableResponse;
	
	$questionsCount=count($idList);
	if($questionsCount>1)
	{
		echo '<hr/>'."\n";
		echo '<h3>Changing order of display</h3>'."\n";
		echo '<div id="selectOrderDiv"><p class="info"><b>JavaScript</b> is turned off. Please turn it on and refresh the page to use this feature.</p></div>';
		
		//if javascript is on print orderingForm, else show information
		$orderResponse="";
		$orderResponse.= '<p>Select an item and position it by pressing Up/Down buttons. When finished press Save Order button.</p>
				
				<form id="changeOrder" method="post" action="'.$filename.'?action=show&filter='.$filter.'">
				<fieldset>
					<input type="button" value="&#47;&#92;" id="buttonUp" style="width:50px"/><input type="button" value="&#92;&#47;" id="buttonDown" style="width:50px"/><br/>
					<select name="selectOrder" id="selectOrder" style="width:300px" size="'.$questionsCount.'">'."\n";
		//select options
		$originalOrder=array();
		$count=0;
		foreach($idList as $id)
		{
			$orderResponse.= '<option value="'.$id.'">'."$id (".htmlspecialchars($summary[$count]).")".'</option>'."\n";
			$originalOrder[]=$id;
			$count++;
		}
		$originalOrder=implode($separator,$originalOrder);
		
		$orderResponse.= '
					</select>
						<input type="hidden" name="itemOrder" id="itemOrder"/>
						<input type="hidden" name="originalOrder" value="'.$originalOrder.'"/>
						<br/>
						<input type="submit" name="changeOrder" value="Save Order"/>
				</fieldset>
				</form>
				';
		$orderResponse=strtr($orderResponse,array("\n"=>"\\\n","\r"=>"")); //preparing for javascirpt input
		
		echo '
				<script type="text/javascript">
					var selectOrderDiv=document.getElementById("selectOrderDiv");
					selectOrderDiv.innerHTML='."'$orderResponse'".';
					
					var selectOrder=document.getElementById("selectOrder");
					var itemOrder=document.getElementById("itemOrder");
					document.getElementById("changeOrder").onsubmit=function(){saveOrder(selectOrder,itemOrder,'."'$separator'".');}
					document.getElementById("buttonUp").onclick=function(){moveUp(selectOrder);}
					document.getElementById("buttonDown").onclick=function(){moveDown(selectOrder);}
				</script>
			';		
		
	}
?>
<?php
break;
}
?>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">QuestionsPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>