<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info (1): On this page you may manage picture galleries (folder $serverName/photos/$galleryDir)
//Info (2): Each folder with photos MUST contain the file "gallery.info", thumbnails are stored in $galleryDir/mini and have the same names as pictures in $galleryDir
//Note (1): You may want to delete/change all CHMOD() according to your server options
//Note (2): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.23

include("login_check.php");

include("functions.php");
$galleriesFolder="photos"; //folder to store galleries

$galleriesPath="../$galleriesFolder";
if(!is_dir($galleriesPath))
{
	if(!mkdir($galleriesPath))
	{
		header("Location: index.php?message=".urlencode('<p class="error">Could not create <b>'.$galleriesFolder.'</b>!</p>'));
	}
	chmod($galleriesPath,0777);
}

$warningSize=6*1048576; //in bytes
$fileFilter=array("jpg"); //array of allowed types, for all types use null
$prefix="gallery"; //used to mark files in tmp folder
$separator=">"; //for properties=>values
$separatorOrder=":"; //for ordering pictures

$maxWidth=700; //resize big pictures
$maxHeight=480; //resize big pictures
$thumbnailHeight=100; //thumbnail resize
$compressionLevel=95;
?>
<?
	$message="";
	$galleryDir=stripslashes($_GET["galleryDir"]);

	$tmpFile=getFromTemp($prefix);
	
	if($_GET["action"]=="edit")
	{
		$action="edit";
		
		if(is_file("$galleriesPath/$galleryDir/gallery.info"))
		{
			//save changes
			if($_POST["editProperties"])
			{
				$galleryDirNew=stripslashes($_POST["galleryDirNew"]);
								
				if(!is_dir("$galleriesPath/$galleryDirNew") || $galleryDir==$galleryDirNew)
				{
					//rename
					if(@rename("$galleriesPath/$galleryDir","$galleriesPath/$galleryDirNew"))
					{
						//ADDITIONAL changing names for all pages using old galleryDir
						include("db_data.php");
						if($connection=@mysql_connect($server,$username,$password))
						{
							$db_select= @mysql_select_db($database);
							mysql_query("SET CHARACTER SET 'utf8'");
							if($db_select)
							{	
								//update personal data
								$query ="UPDATE pages SET galleryDir='$galleryDirNew' WHERE galleryDir='$galleryDir'";
								if(@mysql_query($query))
								{
									$message.='<p class="info">Gallery directory succesfully refreshed in all pages.</p>';
								}
							}
						}
						
						$galleryDir=$galleryDirNew;
						//save changes to file
						if($fh=fopen("$galleriesPath/$galleryDir/gallery.info","w"))
						{
							foreach($_POST as $property => $value)
							{
								if($property!="editProperties" && $property!="galleryDir")
								{
									fwrite($fh,"{$property}$separator".stripslashes($value)."\r\n");
								}
							}
							fclose($fh);
							
							$message.='<p class="success">Gallery information saved.</p>';
						}
						else
						{
							$message.='<p class="error">Could not save gallery information!</p>';
						}
					}
					else
					{
						$message.='<p class="error">Could not rename gallery folder!</p>';
					}
				}
				else
				{
					$message.='<p class="error">Gallery <b>'.$galleryDirNew.'</b> exists already!</p>';
				}
			}
			else if($_POST["appendFile"])
			{
				$newFile=$_FILES["newFile"];
				
				if(is_uploaded_file($newFile["tmp_name"]))
				{
					$newFileExt=strtolower(pathinfo($newFile["name"],PATHINFO_EXTENSION));
					list($width_orig, $height_orig, $type_orig) = getimagesize($newFile['tmp_name']);
					
					//check if valid file (picture or zip)
					if(in_array($type_orig,array(IMAGETYPE_GIF,IMAGETYPE_JPEG,IMAGETYPE_PNG)) || $newFileExt=="zip")
					{				
						//unzip, move to temp, resize, compress, save as file to $galleryDir
						$message.=processPictures($newFile,$galleriesPath,$galleryDir,$maxWidth,$maxHeight,$thumbnailHeight,$compressionLevel);
					}
					else
					{
						$message.='<p class="error">No files appended! Allowed file types are: .zip, .jpg, .png, .gif!</p>';
					}	
					
				}
				else
				{
					$message.='<p class="error">No file uploaded!</p>';
				}
			}
			else if($_POST["changeOrder"])
			{
				$itemOrder=stripslashes($_POST["itemOrder"]);
				$itemOrder=split($separatorOrder,$itemOrder);
				$originalOrder=stripslashes($_POST["originalOrder"]);
				$originalOrder=split($separatorOrder,$originalOrder);
				
				if(count($originalOrder)==count($itemOrder))
				{
					$errors=0;
					//first change to .tmp
					foreach($originalOrder as $id)
					{
						$oldName=sprintf("image%03d.jpg",$id);
						if(!@rename("$galleriesPath/$galleryDir/$oldName","$galleriesPath/$galleryDir/$oldName.tmp")) $errors++;
						if(!@rename("$galleriesPath/$galleryDir/mini/$oldName","$galleriesPath/$galleryDir/mini/$oldName.tmp")) $errors++;
					}
					
					if(!$errors)
					{
						$counter=0;
						//change to new order
						foreach($originalOrder as $id)
						{
							$tmpName=sprintf("image%03d.jpg.tmp",$itemOrder[$counter]);
							$newName=sprintf("image%03d.jpg",$id);
							if(!@rename("$galleriesPath/$galleryDir/$tmpName","$galleriesPath/$galleryDir/$newName")) $errors++;
							if(!@rename("$galleriesPath/$galleryDir/mini/$tmpName","$galleriesPath/$galleryDir/mini/$newName")) $errors++;
							$counter++;
						}
						
						if(!$errors)
						{
							$message.='<p class="success">Reordering successful.</p>';
						}
						else
						{
							$message.='<p class="error">There were some errors ('.$errors.') while reordering!</p>';
						}
					}
					else
					{
						$message.='<p class="error">There were some errors ('.$errors.') while reordering!</p>';
					}
				}
			}
			
			//recieve info file (changes happned recheck needed)
			if(is_file("$galleriesPath/$galleryDir/gallery.info"))
			{
				$info=file("$galleriesPath/$galleryDir/gallery.info");
				$galleryInfo=array();
				foreach($info as $line)
				{
					list($property,$value)=split($separator,rtrim($line));
					$galleryInfo[htmlspecialchars($property)]=htmlspecialchars($value);
				}
			}
		}
		else
		{
			$action="show";
			$message.='<p class="error">Gallery <b>'.$galleryDir.'</b> not found!</p>';
		}
	}
	else if($_GET["action"]=="delete" && $_GET["tmpFile"]=="yes")
	{
		if(deleteTmpFile($prefix))
		{
			$tmpFile=null;
			$message.='<p class="success">Temporary file deleted.</p>';
		}
		else
		{
			$message.='<p class="error">Could not delete temporary file! There is no possibility to upload new files until the temporary file is deleted or renamed!</p>';
		}
	}
	else if($_GET["action"]=="deleteGallery")
	{
		$action="show";
		
		if(is_file("$galleriesPath/$galleryDir/gallery.info"))
		{
			if(deltree("$galleriesPath/$galleryDir"))
			{
				$galleryDir=null;
				$message.='<p class="success">Gallery deleted.</p>';
			}
			else
			{
				$message.='<p class="error">Deletion failed! Could not delete <b>'.$galleryDir.'</b> gallery</p>';
			}
		}
		else
		{
			$message.='<p class="error">Deletion failed! Gallery <b>'.$galleryDir.'</b> not found.</p>';
		}
	}
	else if($_GET["action"]=="deletePicture")
	{
		$action="edit";
	
		$photo=stripslashes($_GET["photo"]);
		if(is_file("$galleriesPath/$galleryDir/$photo"))
		{
			if(unlink("$galleriesPath/$galleryDir/$photo") && unlink("$galleriesPath/$galleryDir/mini/$photo"))
			{
				$message.='<p class="success">Deletion successful!</p>';
			}
			else
			{
				$message.='<p class="error">Could not delete (there were problems)!</p>';
			}
		}
		else
		{
			$message.='<p class="error">Could not delete ('.$photo.' does not exist)!</p>';
		}
	}
	else if($_POST["addGallery"])
	{
		$action="show";
		
		$galleryDir=stripslashes($_POST["galleryDir"]);
		
		//if there is a temp waiting
		if($tmpFile)
		{
			$newFile=array("tmp_name"=>dirname($tmpFile)."/{$prefix}_".basename($tmpFile),"name"=>basename($tmpFile));
		}
		else
		{
			$newFile=$_FILES["newFile"];
		}
		
		if(is_uploaded_file($newFile["tmp_name"]) || is_file($newFile["tmp_name"]))
		{
			$newFileExt=strtolower(pathinfo($newFile["name"],PATHINFO_EXTENSION));
			$galleryDir=$galleryDir?"$galleryDir":pathinfo($newFile["name"],PATHINFO_FILENAME); //if not given, use default
			
			list($width_orig, $height_orig, $type_orig) = getimagesize($newFile['tmp_name']);
			
			//check if valid file (picture or zip)
			if(in_array($type_orig,array(IMAGETYPE_GIF,IMAGETYPE_JPEG,IMAGETYPE_PNG)) || $newFileExt=="zip")
			{
				if(!is_dir("$galleriesPath/$galleryDir"))
				{
					//create dir
					if(mkdir("$galleriesPath/$galleryDir") && chmod("$galleriesPath/$galleryDir",0777))
					{
						//save gallery.info
						if($fh=fopen("$galleriesPath/$galleryDir/gallery.info","w"))
						{
							foreach($_POST as $property => $value)
							{
								if($property!="addGallery" && $property!="galleryDir")
								{
									fwrite($fh,"{$property}$separator".stripslashes($value)."\r\n");
								}
							}
							fclose($fh);
							$message.='<p class="success">Gallery information saved.</p>';
							
							//unzip, move to temp, resize, compress, save as file to $galleryDir
							$message.=processPictures($newFile,$galleriesPath,$galleryDir,$maxWidth,$maxHeight,$thumbnailHeight,$compressionLevel);
							
						}
						else
						{
							$message.='<p class="error">Could not save gallery information!</p>';
						}
					}
					else
					{
						$message.='<p class="error">Could make folder for gallery <b>'.$galleryDir.'</b>! Perhaps the name is too long or it contains not allowed characters?</p>';
					}
				}
				else
				{
					$message.='<p class="error">Name <b>'.$galleryDir.'</b> already exists! Please change the name.</p>';
					saveToTemp($newFile,$prefix);
					$galleryDir="{$galleryDir}_".time();
				}				
			}
			else
			{
				$galleryDir=null; //dont show in form
				$message.='<p class="error">Gallery not created! Allowed file types are: .zip, .jpg, .png, .gif!</p>';
			}	
		}
		else
		{
			$message.='<p class="error">No file uploaded!</p>';
		}
	}
	
	//preventing refresh
	if($message && !$_GET["message"])
	{
		$location="$filename?action=$action";
		$location.=$galleryDir?"&galleryDir=".urlencode($galleryDir):"";
		$location.="&message=".urlencode($message);
		header("Location: $location");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin.js"></script>

<script type="text/javascript" src="lightbox/prototype.js"></script>
<script type="text/javascript" src="lightbox/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="lightbox/lightbox.js"></script>
<link rel="stylesheet" href="lightbox/lightbox.css" type="text/css" media="screen" />

<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Galleries</h1>

	<?php 
		$message=stripslashes($_GET["message"]);
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
<?php
switch($action)
{
case "edit":
?>
	<p>You are managing <b><?php echo $galleryDir; ?></b> gallery.</p>
	<p>Pictures will be resized to: <?php echo"<b>$maxWidth x $maxHeight</b>"; ?> and they will be shown on the page in the same order as they are shown below. New pictures will be added at the end of gallery.</p>
	<p>You may edit <b>properties</b> of the gallery or <b>append</b> a picture or a set of pictures (.zip file) to the gallery.<br/>
	You may also <a href="<?php echo "$filename?action=deleteGallery&galleryDir=".urlencode($galleryDir); ?>" onclick="linkConfirm('Are you sure you want to delete the ENTIRE gallery with ALL pictures containing it?',this.href);return false">delete</a> the <b>entire</b> gallery.</p>
	
	<form method="post" action="<?php echo "$filname?action=edit&galleryDir=".urlencode($galleryDir); ?>">
	<fieldset>
		<table>
			<tr>
				<td style="width:30%;text-align:right"><label for="galleryDirNew">Gallery Directory: </label></td>
				<td><input name="galleryDirNew" type="text" size="30" value="<?php echo $galleryDir; ?>"/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="pl-name">PL-Name: </label></td>
				<td><input name="pl-name" type="text" size="30" value="<?php echo $galleryInfo["pl-name"]; ?>"/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="en-name">EN-Name: </label></td>
				<td><input name="en-name" type="text" size="30" value="<?php echo $galleryInfo["en-name"]; ?>"/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="pl-info">PL-Info: </label></td>
				<td><input name="pl-info" type="text" size="30" value="<?php echo $galleryInfo["pl-info"]; ?>"/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="en-info">EN-Info: </label></td>
				<td><input name="en-info" type="text" size="30" value="<?php echo $galleryInfo["en-info"]; ?>"/></td>
			</tr>
		</table>
		<input type="submit" name="editProperties" value="Save data"/> <input type="button" value="Return" onclick="document.location='<?php echo $filename; ?>'"/>
	</fieldset>
	</form>
	
	<br/>
	<table class="display">
		<thead>
			<tr><td>Picture</td><td>Filename</td><td>Options</td></tr>
		</thead>
		<tbody>
		<tr class="addNew">
		<td style="text-align:right">Add new picture (<b>single</b> or a <b>.zip</b> set): </td>
		<td>
			<form enctype="multipart/form-data" action="<?php echo "$filename?action=edit&galleryDir=".urlencode($galleryDir); ?>" method="post">
				<input name="newFile" type="file" value="Browse..."/><br/>
				<input type="submit" name="appendFile" value="Add"/>
			</form>
		</td>
		<td>
			<p><b>Note: </b>Maximum upload size is 20mb, and time is 200sec.</p>
			<p>Picture or set of pictures will be added to the end of the gallery.</p>
		</td>
		</tr>
<?php
	//pictures from the gallery
	$galleryPath="$galleriesPath/$galleryDir";
	if($gallery=getFiles("$galleryPath",$fileFilter))
	{
		foreach($gallery as $photo)
		{
			$photoSize=filesize("$galleryPath/$photo");
			echo '<tr><td><a href="'."$galleryPath/$photo?".time().'" rel="lightbox['.$galleryDir.']" title="Gallery Directory: '.$galleryDir.'"><img src="'."$galleryPath/mini/$photo?".time().'" alt="'.$photo.'" style="max-height:100px"/></a></td>
				<td>'.$photo; printf("<br/><i>%.2fKB</i>",$photoSize/1024); echo '</td>
				<td><a href="'.$filename.'?action=deletePicture&galleryDir='.urlencode($galleryDir).'&photo='.$photo.'" onclick="linkConfirm(\'Are you sure you want to delete the picture?\',this.href);return false">Delete</a></td></tr>';
		}
	}
?>
<?php
if(count($gallery)>10)
{
?>
		<tr class="addNew">
		<td style="text-align:right">Add new picture (<b>single</b> or a <b>.zip</b> set): </td>
		<td>
			<form enctype="multipart/form-data" action="<?php echo "$filename?action=edit&galleryDir=".urlencode($galleryDir); ?>" method="post">
				<input name="newFile" type="file" value="Browse..."/><br/>
				<input type="submit" name="appendFile" value="Add"/>
			</form>
		</td>
		<td>
			<p><b>Note: </b>Maximum upload size is 20mb, and time is 200sec.</p>
			<p>Picture or set of pictures will be added to the end of the gallery.</p>
		</td>
		</tr>
<?php
}
?>
		</tbody>
		</table>
<?php
	//chaning order
	$galleryCount=count($gallery);
	if($galleryCount>1)
	{
		echo '<hr/>'."\n";
		echo '<h3>Changing order of display</h3>'."\n";
		echo '<div id="selectOrderDiv"><p class="info"><b>JavaScript</b> is turned off. Please turn it on and refresh the page to use this feature.</p></div>';
		
		//if javascript is on print orderingForm, else show information
		$orderResponse="";
		$orderResponse.= '<p>Select an item and position it by pressing Up/Down buttons. When finished press Save Order button.</p>
				
				<form id="changeOrder" method="post" action="'.$filename.'?action=edit&galleryDir='.urlencode($galleryDir).'">
				<fieldset>
					<input type="button" value="&#47;&#92;" id="buttonUp" style="width:50px"/><input type="button" value="&#92;&#47;" id="buttonDown" style="width:50px"/><br/>
					<select name="selectOrder" id="selectOrder" style="width:300px" size="'.$galleryCount.'">'."\n";
		//select options
		$originalOrder=array();
		$count=0;
		foreach($gallery as $photo)
		{
			list($id)=sscanf($photo,"image%d");
			$orderResponse.= '<option value="'.$id.'">'."image$id.jpg".'</option>'."\n";
			$originalOrder[]=$id;
			$count++;
		}
		$originalOrder=implode($separatorOrder,$originalOrder);
		
		$orderResponse.= '
					</select>
						<input type="hidden" name="itemOrder" id="itemOrder"/>
						<input type="hidden" name="originalOrder" value="'.$originalOrder.'"/>
						<br/>
						<input type="submit" name="changeOrder" value="Save Order"/>
				</fieldset>
				</form>
				';
		$orderResponse=strtr($orderResponse,array("\n"=>"\\\n","\r"=>"")); //preparing for javascirpt input
		
		echo '
				<script type="text/javascript">
					var selectOrderDiv=document.getElementById("selectOrderDiv");
					selectOrderDiv.innerHTML='."'$orderResponse'".';
					
					var selectOrder=document.getElementById("selectOrder");
					var itemOrder=document.getElementById("itemOrder");
					document.getElementById("changeOrder").onsubmit=function(){saveOrder(selectOrder,itemOrder,'."'$separatorOrder'".');}
					document.getElementById("buttonUp").onclick=function(){moveUp(selectOrder);}
					document.getElementById("buttonDown").onclick=function(){moveDown(selectOrder);}
				</script>
			';		
		
	}
?>
	
<?php
break;
case "show":
default:
?>
	<p>On this page you may add a new gallery by uploading a picture or set of pictures (.zip file). You may also see/edit/delete existing galleries.</p>
	<p>All valid uploaded files will be <b>resized</b>, <b>compressed</b> and saved as <b>.jpg</b>.<br/>
	<b>Note: </b>The <b>order</b> of pictures shown on field <b>Pictures</b> may be not updated; the order on the edit page is the right one.</p>
	<p>Galleries are located in folder: <b><?php echo "http://".$_SERVER["SERVER_NAME"]."/".$galleryFolder."/"; ?></b>.<br/>
	</b>The upload limit is 20mb; please be <b>patient</b> while uploading a huge file!</p>
	<br/>
	
	<table class="display">
		<thead>
		<tr>
			<td>Directory</td><td>Properties</td><td>Pictures</td>
		</tr>
		</thead>
		<tbody>
			<tr class="addNew">
				<td>Add a new gallery:</td>
				<td>
				<form  enctype="multipart/form-data" method="post" action="<?php echo $filename; ?>">
					<table>
						<tr>
							<td style="width:30%;text-align:right"><label for="galleryDir" <?php echo $tmpFile?'class="info"':""; ?>>Gallery Directory: </label></td>
							<td><input name="galleryDir" type="text" size="30" value="<?php echo $galleryDir?htmlspecialchars($galleryDir):($tmpFile?pathinfo($tmpFile,PATHINFO_FILENAME)."_".time():""); ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="pl-name">PL-Name: </label></td>
							<td><input name="pl-name" type="text" size="30" value="<?php echo stripcslashes(htmlspecialchars($_POST["pl-name"])); ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="en-name">EN-Name: </label></td>
							<td><input name="en-name" type="text" size="30" value="<?php echo stripcslashes(htmlspecialchars($_POST["en-name"])); ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="pl-info">PL-Info: </label></td>
							<td><input name="pl-info" type="text" size="30" value="<?php echo stripcslashes(htmlspecialchars($_POST["pl-info"])); ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="en-info">EN-Info: </label></td>
							<td><input name="en-info" type="text" size="30" value="<?php echo stripcslashes(htmlspecialchars($_POST["en-info"])); ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="newFile">Photo or set of photos: </label></td>
							<td><?php echo $tmpFile?'<p><b>'.basename($tmpFile).'</b></p>':'<input name="newFile" type="file" value="Browse..."/>'; ?></td>
						</tr>						
					</table>
					<input type="submit" name="addGallery" value="Create Gallery"/>
				</form>
				</td>
				<td>
					<?php echo $tmpFile?'<p class="info">There is a file on the server which needs to be <b>renamed</b> in order to be saved. <a href="'.$filename.'?action=delete&tmpFile=yes" onclick="linkConfirm(\'Are you sure you want to delete the temporary file?\',this.href);return false">Delete the file?</a></p>':''; ?>
					<p><b>Note: </b>GalleryDirectory field is the name of the file which will be created on the server. If it is left empty the original name of the uploaded file will be saved instead.</p>
				</td>
			</tr>		
<?php
//show all galleries (directiories, which contain file gallery.info)  from the $galleryFolder
	if($theFiles=getFiles($galleriesPath))
	{
		foreach($theFiles as $galleryDir)
		{
			if(is_file("$galleriesPath/$galleryDir/gallery.info"))
			{
				echo "<tr>\n";
				
				echo '<td><a href="'.$filename.'?action=edit&galleryDir='.urlencode($galleryDir).'"><b>'.$galleryDir.'</b><br/>[ manage ]</a></td>'."\n";
				
				//get all pictures
				$gallerySize=0;
				$photosResponse=""; //generating <img> thumbnails
				if($photos=getFiles("$galleriesPath/$galleryDir",$fileFilter))
				{
					foreach($photos as $photo)
					{
						$gallerySize+=filesize("$galleriesPath/$galleryDir/$photo");
						$photosResponse.='<img src="'."$galleriesPath/$galleryDir/mini/$photo?".'" alt="'.$photo.'" style="height:20px;margin-right:10px;margin-top:10px"/>'."\n";
					}
				}
				
				//SizeWarning
				if($gallerySize>$warningSize)
				{
					printf("<td><p class=\"info\"><b>FileSize:</b> %.2fMB (%d)</p><br/>\n",$gallerySize/1048576,count($photos));
				}
				else
				{
					printf("<td><p><b>FileSize:</b> %.2fMB (%d)</p><br/>\n",$gallerySize/1048576,count($photos));
				}
				
				//info file
				$info=file("$galleriesPath/$galleryDir/gallery.info");
				foreach($info as $line)
				{
					list($property,$value)=split($separator,rtrim($line));
					echo "<p><b>".htmlspecialchars($property).":</b> ".htmlspecialchars($value)."</p>\n";
				}				
				
				echo '</td>'."\n";
				echo "<td>$photosResponse</td>\n";
				echo "</tr>\n";
			}
		}	
	}
?>	
		</tbody>
	</table>
<?php
break;
}
?>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]; ?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">GalleriesPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>