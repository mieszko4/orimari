tinyMCEPopup.requireLangPack();

var PresentationDialog = {
	init : function() {
		//nothing
	},

	insert : function() {
		var presentationValue= document.forms[0].presentation.value
		var presentationsPath="../presentations";
		
		var presentationHTML='<a href="'+presentationsPath+'/'+presentationValue+'.ppt">';
		presentationHTML +='<img src="'+presentationsPath+'/'+presentationValue+'.jpg"';
		presentationHTML +='alt="'+tinyMCEPopup.getLang('presentation_dlg.download')+'" style="border-style: solid; width: 200px;"/></a>';
		
		tinyMCEPopup.editor.execCommand('mceInsertContent', false, presentationHTML);
		tinyMCEPopup.close();
	}
};

tinyMCEPopup.onInit.add(PresentationDialog.init, PresentationDialog);
