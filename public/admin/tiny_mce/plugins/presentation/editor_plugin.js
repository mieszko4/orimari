(function() {
	tinymce.PluginManager.requireLangPack('presentation');

	tinymce.create('tinymce.plugins.PresentationPlugin', {
		init : function(ed, url) {
			ed.addCommand('mcePresentation', function() {
				ed.windowManager.open({
					file : url + '/presentation.php',
					width : 320,
					height : 120,
					inline : 1
				}, {
					plugin_url : url
				});
			});

			ed.addButton('presentation', {
				title : 'presentation.desc',
				cmd : 'mcePresentation',
				image : url + '/img/presentation.png'
			});

			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('presentation', n.nodeName == 'IMG');
			});
		},

		createControl : function(n, cm) {
			return null;
		},

		getInfo : function() {
			return {
				longname : 'Presentation plugin',
				author : 'Mi�osz Chmura',
				authorurl : 'http://www.chmura.org/',
				infourl : 'http://www.orimari.com/',
				version : "1.0"
			};
		}
	});

	tinymce.PluginManager.add('presentation', tinymce.plugins.PresentationPlugin);
})();