<?php
include("../../../functions.php");

$fileFilter=array("ppt","pptx","pps","ppsx"); //for all types use null
$presentationsPath="../../../../presentations";
$presentations = Array();

if($names=getFiles($presentationsPath,$fileFilter)) {
	foreach($names as $name) {
		array_push($presentations,pathinfo($name,PATHINFO_FILENAME));
	}
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{#presentation_dlg.title}</title>
	<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
	<script type="text/javascript" src="js/presentation.js"></script>
</head>
<body>
<p>{#presentation_dlg.desc}</p>
<form onsubmit="PresentationDialog.insert();return false;" action="#">
	<select id="presentation" name="presentation">
		<?php
			foreach($presentations as $presentation) {
				print '<option value="'.$presentation.'">'.$presentation.'</option>';
			}
		?>
	</select>
	<div class="mceActionPanel">
		<input type="button" id="insert" name="insert" value="{#presentation_dlg.submit}" onclick="PresentationDialog.insert();" />
		<input type="button" id="cancel" name="cancel" value="{#presentation_dlg.cancel}" onclick="tinyMCEPopup.close();" />
	</div>
</form>
<!--
<form onsubmit="ExampleDialog.insert();return false;" action="#">
	<p>Here is a example dialog.</p>
	<p>Selected text: <input id="someval" name="someval" type="text" class="text" /></p>
	<p>Custom arg: <input id="somearg" name="somearg" type="text" class="text" /></p>

	<div class="mceActionPanel">
		<input type="button" id="insert" name="insert" value="{#insert}" onclick="ExampleDialog.insert();" />
		<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();" />
	</div>
</form>-->

</body>
</html>
