tinyMCE.addI18n('en.presentation_dlg',{
	title : 'Select presentation and click insert',
	desc : 'Please select presentation in order to insert presentation in the text content at cursor position.',
	submit : 'Insert',
	cancel : 'Cancel',
	download : 'Download presentation'
});
