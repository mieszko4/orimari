﻿(function() {
	tinymce.PluginManager.requireLangPack('presentation');

	tinymce.create('tinymce.plugins.PresentationPlugin', {
		init : function(ed, url) {
			ed.addCommand('mcePresentation', function() {
				/*ed.windowManager.open({
					file : url + '/dialog.htm',
					width : 320 + parseInt(ed.getLang('example.delta_width', 0)),
					height : 120 + parseInt(ed.getLang('example.delta_height', 0)),
					inline : 1
				}, {
					plugin_url : url, // Plugin absolute URL
					some_custom_arg : 'custom arg' // Custom argument
				});*/
				alert('clicked!');
			});

			ed.addButton('presentation', {
				title : 'presentation.desc',
				cmd : 'mcePresentation',
				image : url + '/img/presentation.png'
			});

			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('presentation', n.nodeName == 'IMG');
			});
		},

		createControl : function(n, cm) {
			return null;
		},

		getInfo : function() {
			return {
				longname : 'Presentation plugin',
				author : 'Miłosz Chmura',
				authorurl : 'http://www.chmura.org/',
				infourl : 'http://www.orimari.com/',
				version : "1.0"
			};
		}
	});

	tinymce.PluginManager.add('presentation', tinymce.plugins.PresentationPlugin);
})();