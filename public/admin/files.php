<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a page for AdminPages from which some specified files can be accessed and edited; (see $allowedAcces)
//Note (1): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.25

include("login_check.php");

include("functions.php");
$allowedFiles=array("files/orimari.css","files/header.php","files/footer.php", "./index.php"); //for root files use "./$filename"
$allowedFolders=array("pl","en"); //ALL files in the folder will be allowed
?>
<?php
	$message=="";
	
	if($_GET["action"]=="edit")
	{
		$action="edit";
		
		$file=$_GET["file"];
		$dirName=dirname($file);
		$fileName=basename($file);
		
		if(in_array($dirName,$allowedFolders) || in_array("$dirName/$fileName",$allowedFiles))
		{
			$action="edit";
			
			//save changes
			if($_POST["submit"] && $_POST["fileContent"])
			{
				if($fh=@fopen("../$file","w"))
				{
					if(fwrite($fh,stripslashes($_POST["fileContent"])))
					{
						$message.='<p class="success">File saved!</p>';
					}
					else
					{
						$message.='<p class="error">Could not save the file!</p>';
					}				
					fclose($fh);
				}
				else
				{
					$message.='<p class="error">Could not open for writing!</p>';
				}
			}
			
			//get the file to string
			if($fileContent=file("../$file"))
			{
				$fileContent=implode("",$fileContent);
			}
			else
			{
				$message.='<p class="error">Could not open the file!</p>';
			}
		}
		else
		{
			$action="show";
			
			$message.='<p class="error">No access for file <b>'.$file.'</b>!</p>';
			$file="";
		}
	}

	//preventing refresh
	if($message && !$_GET["message"])
	{
		$location="$filename?action=$action";
		$location.=$file?"&file=".urlencode($file):"";
		$location.="&message=".urlencode($message);
		header("Location: $location");
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script language="Javascript" type="text/javascript" src="edit_area/edit_area_full.js"></script>
<script language="Javascript" type="text/javascript">
	// initialisation
	editAreaLoader.init({
		id: "fileContent" // id of the textarea to transform		
		,start_highlight: true
		,allow_resize: "n"
		,font_family: "monospace"
		,font_size: "8"
		,allow_toggle: false
		,language: "en"
		,syntax: "<?php echo pathinfo($file,PATHINFO_EXTENSION); ?>" //generated on server
		,show_line_colors: true
		,toolbar: "|,search,fullscreen,|,undo,redo,|,help"
	});
</script>
<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Files</h1>

	<?php 
		$message=stripslashes($_GET["message"]);
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
<?php
switch($action)
{
case "edit":
?>
	<p>You are editing <b><?php echo $file; ?></b>. After you finish click on <b>Save</b> button.</p>
	<form name="fileForm" method="post" action="<?php echo "$filename?action=edit&file=".urlencode($file); ?>" enctype="multipart/form-data">
		<textarea name="fileContent" id="fileContent" style="width:100%;height:300px"><?php echo htmlspecialchars($fileContent); ?></textarea><br/>
		<br/>
		<input type="submit" name="submit" value="Save"/> <input type="button" value="Preview" id="previewA"/> <input type="button" value="Return" onclick="document.location='<?php echo $filename; ?>'"/>
	</form>

	<div id="iframeDiv"></div>
	<script type="text/javascript">
		var previewA = document.getElementById("previewA");
		var iframeDiv = document.getElementById("iframeDiv");
		var fileContent = document.getElementsByName("fileContent")[0];
		
		function showHideIframe()
		{
			if(iframeDiv.innerHTML=="")
			{
				iframeDiv.innerHTML='<br/><iframe src="<?php echo "../$file"; ?>" style="width:100%;height:500px;background-color:#ffffff"></iframe>';
				fileContent.style.height="50px";
			}
			else
			{
				iframeDiv.innerHTML="";
				fileContent.style.height="300px";
			}
		}
		previewA.onclick=showHideIframe;
		<?php if($_POST["submit"]) echo "showHideIframe();"; ?>
	</script>

<?php
break;
case "show";
default:
?>
	<p>On this page you can get direct access to the source code of chosen pages.</p>
	<p>Unless you are sure what you are doing, please <a href="index.php">LEAVE</a> this page.</p>
	<br/>
	<p><b>MainCore files:</b><p>
	<ul>		
		<li>Edit style sheet <a href="<?php echo "$filename?action=edit&file=".urlencode("files/orimari.css"); ?>">orimari.css</a>.</li>
		<li>Edit header <a href="<?php echo "$filename?action=edit&file=".urlencode("files/header.php"); ?>">header.php</a>.</li>
		<li>Edit footer <a href="<?php echo "$filename?action=edit&file=".urlencode("files/footer.php"); ?>">footer.php</a>.</li>
		<li>Edit <b>main</b> index file <a href="<?php echo "$filename?action=edit&file=".urlencode("index.php"); ?>">index.php</a>.</li>
	</ul>
	<p><b>Folder PL files</b></p>
	<ul>
<?php
$dirName="pl"; //name relative to the http://$server/
if($theFiles=getFiles("../$dirName"))
{
	foreach ($theFiles as $currentFile)
	{
		if(is_file("../$dirName/$currentFile"))
		{
			echo '<li>Edit polish <a href="'.$filename.'?action=edit&file='.urlencode("$dirName/$currentFile").'">'.$currentFile.'</a></li>';
		}
	}
}
else
{
	echo '<li><p class="info">Folder empty.</p></li>';
}	
?>		</ul>
		<p><b>Folder EN files</b></p>
		<ul>
<?php
$dirName="en";  //name relative to the http://$server/
if($theFiles=getFiles("../$dirName"))
{
	foreach ($theFiles as $currentFile)
	{
		if(is_file("../$dirName/$currentFile"))
		{
			echo '<li>Edit english <a href="'.$filename.'?action=edit&file='.urlencode("$dirName/$currentFile").'">'.$currentFile.'</a></li>';
		}
	}
}
else
{
	echo '<li><p class="info">Folder empty.</p></li>';
}
?>
		</ul>
	
<?php
break;
}
?>
	
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">FilesPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>