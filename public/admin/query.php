<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a page for AdminPages from which mysql queries can be made; for some queries detailed feedback is shown
//Note (1): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.10.07

include("login_check.php");

include("functions.php");
?>
<?php
	include("db_data.php");
	if($connection=@mysql_connect($server,$username,$password))
	{
		$db_select= @mysql_select_db($database);
		mysql_query("SET CHARACTER SET 'utf8'");
		if($db_select)
		{	
			if($_POST["query"] || $_POST["queryFile"])
			{
				if($_POST["query"])
				{
					$queryString=stripslashes($_POST["queryLine"]);
					
				}
				else //query from file
				{
					$file=$_FILES["upload"];
					if(is_uploaded_file($file["tmp_name"]))
					{
						$file=file($file["tmp_name"]);
						$queryString=implode($file,"");	
					}
				}
				
				$queryString=strtr($queryString,array("\r\n"=>"\n")); //problem with multple lines
				$queries=split(";\n",$queryString);
				
				foreach($queries as $queryLine)
				{
					if($queryLine)
					{
						if($table=@mysql_query($queryLine))
						{
							$message.='<p class="success">Query <b>'.htmlspecialchars($queryLine).'</b> successfully done!</p>';
							
							$queryLine=ltrim($queryLine);
							preg_match("/^[^ \s]*/",$queryLine,$matches);
							$queryCommand=$matches[0];
							if(in_array($queryCommand,array("select","show","describe","explain")))
							{
								$message.='<p>Result:</p>';
								list($tableResponse)=printTable($table);
								$message.=$tableResponse;
							}
							else if(in_array($queryCommand,array("delete","insert","replace","update","alter")))
							{
								$message.='<p class="info">Number of affected rows: '.mysql_affected_rows().'</p>';
								$message.='<p class="info">'.htmlspecialchars(mysql_info()).'</p>';
							}						
						}
						else
						{
							$message.='<p class="error">Could not query!</p>';
							$message.='<p class="error">'.htmlspecialchars(mysql_error()).'</p>';
						}
					}
				}
			}
		}
		else
		{
			$message.='<p class="error">Database error (2). Try again later.</p>';
		}
	}
	else
	{
		$message.='<p class="error">Database error (1). Try again later.</p>';
	}
	@mysql_close($connection);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Query</h1>

	<?php if($message) {echo "<br/>$message<hr/>";} ?>

	<br/>
	<p>On this page you can make a user-defined query.</p>
	<p>Unless you are sure what you are doing, please <a href="index.php">LEAVE</a> this page.</p>
	<p><b>Note:</b> If you use multiple queries separate them by <b>;</b> and <b>new line</b>.</p>
	<br/>
		
	<form  name="query" method="post" action="<?php echo $filename; ?>">
	<fieldset>
		<table>
			<tr>
				<td style="width:30%;text-align:right"><label for="query">Query statement: </label></td>
				<td><textarea name="queryLine" cols="40" rows="3"></textarea></td>
			</tr>
			<tr>
				<td style="text-align:right"></td>
				<td><input type="submit" name="query" value="Query!"/></td>
			</tr>
		</table>		
	</fieldset>
	</form>
	<br/>
	<br/>
	<form enctype="multipart/form-data" action="<?php echo $filename; ?>" method="post">
	<fieldset>
		<table>
			<tr>
				<td style="text-align:right"><label for="upload">File: </label></td>
				<td><input name="upload" type="file" value="Browse..." /></td>
			</tr>
		</table>
		<br/>
		<input type="submit" name="queryFile" value="Query from file"/>
	</fieldset>
	</form>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">QueryPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>