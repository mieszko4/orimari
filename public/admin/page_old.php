<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a page for AdminPages on which you can manage all main and sub pages (except for main $serverName/index.php and $serverName/pl/pytanie.php)
//Note (1): NO CHECKING IF TABLES >>>mainpages<<< >>>pages<<<  >>>submenupages<<< EXIST. IT MUST EXIST FOR THE PAGE TO WORK
//Note (2): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.30
//UpdateDate: 2009.05.24 (general reclearance, allows adding subSubpages to subpages)

include("login_check.php");

include("functions.php");

$galleriesPath="../photos";
$moviesPath="../movies";
$fileFilter=array("flv"); //for movies
$templateFile="template.php";
?>
<?php
	include("db_data.php");
	if($connection=@mysql_connect($server,$username,$password))
	{
		$db_select= @mysql_select_db($database);
		mysql_query("SET CHARACTER SET 'utf8'");
		if($db_select)
		{
			$action="show"; //default action
			$refresh=true;
			$message="";
			
			$pageID=$_GET["pageID"];
			$pageData["pageIDHolder"]=$_GET["pageIDHolder"];
			$pageType=$pageData["pageIDHolder"]?"sub":"main";
			$pageData["languageID"]=$_GET["languageID"];
			
			if($_GET["action"]=="add")
			{
				$action="add";
				$refresh=false;
				
				if($pageType=="main")
				{
					if($_POST["addPage"])
					{
						//get all data
						$pageData["filename"]=stripslashes($_POST["filename"]); //needed for saving file (queryData has addslashes())
						$pageData["title"]=$_POST["title"];
						$pageData["titleSubMenuPosition"]=$_POST["titleSubMenuPosition"];
						$pageData["languageID"]=$_POST["languageID"];
						
						$pageData["textContent"]=$_POST["textContent"];
						$pageData["galleryDir"]=$_POST["galleryDir"];
						$pageData["movieName"]=$_POST["movieName"];
						$pageData["description"]=$_POST["description"];
						
						//get language name
						$query="SELECT language FROM languages WHERE languageID={$pageData["languageID"]} LIMIT 1";
						if($language=@mysql_query($query))
						{
							if($row=@mysql_fetch_array($language,MYSQL_ASSOC));
							{
								$pageData["language"]=$row["language"];
							}
						}
						
						//check if extension used
						$ext=strtolower(pathinfo($pageData["filename"],PATHINFO_EXTENSION));
						//ignore extension
						$name=($ext=="php")?pathinfo($pageData["filename"],PATHINFO_FILENAME):$pageData["filename"];
						$pageData["filename"]=$name?"$name.php":$pageData["filename"]; //if not given, use default
						
						if(!is_file("../{$pageData["language"]}/{$pageData["filename"]}"))
						{
							if(@copy("../files/$templateFile","../{$pageData["language"]}/{$pageData["filename"]}"))
							{
								//make queries
								$queryData["textContent"]=$pageData["textContent"]?"'{$pageData["textContent"]}'":"NULL";
								$queryData["movieName"]=$pageData["movieName"]?"'{$pageData["movieName"]}'":"NULL";
								$queryData["galleryDir"]=$pageData["galleryDir"]?"'{$pageData["galleryDir"]}'":"NULL";
								$queryData["description"]=$pageData["description"]?"'{$pageData["description"]}'":"NULL";
								
								$query="INSERT INTO pages VALUES (NULL,NULL,{$queryData["textContent"]},{$queryData["movieName"]},{$queryData["galleryDir"]},{$queryData["description"]})";
								if(@mysql_query($query))
								{
									$pageID=mysql_insert_id();
									
									$queryData["filename"]="'".addslashes($pageData["filename"])."'"; //have to add slashes
									$queryData["title"]=$pageData["title"]?"'{$pageData["title"]}'":"NULL";
									$queryData["titleSubMenuPosition"]=$pageData["titleSubMenuPosition"]?"'{$pageData["titleSubMenuPosition"]}'":"left";
									$queryData["languageID"]=$pageData["languageID"]?$pageData["languageID"]:1;
									
									
									$query="INSERT INTO mainpages VALUES($pageID,{$queryData["title"]},{$queryData["titleSubMenuPosition"]},{$queryData["filename"]},{$queryData["languageID"]})";
									if(@mysql_query($query))
									{
										$action="show";
										$refresh=true;
										$message.='<p class="success">Page <b>'.htmlspecialchars($pageData["filename"]).'</b> successfully created.</p>';
									}
									else
									{
										@unlink("../{$pageData["language"]}/{$pageData["filename"]}");
										$pageID=null;
										$message.='<p class="error">Could not save the page (2)! Try again later.</p>';
									}
								}
								else
								{
									@unlink("../{$pageData["language"]}/{$pageData["filename"]}");
									$message.='<p class="error">Could not save the page (1)! Try again later.</p>';
								}
							}
							else
							{
								$message.='<p class="error">Could not create filename <b>'.htmlspecialchars($pageData["filename"]).'</b>!</p>';
							}
						}
						else
						{
							$message.='<p class="error">Filename <b>'.htmlspecialchars($pageData["filename"]).'</b> exists already!</p>';
							$pageData["filename"]=null;
						}						
					}
					
				}
				else //$pageType=="sub"
				{
					//find recursive path
					$currentPageID=$pageData["pageIDHolder"];
					$backwardDescriptions = null;
					$backwardIds = null;
					$recCounter=0;
					do
					{
						$query="SELECT pageIDHolder, description FROM pages WHERE pageID=$currentPageID LIMIT 1";
						if($page=@mysql_query($query))
						{
							if($rowPage=@mysql_fetch_array($page,MYSQL_ASSOC))
							{
								$backwardDescriptions[$recCounter]=$rowPage['description'];
								$backwardIds[$recCounter]=$currentPageID;
								
								$currentPageID=$rowPage['pageIDHolder'];
								$recCounter++;
							}
							else
							{
								$currentPageID=null;
							}
						}
						else
						{
							$message.='<p class="error">Could not query.</p>';
							$currentPageID=null;
						}
					}
					while($currentPageID);
					
					if($_POST["addPage"])
					{
						$pageData["textContent"]=$_POST["textContent"];
						$pageData["movieName"]=$_POST["movieName"];
						$pageData["galleryDir"]=$_POST["galleryDir"];
						$pageData["description"]=$_POST["description"];
						
						$queryData["pageIDHolder"]=$pageData["pageIDHolder"];
						$queryData["textContent"]=$pageData["textContent"]?"'{$pageData["textContent"]}'":"NULL";
						$queryData["movieName"]=$pageData["movieName"]?"'{$pageData["movieName"]}'":"NULL";
						$queryData["galleryDir"]=$pageData["galleryDir"]?"'{$pageData["galleryDir"]}'":"NULL";
						$queryData["description"]=$pageData["description"]?"'{$pageData["description"]}'":"NULL";
						
						$query="INSERT INTO pages VALUES (NULL,{$queryData["pageIDHolder"]},{$queryData["textContent"]},{$queryData["movieName"]},{$queryData["galleryDir"]},{$queryData["description"]})";
						if(@mysql_query($query))
						{
							$action="edit";
							$pageID=$pageData["pageIDHolder"];
							$refresh=true;
							$message.='<p class="success">Subpage for <b>'.htmlspecialchars($pageData["filename"]).'</b> successfully created.<br/>You may now add it as a new item in submenu.</p>';
						}
						else
						{
							$action="edit";
							$pageID=$pageData["pageIDHolder"];
							$refresh=true;
							$message.='<p class="error">Could not save the subpage! Try again later.</p>';
						}
					}
				}
			}
			else if($_GET["action"]=="edit")
			{
				$action="edit"; //with refresh
				$pageType=null; //noType
				
				
				//find recursive path
				$currentPageID=$pageID;
				$backwardDescriptions = null;
				$backwardIds = null;
				$recCounter=0;
				do
				{
					$query="SELECT pageIDHolder, description FROM pages WHERE pageID=$currentPageID LIMIT 1";
					if($page=@mysql_query($query))
					{
						if($rowPage=@mysql_fetch_array($page,MYSQL_ASSOC))
						{
							$backwardDescriptions[$recCounter]=$rowPage['description'];
							$backwardIds[$recCounter]=$currentPageID;
							
							$currentPageID=$rowPage['pageIDHolder'];
							$recCounter++;
						}
						else
						{
							$currentPageID=null;
						}
					}
					else
					{
						$message.='<p class="error">Could not query.</p>';
						$currentPageID=null;
					}
				}
				while($currentPageID);
				
				$isError=false;
				if($recCounter==1)
				{
					$pageType="main";
					
					//SAVE MAINPAGE data
					if($_POST["editPage"])
					{
						//get all data
						$pageData["filenameNew"]=stripslashes($_POST["filename"]); //needed for saving file (queryData has addslashes())
						$pageData["title"]=$_POST["title"];
						$pageData["titleSubMenuPosition"]=$_POST["titleSubMenuPosition"];
						//$pageData["languageID"]=$_POST["languageID"]; //disable changing of language
						
						//get file path
						$query="SELECT language,filename FROM languages NATURAL JOIN mainpages WHERE pageID=$pageID LIMIT 1";
						if($languagePage=@mysql_query($query))
						{
							if($row=@mysql_fetch_array($languagePage,MYSQL_ASSOC))
							{
								$pageData["language"]=$row["language"];
								$pageData["filename"]=$row["filename"]; //no slashes
								$filePath="../{$row["language"]}/{$row["filename"]}";
							}
						}
						
						//check if extension used
						$ext=strtolower(pathinfo($pageData["filenameNew"],PATHINFO_EXTENSION));
						//ignore extension
						$name=($ext=="php")?pathinfo($pageData["filenameNew"],PATHINFO_FILENAME):$pageData["filenameNew"];
						$pageData["filenameNew"]=$name?"$name.php":$pageData["filenameNew"]; //if not given, use default
						
						if(!is_file("../{$pageData["language"]}/{$pageData["filenameNew"]}") || $pageData["filenameNew"]==$pageData["filename"])
						{
							if(rename($filePath,"../{$pageData["language"]}/{$pageData["filenameNew"]}"))
							{
								$isError=false;
							}
							else
							{
								$message.='<p class="error">Could not rename to filename <b>'.htmlspecialchars($pageData["filenameNew"]).'</b>!</p>';
								$isError=true;
							}
						}
						else
						{
							$message.='<p class="error">Filename <b>'.htmlspecialchars($pageData["filename"]).'</b> exists already!</p>';
							$pageData["filename"]=null;
							$isError=true;
						}
					}
				}
				else
				{
					$pageType="sub";
				}
				
				if($recCounter>=1 && !$isError) //mainpage and subpage
				{
					//SAVE menuitems
					if($_POST["javascriptCheck"]=="yes")
					{
						$itemOrder=stripslashes($_POST["itemOrder"]);
						$itemOrder=strtr($itemOrder,array("\r"=>""));
						
						$queryDelete="DELETE FROM submenupages WHERE pageIDHolder=$pageID";
						if(@mysql_query($queryDelete))
						{
							if($itemOrder) //check if not empty
							{
								$queryInsert="INSERT INTO submenupages VALUES ";
								
								$items=explode("\n",$itemOrder);
								$itemCounter=0;
								foreach($items as $itemNum=>$item)
								{
									list($itemName,$value)=explode("\t",$item);
									if(substr($value,0,1)=="L") //externalLink (L)
									{
										$externalLink=substr($value,1);
										$pageIDLink="NULL";
									}
									else
									{
										$externalLink="NULL";
										$pageIDLink=$value;
									}
									$externalLink=addslashes($externalLink);
									$itemName=addslashes($itemName);
									$orderNumber=$itemNum+1;
									
									if($pageIDLink)
									{
										$queryInsert.="($pageID,$pageIDLink,'$externalLink','$itemName',$orderNumber),";
										$itemCounter++;
									}
								}
								
								$queryInsert=rtrim($queryInsert,",");
								if($itemCounter==0 || @mysql_query($queryInsert))
								{
									$message.='<p class="success">Submenu data successfully updated ('.$itemCounter.').</p>';
								}
								else
								{
									$message.='<p class="error">There were problems with updating submenu (1)!</p>';
								}
								
							}
							else
							{
								$message.='<p class="success">Submenu data successfully updated (submenu empty).</p>';
							}
						}
						else
						{
							$message.='<p class="error">There were problems with updating submenu (1)!</p>';
						}
					}
					
					
					//SAVE data
					if($_POST["editPage"])
					{	
						//get all data						
						$pageData["textContent"]=$_POST["textContent"];
						$pageData["movieName"]=$_POST["movieName"];
						$pageData["galleryDir"]=$_POST["galleryDir"];
						$pageData["description"]=$_POST["description"];
						
						//make query
						$queryData["textContent"]=$pageData["textContent"]?"'{$pageData["textContent"]}'":"NULL";
						$queryData["movieName"]=$pageData["movieName"]?"'{$pageData["movieName"]}'":"NULL";
						$queryData["galleryDir"]=$pageData["galleryDir"]?"'{$pageData["galleryDir"]}'":"NULL";
						$queryData["description"]=$pageData["description"]?"'{$pageData["description"]}'":"NULL";
						
						$query="UPDATE pages SET textContent={$queryData["textContent"]}, movieName={$queryData["movieName"]}, galleryDir={$queryData["galleryDir"]}, description={$queryData["description"]} WHERE pageID=$pageID LIMIT 1";
						if(@mysql_query($query))
						{
							//do not go back to holder
							$message.='<p class="success">Page <b>'.htmlspecialchars($pageData["filename"]).'</b> successfully updated.</p>';
						}
						else
						{
							$message.='<p class="error">Could not update the subpage (1)! Try again later.</p>';
						}
					}
					
					//GET data
					$query="SELECT textContent,galleryDir,movieName,description FROM pages WHERE pageID=$pageID";
					if($page= @mysql_query($query))
					{
						while($rowPage=@mysql_fetch_array($page,MYSQL_ASSOC))
						{
							foreach($rowPage as $key=>$value)
							{
								$pageData[$key]=$value;
							}
							
							//get mainpage
							$query="SELECT title,titleSubMenuPosition,filename,languageID FROM mainpages WHERE pageID=$pageID";
							if($mainpage= @mysql_query($query))
							{
								while($rowPage=@mysql_fetch_array($mainpage,MYSQL_ASSOC))
								{
									foreach($rowPage as $key=>$value)
									{
										$pageData[$key]=$value;
									}
								}
							}
							else
							{
								$action="show";
								$pageID=null;
								$message.='<p class="error">There was an error while reading mainpages! Try again later.</p>';
							}
						}
					}
					else
					{
						$action="show";
						$pageID=null;
						$message.='<p class="error">There was an error while reading pages! Try again later.</p>';
					}
					
					
				}
				else if($recCounter==0)
				{
					$action="show";
					$message.='<p class="error">The page with pageID='.$pageID.' could not be found!</p>';
					$pageID=null;
				}
			}
			else if($_GET["action"]=="delete")
			{
				$refresh=true;
				$pageType=null; //noType
				
				
				//find pageIDHolder
				$query="SELECT pageIDHolder FROM pages WHERE pageID=$pageID LIMIT 1";
				if($page=@mysql_query($query))
				{
					if($rowPage=@mysql_fetch_array($page,MYSQL_ASSOC))
					{
						$pageData["pageIDHolder"]=$rowPage["pageIDHolder"];
						$pageType=$pageData["pageIDHolder"]?"sub":"main";
					}
					else
					{
						$message.='<p class="error">Deletion failed, page with id='.$pageID.' does not exist!</p>';
					}
				}
				else
				{
					$message.='<p class="error">Could not query!</p>';
				}
				
				
				if($pageType=="main")
				{
					$action="show";
					
					//get language name and filename (needed for deletion)
					$query="SELECT language,filename FROM languages NATURAL JOIN mainpages WHERE pageID=$pageID LIMIT 1";
					if($languageFilename=@mysql_query($query))
					{
						if($row=@mysql_fetch_array($languageFilename,MYSQL_ASSOC))
						{
							$filePath="../{$row["language"]}/{$row["filename"]}";
						}
					}
					
					//unlink the file!
					if(!@unlink($filePath))
					{
						$pageID=null;
						$message.='<p class="error">Could not delete file ('.htmlspecialchars($filePath).')!</p>';
					}
				}
				else if($pageType=="sub")
				{
					$action="edit";
					$pageID=$pageData["pageIDHolder"];
				}
				
				//db recursive deletion
				if($pageType)
				{
					$i = deletePage($pageID);
					if($i!=-1)
					{
						$message.='<p class="success">Deletion of page id='.$pageID. ' successful ('.$i.')</p>';
					}
					else
					{
						$message.='<p class="error">Deletion of page id='.$pageID. ' failed!</p>';
					}
				}
			}
			
			//get galleries
			$galleries=array();
			if($theFiles=getFiles($galleriesPath))
			{
				foreach($theFiles as $galleryDir)
				{
					if(is_file("$galleriesPath/$galleryDir/gallery.info"))
					{
						$galleries[]=$galleryDir;
					}
				}
			}
			//get movies
			$movies=array();
			if($theFiles=getFiles($moviesPath,$fileFilter))
			{
				foreach($theFiles as $movieFile)
				{
					$movieName=pathinfo($movieFile,PATHINFO_FILENAME);
					
					if(is_file("$moviesPath/$movieFile"))
					{
						$movies[]=$movieName;
					}
				}
			}			
			
			//preventing refresh
			if($message && !$_GET["message"] && $refresh)
			{
				$location="$filename?action=$action";
				$location.=$pageID?"&pageID=".urlencode($pageID):"";
				$location.="&message=".urlencode($message);
				header("Location: $location");
			}
		}
		else
		{
			$message.='<p class="error">Database error (2). Try again later.</p>';
		}
	}
	else
	{
		$message.='<p class="error">Database error (1). Try again later.</p>';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin.js"></script>
<script language="Javascript" type="text/javascript" src="edit_area/edit_area_full.js"></script>
<script language="Javascript" type="text/javascript">
	// initialisation
	editAreaLoader.init({
		id: "textContent" // id of the textarea to transform		
		,start_highlight: true
		,allow_resize: "n"
		,font_family: "monospace"
		,font_size: "8"
		,allow_toggle: false
		,language: "en"
		,syntax: "html"
		,show_line_colors: true
		,toolbar: "|,search,fullscreen,|,undo,redo,|,help,|,presentation_select"
		,plugins: "presentation"
	});
</script>
<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Pages manager</h1>

	<?php
		//ako ima bilo koja poruka
		$message=$_GET["message"]?stripslashes($_GET["message"]):$message;
		
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
	<div id="javascriptCheck"><p class="info">Because <b>JavaScript</b> is turned off some functionalities are disabled. Please turn it on and refresh this page.</p><br/></div>
	<script type="text/javascript">
		var javascriptCheck=document.getElementById("javascriptCheck");
		javascriptCheck.innerHTML='';
	</script>
<?php
switch($action)
{
case "add":
?>
	<form  name="addPage" method="post" action="<?php echo $pageData["pageIDHolder"]?"$filename?action=add&pageIDHolder={$pageData["pageIDHolder"]}":"$filename?action=add"; ?>">
<?php if($pageType=="main")
{
?>
	<p>On this page you are adding <b>a new</b> page.</p>
	<br/>
	<h3>Page settings</h3>
	<fieldset style="width:80%">
	<table>
		<tr>
			<td style="width:30%;text-align:right"><label for="filename">Page filename: </label></td>
			<td><input name="filename" type="textbox" size="30" value="<?php echo htmlspecialchars($pageData["filename"]); ?>"/> <a style="font-size:9px">obligatory (e.g. onko.php)</a></td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="title">Title: </label></td>
			<td>
				<textarea rows="3" cols="30" name="title" style="white-space:nowrap"><?php echo htmlspecialchars($pageData["title"]); ?></textarea>
			</td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="titleSubMenuPosition">Title and submenu position: </label></td>
			<td>
				<select name="titleSubMenuPosition">
					<option value="left">left</option>
					<option value="center" <?php echo ($pageData["titleSubMenuPosition"]=="center")?'selected="selected"':''; ?>>center</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="languageID">Language: </label></td>
			<td>
				<select name="languageID">
				<?php
					$query="SELECT * from languages";
					if($languages= @mysql_query($query))
					{
						while($row=@mysql_fetch_array($languages,MYSQL_ASSOC))
						{
							if($row["languageID"]==$pageData["languageID"])
							{
								echo '<option value="'.$row["languageID"].'" selected="selected">'.htmlspecialchars($row["language"]).'</option>';
							}
							else
							{
								echo '<option value="'.$row["languageID"].'">'.htmlspecialchars($row["language"]).'</option>';
							}
						}
					}
				?>
				</select>
			</td>
		</tr>
	</table>
	</fieldset>
	<div style="height:30px;display:block"></div>
<?php
}
else
{
?>
<?php
//draw backward
echo '<p>On this page you are adding <b>a new subpage</b> for: ';
for($i=0;$i<$recCounter-1;$i++)
{
	echo '<a href="page.php?action=edit&pageID='.$backwardIds[$i].'">'.($backwardDescriptions[$i]?$backwardDescriptions[$i]:"pageId={$backwardIds[$i]}").'</a> >>> ';
}
echo '<a href="page.php?action=edit&pageID='.$backwardIds[$i].'">'.($backwardDescriptions[$i]?$backwardDescriptions[$i]:"pageId={$backwardIds[$i]}").'</a>';
echo '.</p>';
?>
	<p><b>Note:</b> After adding the subpage you have to add it as a new item in submenu.</p>
	<div style="height:30px;display:block"></div>
<?php
}
?>
	<h3>General settings</h3>
	<fieldset style="width:100%">
	<label for="textContent">Text content: </label><br/><br/>
	<textarea name="textContent" id="textContent" style="width:100%;height:300px"><?php echo $pageData["textContent"]; ?></textarea>
	<br/>
	<br/>
	<table>
		<tr>
			<td style="width:30%;text-align:right"><label for="movieName">Movie: </label></td>
			<td>
				<select name="movieName">
					<option value="">None</option>
				<?php
					foreach($movies as $movie)
					{
						if($movie==$pageData["movieName"])
						{
							echo '<option value="'.htmlspecialchars($movie).'" selected="selected">'.htmlspecialchars($movie).'</option>';
						}
						else
						{
							echo '<option value="'.htmlspecialchars($movie).'">'.htmlspecialchars($movie).'</option>';
						}
					}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="galleryDir">Gallery: </label></td>
			<td>
				<select name="galleryDir">
					<option value="">None</option>
				<?php
					foreach($galleries as $galleryDir)
					{
						if($galleryDir==$pageData["galleryDir"])
						{
							echo '<option value="'.htmlspecialchars($galleryDir).'" selected="selected">'.htmlspecialchars($galleryDir).'</option>';
						}
						else
						{
							echo '<option value="'.htmlspecialchars($galleryDir).'">'.htmlspecialchars($galleryDir).'</option>';
						}
					}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="description">Description: </label></td>
			<td>
				<textarea rows="2" cols="30" name="description" style="white-space:nowrap"><?php echo htmlspecialchars($pageData["description"]); ?></textarea>
				<a style="font-size:9px">short description for this page (not obligatory)</a>
			</td>
		</tr>
	</table>
	</fieldset>
	<br/><input type="submit" name="addPage" value="Save"/> <input type="button" value="Return" onclick="document.location='<?php echo $pageData["pageIDHolder"]?"$filename?action=edit&pageID={$pageData["pageIDHolder"]}":"$filename"; ?>'"/>
	</form>
	
<?php
break;
case "edit":
?>
	<form  name="editPage" id="editPage" method="post" action="<?php echo "$filename?action=edit&pageID=$pageID&pageIDHolder={$pageData["pageIDHolder"]}"; ?>">
<?php
//draw backward
echo '<p>On this page you are editing: ';
for($i=$recCounter-1;$i>0;$i--)
{
	echo '<a href="page.php?action=edit&pageID='.$backwardIds[$i].'">'.($backwardDescriptions[$i]?$backwardDescriptions[$i]:"pageId={$backwardIds[$i]}").'</a> <<< ';
}
echo '<b><a>'.($backwardDescriptions[$i]?$backwardDescriptions[$i]:"pageId={$backwardIds[$i]}").'</a></b>';
echo '.</p>';
?>
	<p>You may <a href="<?php echo "$filename?action=delete&pageID=".urlencode($pageID); ?>" onclick="linkConfirm('Are you sure you want to delete ENTIRE page with ALL its subpages?',this.href);return false">delete</a> this page with all its subpages.<p>
	<p>Press <b>Save</b> button when you are done.</p>
	<br/>
<?php
if($pageType=="main")
{
?>
	<h3>Page settings</h3>
	<fieldset style="width:80%">
	<table>
		<tr>
			<td style="width:30%;text-align:right"><label for="filename">Page filename: </label></td>
			<td><input name="filename" type="textbox" size="30" value="<?php echo htmlspecialchars($pageData["filename"]); ?>"/> <a style="font-size:9px">obligatory (e.g. onko.php)</a></td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="title">Title: </label></td>
			<td>
				<textarea rows="3" cols="30" name="title" style="white-space:nowrap"><?php echo htmlspecialchars($pageData["title"]); ?></textarea>
			</td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="titleSubMenuPosition">Title and submenu position: </label></td>
			<td>
				<select name="titleSubMenuPosition">
					<option value="left">left</option>
					<option value="center" <?php echo ($pageData["titleSubMenuPosition"]=="center")?'selected="selected"':''; ?>>center</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="languageID">Language: </label></td>
			<td>
				<select name="languageID" disabled="disabled">
				<?php
					$query="SELECT * from languages";
					if($languages= @mysql_query($query))
					{
						while($row=@mysql_fetch_array($languages,MYSQL_ASSOC))
						{
							if($row["languageID"]==$pageData["languageID"])
							{
								echo '<option value="'.$row["languageID"].'" selected="selected">'.htmlspecialchars($row["language"]).'</option>';
							}
							else
							{
								echo '<option value="'.$row["languageID"].'">'.htmlspecialchars($row["language"]).'</option>';
							}
						}
					}
				?>
				</select> <a style="font-size:9px"><b>Note:</b> You cannot change the language, you can only delete the page and recreate it.</a>
			</td>
		</tr>
	</table>
	</fieldset>
	<div style="height:30px;display:block"></div>
<?php
}
?>
	<h3>General settings</h3>

	<fieldset style="width:100%">
	<label for="textContent">Text content: </label><br/><br/>
	<textarea name="textContent" id="textContent" style="width:100%;height:300px"><?php echo $pageData["textContent"]; ?></textarea>
	<br/>
	<br/>
	<table>
		<tr>
			<td style="width:30%;text-align:right"><label for="movieName">Movie: </label></td>
			<td>
				<select name="movieName">
					<option value="">None</option>
				<?php
					foreach($movies as $movie)
					{
						if($movie==$pageData["movieName"])
						{
							echo '<option value="'.htmlspecialchars($movie).'" selected="selected">'.htmlspecialchars($movie).'</option>';
						}
						else
						{
							echo '<option value="'.htmlspecialchars($movie).'">'.htmlspecialchars($movie).'</option>';
						}
					}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="galleryDir">Gallery: </label></td>
			<td>
				<select name="galleryDir">
					<option value="">None</option>
				<?php
					foreach($galleries as $galleryDir)
					{
						if($galleryDir==$pageData["galleryDir"])
						{
							echo '<option value="'.htmlspecialchars($galleryDir).'" selected="selected">'.htmlspecialchars($galleryDir).'</option>';
						}
						else
						{
							echo '<option value="'.htmlspecialchars($galleryDir).'">'.htmlspecialchars($galleryDir).'</option>';
						}
					}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="width:30%;text-align:right"><label for="description">Short page description: </label></td>
			<td>
				<textarea rows="2" cols="30" name="description" style="white-space:nowrap"><?php echo htmlspecialchars($pageData["description"]); ?></textarea>
				<a style="font-size:9px">(useful when adding items for submenu)</a>
			</td>
		</tr>
	</table>
	</fieldset>
<?php //SUBPAGES AND SUBMENU
if($recCounter<3) //Adjust recursion level!!!
{
?>
	<div style="height:30px;display:block"></div>
	<h3>Subpages</h3>
	<p>Below is the list of existing subpages for this page. You may edit them by clicking on their names.</p;			
	<fieldset>
	<?php
	//get only subpages for pageID
	$subpagesArray=array();
	$query="SELECT pageID,description FROM pages WHERE pageIDHolder=$pageID"; //only subpages for this page
	if($subpages= @mysql_query($query))
	{
		if(mysql_num_rows($subpages)>0)
		{
			echo '<ul>'."\n";
			while($row=@mysql_fetch_array($subpages,MYSQL_ASSOC))
			{
				$visible=$row["description"]?substr($row["description"],0,100)." (pageID={$row["pageID"]})":"pageID=".$row["pageID"];
				$subpagesArray[$row["pageID"]]=$visible;
				echo '<li><a href="'.$filename.'?action=edit&pageID='.$row["pageID"].'">'.$visible.'</a></li>'."\n";
			}
			echo '</ul>'."\n";
		}
		else
		{
			echo '<p class="info">No subpages yet.</p>';
		}
	}
	?>
	
	<p>You may <a href="<?php echo "$filename?action=add&pageIDHolder=$pageID"; ?>">add another subpage here</a>.</p>
	</fieldset>
	<div style="height:30px;display:block"></div>
	<h3>Submenu settings</h3>
	<p>Below you may add submenu item for the existing subpages or for some external link.</p>	
	<?php
		//generate editableDIV
		$menuEditDiv='';
		$menuEditDiv.='<table>
							<tr>
								<td style="text-align:right"><label for="itemName">Item name: </label></td>
								<td><input name="itemName" type="text" size="30" value=""/></td>
							</tr>
							<tr>
								<td style="text-align:right"><label for="pageSelect">Page: </label></td>
								<td>
						<select name="pageSelect">
							<option value="-1">Choose page...</option>'."\n".
							'<option value="'.$pageID.'">This page</option>'."\n";
		foreach($subpagesArray as $pageNumb=>$pageName)
		{
			$menuEditDiv.='<option value="'.$pageNumb.'">'.htmlspecialchars($pageName).'</option>'."\n";
		}
		//watch out when moving externalLink option (948859)
		$menuEditDiv.='		<option value="">Use external link...</option>
						</select>
						<br/><input name="externalLink" type="text" value=""/>
						</td>
						</tr>
						<tr>
						<td></td>
						<td>
							<div></div>					
						</td>
						</tr>
						</table>'."\n";
						
	$menuItems=array(); $count=0;
	$query="SELECT pageIDLink AS pageID,itemName,externalLink,description FROM submenupages, pages WHERE (pageIDLink=pageID OR pageIDLink IS NULL) AND submenupages.pageIDHolder=$pageID GROUP BY orderNumber ORDER BY orderNumber";
	if($menu=@mysql_query($query))
	{
		while($rowMenu=@mysql_fetch_array($menu,MYSQL_ASSOC))
		{
			$menuItems[$count]["pageID"]=$rowMenu["pageID"];
			$menuItems[$count]["externalLink"]=stripcslashes($rowMenu["externalLink"]);
			$menuItems[$count]["itemName"]=stripslashes($rowMenu["itemName"]);
			$count++;
		}
	}		
	$selectAreaSize=(count($menuItems)>7)?count($menuItems):7;
	
	?>
	<fieldset name="editFieldset" style="width:80%">
	<table>
		<tr><td>
			[ &nbsp;<a href="#">up</a>&nbsp; | &nbsp;<a href="#">down</a>&nbsp; | &nbsp;<a href="#">remove</a>&nbsp; | &nbsp;<a href="#">add</a>&nbsp; ]<br/><br/>
			<select name="selectArea" id="selectArea" style="width:300px" size="<?php echo $selectAreaSize; ?>">
	<?php
	foreach($menuItems as $orderNumber=>$properties)
	{
		$value=$properties["pageID"]?$properties["pageID"]:"L".$properties["externalLink"]; //if pageID is null then externalLink (L)
		echo '<option value="'.htmlspecialchars($value).'">'.htmlspecialchars($properties["itemName"]).'</option>'."\n";
	}	
	?>
			</select>
			<input type="hidden" name="itemOrder" value=""/>
			<input type="hidden" name="javascriptCheck" value=""/>
		</td>
		<td style="width:10px"></td>
		<td>
			<div style="border:1px solid #bbbbbb;padding:5px;visibility:hidden"><?php echo $menuEditDiv; ?></div>
		</td>
		</tr>
		</table>		
	</fieldset>
	
	<script type="text/javascript">
		var indexEdited=Array();
		var editPage=document.getElementsByName("editFieldset")[0];
		
		var selectArea=editPage.getElementsByTagName("select")[0]; 
		var menuEditDiv=editPage.getElementsByTagName("div")[0];
		var itemOrder=editPage.getElementsByTagName("input")[0];
		indexEdited[0]=-1;
		menuEditDiv.getElementsByTagName("select")[0].onchange= new Function("showHideExternal(menuEditDiv); return false;");
					
		editPage.getElementsByTagName("a")[0].onclick= new Function("moveUp(selectArea,indexEdited,0); return false;");
		editPage.getElementsByTagName("a")[1].onclick= new Function("moveDown(selectArea,indexEdited,0); return false;");
		editPage.getElementsByTagName("a")[2].onclick= new Function("removeItem(selectArea,menuEditDiv,indexEdited,0); return false;");
		editPage.getElementsByTagName("a")[3].onclick= new Function("addItem(selectArea,menuEditDiv,indexEdited,0); return false;");
		
		selectArea.onclick= new Function("editItem(selectArea,menuEditDiv,indexEdited,0); return false;");
		
		document.getElementById("editPage").onsubmit= new Function("saveItems(selectArea,menuEditDiv,itemOrder,indexEdited,0);");
	
		editPage.getElementsByTagName("input")[1].value="yes"; //javascript check
	</script>
<?php
}
?>
	<br/><input type="submit" name="editPage" value="Save"/> <input type="button" value="Return" onclick="document.location='<?php echo ($recCounter>1)?"$filename?action=edit&pageID={$backwardIds[1]}":"$filename"; ?>'"/>
	</form>	
<?php
break;
case "show":
default:
?>
	<br/>
	<p>This page <b>manager</b> enables you to manage all <b>pages and subpages</b>, <b>except</b> for <a href="files.php?action=edit&file=index.php">main index.php</a>, and <a href="files.php?action=edit&file=pl%2Fpytanie.php">pl/pytanie.php</a> (click on their names to manage them as source files).</p>
	<p>Below there are displayed all <b>main</b> pages grouped by language. You may add a <b>new main</b> page or <b>edit</b> existing ones.</p>
	<br/>
	
	<ul>
<?php

$query="SELECT * FROM languages";
if($languages= @mysql_query($query))
{
	//display languages
	if(@mysql_num_rows($languages)>0)
	{
		echo '<ul>'."\n";
		while($rowLang=@mysql_fetch_array($languages,MYSQL_ASSOC))
		{
			echo '<li><h3>Language: '.$rowLang["language"].'</h3></li>'."\n";
			//display pages
			$query="SELECT pageID,filename,title,language FROM mainpages NATURAL JOIN languages WHERE languageID={$rowLang["languageID"]}";
			if($mainpages= @mysql_query($query))
			{
				echo '<ul>'."\n";
				
				//new page
				echo '<li><a href="'.htmlspecialchars($filename).'?action=add&languageID='.htmlspecialchars($rowLang["languageID"]).'" style="border:1px solid orange"><b>Add new</b> '.$rowLang["language"].' page</a></li>'."\n";
				
				//existing pages
				while($rowPage=@mysql_fetch_array($mainpages,MYSQL_ASSOC))
				{
					echo '<li><a href="'.htmlspecialchars($filename).'?action=edit&pageID='.$rowPage["pageID"].'"><b>'.htmlspecialchars($rowPage["filename"]).'</b> ("'.htmlspecialchars($rowPage["title"]).'")</a></li>'."\n";
				}
				echo '</ul>'."\n";
			}
			else
			{
				echo '<p class="error">Database error (3,pages). Try again later.</p>';
			}
		}
		echo '</ul>'."\n";
	}
	else
	{
		echo '<p class="error">No languages found!</p>'."\n";
	}
}
else
{
	echo '<p class="error">Database error (3,lang). Try again later.</p>';
}
?>
<?php
break;
}
@mysql_close($connection);
?>
	<ul>
	
</div>

<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">PagesManager</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>