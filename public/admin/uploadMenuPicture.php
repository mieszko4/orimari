<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a page for AdminPages needed for instant uploading file (method using iframes and javascript)
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2010.07.26

include("imageSmartResize.php");
$target_path = "../photos/menu/";

$maxWidth=180; //resizing in pixels for pictures 
$maxHeight=120;
$compressionLevel=95; //0->100

$picture=$_FILES["menuPicture"];


$count = count(glob($target_path."*"));
$src="";

if(is_uploaded_file($picture['tmp_name']))
{

	list($width_orig, $height_orig, $type_orig) = getimagesize($picture['tmp_name']);
	if(in_array($type_orig,array(IMAGETYPE_GIF,IMAGETYPE_JPEG,IMAGETYPE_PNG)))
	{
		if($maxWidth>$width_orig) $maxWidth=$width_orig;
		if($maxHeight>$height_orig) $maxHeight=$height_orig;
		
		$image_p=smart_resize_image2($picture["tmp_name"],$maxWidth,$maxHeight,true,'return',false,false);
		
		$src=$target_path;
		$src.=($_GET['src']=='NULL')?"pic_$count.jpg":$_GET['src'];
		$tmp=explode('?',$src,2);
		$src=$tmp[0];
		if(!imagejpeg($image_p,$src,$compressionLevel))
		{
			$src='';
		}
	}
	unlink($picture["tmp_name"]);
}					
?>

<script>
	parent.document.getElementById("uploadedPicture").src = '<?php echo $src.'?'.time()?>';
	//unblock here
	parent.isBlock=false;
</script>