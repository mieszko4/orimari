<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: On this page you may manage AdobeFlash movie files with a picture for showing them before they load(folder $serverName/movies) and you may add information abou width and height of the movie player
//Note (1): You may want to delete/change all CHMOD() according to your server options
//Note (2): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.23
//Update: 2009.03.05 movie info added
//Update: 2009.05.27 correction for picture resize a picture

include("login_check.php");

include("imageSmartResize.php");
include("functions.php");
$moviesFolder="movies"; //folder to store movies

$moviesPath="../$moviesFolder";
if(!is_dir($moviesPath))
{
	if(!mkdir($moviesPath))
	{
		header("Location: index.php?message=".urlencode('<p class="error">Could not create <b>'.$moviesFolder.'</b>!</p>'));
	}
	chmod($moviesPath,0777);
}

$widthDef=330; //default values for movie player
$heightDef=310;
$maxWidth=$widthDef+50; //resizing in pixels for pictures 
$maxHeight=$heightDef+50;
$separator=">";
$warningSize=50*1048576; //in bytes
$fileFilter=array("flv"); //for all types use null
$prefix="movie"; //used to mark files in tmp folder
$compressionLevel=95; //0->100
?>
<?
	$message="";
	$movieFile=stripslashes($_GET["movieFile"]);
	$movieName=pathinfo($movieFile,PATHINFO_FILENAME);
	$movieExt=strtolower(pathinfo($movieFile,PATHINFO_EXTENSION));
	
	$picture=is_file("$moviesPath/$movieName.jpg"); //needed for edit pictureBox
	$tmpFile=getFromTemp($prefix);
	
	
	if($_GET["action"]=="edit")
	{
		$action="edit";
		
		if(is_file("$moviesPath/$movieFile"))
		{
			//save changes
			if($_POST["editMovie"])
			{
				$movieFileNew=stripslashes($_POST["movieFileNew"]);
				$movieExtNew=strtolower(pathinfo($movieFileNew,PATHINFO_EXTENSION));
				//ignore extension
				$movieNameNew=($movieExt==$movieExtNew)?pathinfo($movieFileNew,PATHINFO_FILENAME):$movieFileNew;
				$movieFileNew="$movieNameNew.$movieExt";
				
				if(!is_file("$moviesPath/$movieFileNew") || $movieName==$movieNameNew)
				{
					//addInfo
					if($fh=fopen("$moviesPath/$movieName.info","w"))
					{
						$width=htmlspecialchars($_POST["width"]);
						$height=htmlspecialchars($_POST["height"]);
						$maxWidth=$width?$width:$maxWidth;
						$maxHeight=$height?$height:$maxHeight;
						
						$plInfo=htmlspecialchars($_POST["plInfo"]);
						$enInfo=htmlspecialchars($_POST["enInfo"]);
						
						if($width) fwrite($fh,"width{$separator}$width\r\n");
						if($height) fwrite($fh,"height{$separator}$height\r\n");
						if($plInfo) fwrite($fh,"pl-info{$separator}$plInfo\r\n");
						if($enInfo) fwrite($fh,"en-info{$separator}$enInfo\r\n");
						
						fclose($fh);
						$message.='<p class="success">Info file updated.</p>';
					}
					else
					{
						$message.='<p class="error">Could not update info file.</p>';
					}
				
					//add or delete photo 
					if($_POST["radioPicture"]=="delete")
					{
						if(@unlink("$moviesPath/$movieName.jpg"))
						{
							$message.='<p class="success">Picture removed.</p>';
						}
						else
						{
							$message.='<p class="error">Could not remove the picture.</p>';
						}
					}				
					else if($_POST["checkboxPicture"] || $_POST["radioPicture"]=="change")
					{
						$picture=$_FILES["picture"];
						if(is_uploaded_file($picture['tmp_name']))
						{
						
							list($width_orig, $height_orig, $type_orig) = getimagesize($picture['tmp_name']);
							if(in_array($type_orig,array(IMAGETYPE_GIF,IMAGETYPE_JPEG,IMAGETYPE_PNG)))
							{
								if($maxWidth>$width_orig) $maxWidth=$width_orig;
								if($maxHeight>$height_orig) $maxHeight=$height_orig;
								
								$image_p=smart_resize_image($picture["tmp_name"],$maxWidth,$maxHeight,true,'return',false,false);
								
								if(imagejpeg($image_p,"$moviesPath/$movieName.jpg",$compressionLevel))
								{
									$message.='<p class="success">New picture added.</p>';
								}
							}
							else
							{
								$message.='<p class="error">Specified file is not a valid picture file.</p>';
							}
							unlink($picture["tmp_name"]);
						}
						else
						{
							$message.='<p class="error">Could not add new picture!</p>';
						}
					}
					
					//rename
					if($movieName!=$movieNameNew)
					{
						if(rename("$moviesPath/$movieFile","$moviesPath/$movieFileNew"))
						{
							if(is_file("$moviesPath/$movieName.jpg"))
							{
								if(rename("$moviesPath/$movieName.jpg","$moviesPath/$movieNameNew.jpg"))
								{
									if(is_file("$moviesPath/$movieName.info"))
									{
										if(rename("$moviesPath/$movieName.info","$moviesPath/$movieNameNew.info"))
										{
											$message.='<p class="success">Movie renamed.</p>';
										}
										else
										{
											$message.='<p class="error">There were problems with renaming movie (info)!</p>';
										}
									}
								}
								else
								{
									$message.='<p class="error">There were problems with renaming movie (picture)!</p>';
								}
							}
							
							//ADDITIONAL changing names for all pages using old movieName
							include("db_data.php");
							if($connection=@mysql_connect($server,$username,$password))
							{
								$db_select= @mysql_select_db($database);
								mysql_query("SET CHARACTER SET 'utf8'");
								if($db_select)
								{	
									//update personal data
									$query ="UPDATE pages SET movieName='$movieNameNew' WHERE movieName='$movieName'";
									if(@mysql_query($query))
									{
										$message.='<p class="info">Movie name succesfully refreshed in all pages.</p>';
									}
								}
							}
							$movieFile=$movieFileNew;
						}
						else
						{
							$message.='<p class="error">There were problems with renaming movie!</p>';
						}
					}
				}
				else
				{
					$message.='<p class="error">Movie <b>'.$movieFileNew.'</b> exists already!</p>';
				}
			}
			
			//get info file
			if(is_file("$moviesPath/$movieName.info"))
			{
				$lines=file("$moviesPath/$movieName.info");
				$movieInfo=null;
				foreach($lines as $line)
				{
					list($property,$value)=split($separator,rtrim($line));
					$property=htmlspecialchars_decode($property);
					$value=htmlspecialchars_decode($value);
					$movieInfo[$property]=$value;
				}
			}
			
		}
		else
		{
			$action="show";
			$message.='<p class="error">Movie <b>'.$movieFile.'</b> not found!</p>';
		}
	}
	else if($_GET["action"]=="delete")
	{
		$action="show";
		
		if($_GET["tmpFile"]=="yes")
		{
			if(deleteTmpFile($prefix))
			{
				$tmpFile=null;
				$message.='<p class="success">Temporary file deleted.</p>';
			}
			else
			{
				$message.='<p class="error">Could not delete temporary file! There is no possibility to upload new files until the temporary file is deleted or renamed!</p>';
			}
		}
		else if(is_file("$moviesPath/$movieFile"))
		{
			if(unlink("$moviesPath/$movieFile"))
			{
				$movieFile=null;
				$message.='<p class="success">Movie deleted.</p>';
				
				if(is_file("$moviesPath/$movieName.jpg") && unlink("$moviesPath/$movieName.jpg"))
				{
					$message.='<p class="success">Picture deleted.</p>';
				}
				
				if(is_file("$moviesPath/$movieName.info") && unlink("$moviesPath/$movieName.info"))
				{
					$message.='<p class="success">Info file deleted.</p>';
				}
				
			}
			else
			{
				$message.='<p class="error">Deletion failed! Could not delete the movie</p>';
			}
		}
		else
		{
			$message.='<p class="error">Deletion failed! Movie <b>'.$movieFile.'</b> not found.</p>';
		}
	}
	else if($_POST["addMovie"])
	{
		$action="show";
		
		$movieFile=stripslashes($_POST["movieFile"]);
		$widthDef=$_POST["width"];
		$heightDef=$_POST["height"];
		$maxWidth=$widthDef;
		$maxHeight=$heightDef;
		
		//if there is a temp waiting
		if($tmpFile)
		{
			$movie=array("tmp_name"=>dirname($tmpFile)."/{$prefix}_".basename($tmpFile),"name"=>basename($tmpFile));
		}
		else
		{
			$movie=$_FILES["movie"];
		}
		
		if(is_uploaded_file($movie["tmp_name"]) || is_file($movie["tmp_name"]))
		{
			$movieExt=strtolower(pathinfo($movie["name"],PATHINFO_EXTENSION));
			$movieExtForm=strtolower(pathinfo($movieFile,PATHINFO_EXTENSION));
			//ignore extension
			$movieName=($movieExt==$movieExtForm)?pathinfo($movieFile,PATHINFO_FILENAME):$movieFile;
			$movieFile=$movieName?"$movieName.$movieExt":$movie["name"]; //if not given, use default
			$movieName=pathinfo($movieFile,PATHINFO_FILENAME); //if name changes
		

			if(!$fileFilter || in_array($movieExt,$fileFilter))
			{
				if(!is_file("$moviesPath/$movieFile"))
				{
					//dont use move because of tmp file
					if(@copy($movie["tmp_name"],"$moviesPath/$movieFile")) 
					{
						$message.='<p class="success">Movie added.</p>';
						
						$picture=$_FILES["picture"];
						if(is_uploaded_file($picture["tmp_name"]))
						{
						
							list($width_orig, $height_orig, $type_orig) = getimagesize($picture['tmp_name']);
							
							if(in_array($type_orig,array(IMAGETYPE_GIF,IMAGETYPE_JPEG,IMAGETYPE_PNG)))
							{
								if($maxWidth>$width_orig) $maxWidth=$width_orig;
								if($maxHeight>$height_orig) $maxHeight=$height_orig;
								
								$image_p=smart_resize_image($picture["tmp_name"],$maxWidth,$maxHeight,true,'return',false,false);
								
								if(imagejpeg($image_p,"$moviesPath/$movieName.jpg",$compressionLevel))
								{
									$message.='<p class="success">Picture added.</p>';
								}
							}
							else
							{
								$message.='<p class="error">Specified file is not a valid picture file.</p>';
							}
							unlink($picture["tmp_name"]);
						}
						
						//save info
						if($fh=fopen("$moviesPath/$movieName.info","w"))
						{
							$width=$widthDef;
							$height=$heightDef;
							$plInfo=$_POST['plInfo'];
							$enInfo=$_POST['enInfo'];
							
							if($width) fwrite($fh,"width{$separator}$width\r\n");
							if($height) fwrite($fh,"height{$separator}$height\r\n");
							if($plInfo) fwrite($fh,"pl-info{$separator}$plInfo\r\n");
							if($enInfo) fwrite($fh,"en-info{$separator}$enInfo\r\n");
							
							fclose($fh);
							$message.='<p class="success">Info file updated.</p>';
						}
						else
						{
							$message.='<p class="error">Could not update info file.</p>';
						}
						
						unlink($movie["tmp_name"]);
						$movieFile=null; //dont show in form
					}
					else
					{
						$message.='<p class="error">Could not move movie file to movies folder!</p>';
					}
				}
				else
				{
					$message.='<p class="error">Name <b>'.$movieFile.'</b> already exists! Please change the name.</p>';
					saveToTemp($movie,$prefix);
					$movieFile="{$movieName}_".time()."$movieExt";
				}				
			}
			else
			{
				$movieFile=null; //dont show in form
				$message.='<p class="error">Movie file not uploaded! Allowed file types are: '.implode(", ",$fileFilter).'!</p>';
			}
	
		}
		else
		{
			$message.='<p class="error">No file uploaded!</p>';
		}

	}
	
	//preventing refresh
	if($message && !$_GET["message"])
	{
		$location="$filename?action=$action";
		$location.=$movieFile?"&movieFile=".urlencode($movieFile):"";
		$location.="&message=".urlencode($message);
		header("Location: $location");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin.js"></script>

<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Movies</h1>

	<?php 
		$message=stripslashes($_GET["message"]);
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
<?php
switch($action)
{
case "edit":
?>
	<p>On this page you are managing <b><?php echo $movieName;?></b>.</p>
	<p>You may change the name or add/delete/change the picture.<br/>
	You may also <a href="<?php echo "$filename?action=delete&movieFile=".urlencode($movieFile); ?>" onclick="linkConfirm('Are you sure you want to delete entire movie?',this.href);return false">delete</a> the entire movie.</p>
	
	<form  name="editMovie" method="post" action="<?php echo "$filename?action=edit&movieFile=$movieFile";?>" enctype="multipart/form-data">
		<fieldset>
		<table>
			<tr>
				<td style="width:30%;text-align:right"><label for="movieFileNew">Name: </label></td>
				<td><input name="movieFileNew" type="text" size="20" value="<?php echo htmlspecialchars($movieName); ?>"/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="width">Width: </label></td>
				<td><input name="width" type="text" size="20" value="<?php echo $movieInfo["width"] ?>"/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="height">Height: </label></td>
				<td><input name="height" type="text" size="20" value="<?php echo $movieInfo["height"] ?>"/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="plInfo">PL-Info: </label></td>
				<td><input name="plInfo" type="text" size="20" value="<?php echo $movieInfo["pl-info"] ?>"/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="enInfo">EN-Info: </label></td>
				<td><input name="enInfo" type="text" size="20" value="<?php echo $movieInfo["en-info"] ?>"/></td>
			</tr>
		</table>
		<br/>
	<?php
	if($picture)
	{
	?>
		<table>
			<tr>
				<td style="width:30%;text-align:right">Movie has a picture: </td>
				<td><img src="<?php echo "$moviesPath/$movieName.jpg?".time(); ?>" alt="" style="max-height:50px"/></td>
			</tr>
			<tr>
				<td style="text-align:right">Leave picture: </td>
				<td><input type="radio" name="radioPicture" value="leave" checked="checked" onclick="showHidePictureBox()"/></td>
			</tr>
			<tr>
				<td style="text-align:right">Delete picture: </td>
				<td><input type="radio" name="radioPicture" value="delete" onclick="showHidePictureBox()"//></td>
			</tr>
			<tr>
				<td style="text-align:right">Change picture: </td>
				<td><input type="radio" name="radioPicture" id="checkerPicture" value="change"/></td>
			</tr>		
		</table>
	<?php
	}
	else //no Jpeg
	{
	?>
		<label for="checkboxPicture">Add picture?</label><input id="checkerPicture" name="checkboxPicture[]" type="checkbox"/><br/>
	<?php
	}
	?>
		<div id="pictureBox" style="border: 1px #cccccc dashed; padding:3px">
			<table>
				<tr>
					<td style="text-align:right"><label for="picture">New picture: </label></td>
					<td><input type="file" name="picture" id="picture"/></td>
				</tr>
			</table>
		</div>
		<script type="text/javascript">
			//connected pictureBox div
			var checkerPicture = document.getElementById("checkerPicture");
			var pictureBox = document.getElementById("pictureBox");
			
			function showHidePictureBox()
			{
				if(checkerPicture.checked)
				{
					pictureBox.style.visibility="visible";
				}
				else
				{
					pictureBox.style.visibility="hidden";
				}
			}
			showHidePictureBox(); //hide on start
			checkerPicture.onclick=showHidePictureBox;	
		</script>
					
		<br/>
		<input type="submit" name="editMovie" value="Save"/> <input type="button" value="Return" onclick="document.location='<?php echo $filename; ?>'"/>
		</fieldset>
		</form>
<?php
break;
case "show":
default:
?>
	<p>On this page you may see/add/delete <b>Adobe Flash movies</b>.<br/>
	You may also upload a picture which will be shown on the page before any playing of the movie begins.</p>
	<p>Movies are located in folder: <b><?php echo "http://".$_SERVER["SERVER_NAME"]."/".$moviesFolder."/"; ?></b>.<br/>
	<b>Note:</b> The upload limit is 20mb and the movie will <b>not</b> be compressed; please compress the movie before upload and be <b>patient</b> while uploading a huge file!</p>
	<br/>
	
	<table class="display">
		<thead>
		<tr>
			<td>Movie filename</td><td>Static picture and movie properties</td><td>Preview</td>
		</tr>
		</thead>
		<tbody>
			<tr class="addNew">
				<td>Add a new movie:</td>
				<td>
				<form enctype="multipart/form-data" action="<?php echo $filename; ?>" method="post">
					<table>
						<tr>
							<td style="width:30%;text-align:right"><label for="movieFile" <?php echo $tmpFile?'class="info"':""; ?>>Name: </label></td>
							<td><input name="movieFile" type="text" size="20" value="<?php echo $movieName?htmlspecialchars($movieName):($tmpFile?pathinfo($tmpFile,PATHINFO_FILENAME)."_".time():""); ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="movie">Movie: </label></td>
							<td><?php echo $tmpFile?'<p><b>'.basename($tmpFile).'</b></p>':'<input name="movie" type="file" value="Browse..." />'; ?></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="picture">Picture: </label></td>
							<td><input name="picture" type="file" value="Browse..."/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="width">Player width: </label></td>
							<td><input name="width" type="text" value="<?php echo $widthDef; ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="height">Player height: </label></td>
							<td><input name="height" type="text" value="<?php echo $heightDef; ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="plInfo">PL-Info: </label></td>
							<td><input name="plInfo" type="text" value=""/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="enInfo">EN-Info: </label></td>
							<td><input name="enInfo" type="text" value=""/></td>
						</tr>
					</table>
					<br/>
					<input type="submit" name="addMovie" value="Add"/>
				</form>
				</td>
				<td>
					<?php echo $tmpFile?'<p class="info">There is a file on the server which needs to be <b>renamed</b> in order to be saved. <a href="'.$filename.'?action=delete&tmpFile=yes" onclick="linkConfirm(\'Are you sure you want to delete the temporary file?\',this.href);return false">Delete the file?</a></p>':''; ?>
					<p><b>Note: </b> Name is the name of the movie and picture files which will be created on the server. If it is left empty the original name of the movie file will be saved instead.</p>
					<p><b>Picture</b> is the picture shown before any playing begins. It is not obligatory.</p>
				</td>
			</tr>		
<?php
//show all movie files from the folder
	if($theFiles=getFiles($moviesPath,$fileFilter))
	{
		foreach($theFiles as $movieFile)
		{
			$movieName=pathinfo($movieFile,PATHINFO_FILENAME);
			
			if(is_file("$moviesPath/$movieFile"))
			{
				echo "<tr>\n";
				
				echo '<td><a href="'.$filename.'?action=edit&movieFile='.urlencode($movieFile).'"><b>'.$movieName.'</b><br/>[ manage ]</a></td>'."\n";
				
				if(is_file("$moviesPath/$movieName.jpg"))
				{
					echo '<td><img src="'."$moviesPath/$movieName.jpg?".time().'" alt="" style="max-height:150px"/>';
				}
				else
				{
					echo '<td><p class="info">No picture specified for this movie.</p>';
				}
				
				$movieSize=filesize("$moviesPath/$movieFile");
				printf("<p><b>FileSize:</b> %.2fMB </p>\n",$movieSize/1048576);
				
				//SizeWarning
				if($movieSize>$warningSize)
				{
					printf("<p class=\"info\">The movie exceeds %.2fMB! Consider compressing the file and upload it again.</p>\n",$warningSize/1048576);
				}
				
				echo '<p>[<a href="'."$moviesPath/$movieFile".'"> download </a>| <a href="'.$filename.'?action=delete&movieFile='.urlencode($movieFile).'" onclick="linkConfirm(\'Are you sure you want to delete entire movie?\',this.href);return false"> delete </a>]</p><p>'."\n";
				
				if(is_file("$moviesPath/$movieName.info"))
				{
					if($lines=file("$moviesPath/$movieName.info"))
					{
						foreach($lines as $line)
						{
							list($property,$value)=split($separator,rtrim($line));
							$property=htmlspecialchars_decode($property);
							$value=htmlspecialchars_decode($value);
							echo "<b>$property:</b> $value<br/>\n";
						}
					}
				}
				
				echo '</p><td>'; showFlvMovie($movieName,"en",false,250,250); echo'</td>'."\n";
				echo "</tr>\n";
			}
		}	
	}
?>	
		</tbody>
	</table>
<?php
break;
}
?>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]; ?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">MoviesPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>