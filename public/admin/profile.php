<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a profile page for AdminPages; it uses JavaScript for secure password change (possible to use without JavaScript too but not secure)
//Note (1): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.25
include("login_check.php");

$minimumPassLength=6;
?>
<?php
	include("db_data.php");
	if($connection=@mysql_connect($server,$username,$password))
	{
		$db_select= @mysql_select_db($database);
		mysql_query("SET CHARACTER SET 'utf8'");
		if($db_select)
		{
			
			//save changes if any
			if($_POST["editProfile"])
			{
				$name=$_POST["name"];
				$surname=$_POST["surname"];
				$oldpass=$_POST["oldpass"];
				$newpass1=$_POST["newpass1"];
				$newpass2=$_POST["newpass2"];
				
				//update personal data
				$query ="UPDATE usersadmin SET surname='$surname', name='$name' WHERE username='".$_SESSION['usernameAdmin']."' LIMIT 1";
				if(@mysql_query($query))
				{
					$message.='<p class="success">Personal data saved!</p>';
				}
				else
				{
					$message.='<p class="error">Could not change personal data!</p>';
				}
				
				if($_POST["password"])
				{
					if($newpass1==$newpass2 && $_POST["passwordCountCheck"]=="true" && strlen($newpass1)>$minimumPassLength)
					{
						if(strlen($oldpass)==32) //if hashed
						{
							$query = "UPDATE usersadmin SET password='$newpass1' WHERE username='{$_SESSION['usernameAdmin']}' AND password='$oldpass' LIMIT 1";
						}
						else //if javascript not used (not hashed)
						{
							$query = "UPDATE usersadmin SET password='".md5($newpass1)."' WHERE username='{$_SESSION['usernameAdmin']}' AND password='".md5($oldpass)."' LIMIT 1";
						}
						
						@mysql_query($query);
						if(@mysql_affected_rows()>0)
						{
							//security reasons
							header("Location: index.php?action=passchange");
						}
						else
						{
							$message.='<p class="error">Could not change password!</p>';
						}
					}
					else
					{
						$message='<p class="error">New password must be entered twice and has to have at least '.$minimumPassLength.' characters!</p>';
					}
				}
			}
			
			//get data
			$query = "SELECT name,surname FROM usersadmin WHERE username='".$_SESSION["usernameAdmin"]."' LIMIT 1";
			if($user=@mysql_query($query))
			{
				$userAr = mysql_fetch_array($user,MYSQL_ASSOC);
			}
			else
			{
				$message.='<p class="error">Database error (3). Try again later.</p>';
			}
		}
		else
		{
			$message.='<p class="error">Database error (2). Try again later.</p>';
		}
	}
	else
	{
		$message.='<p class="error">Database error (1). Try again later.</p>';
	}
	@mysql_close($connection);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />

<script type="text/javascript" src="md5.js"></script>
<script type="text/javascript">
	function processEdit(editForm)
	{
		if(editForm)
		{
			document.getElementById("passwordBox").style.visibility="hidden";
			
			editForm.passwordCountCheck.value=editForm.newpass1.value.length><?php echo $minimumPassLength; ?>;
			editForm.oldpass.value=hex_md5(editForm.oldpass.value);
			editForm.newpass1.value=hex_md5(editForm.newpass1.value);
			editForm.newpass2.value=hex_md5(editForm.newpass2.value);
			
			return true;
		}
		else
		{
			return false;
		}		
		
	}
</script>

<title>AdminPages Orimari</title>

</head>

<body>

<div class="mainContentDiv">
	<h1>Profile</h1>
	<div id="javascriptCheck">
		<p class="info"><b>Warning!</b> JavaScript is turned off, which makes changing password to be <b>unsafe</b>!</p>
		<p class="info">Please turn on <b>JavaScript</b> and refresh this page.</p><br/>
	</div>
	<script type="text/javascript">
		var javascriptCheck=document.getElementById("javascriptCheck");
		javascriptCheck.innerHTML="";
	</script>
	<?php echo "$message<br/>"; ?>

	<p>On this page you may see your profile settings. If you want to edit some data click on save button later on to complete changes.</p>
	<p>For changing the password you must enter the old one.</p>
	<br/>
		
	<form onsubmit="return processEdit(this)" name="editProfile" method="post" action="<?php echo $filename; ?>">
	<fieldset>
		<table>
			<tr>
				<td style="width:30%;text-align:right"><label for="name">Name: </label></td>
				<td><input name="name" type="text" size="20" value="<?php echo htmlspecialchars($userAr['name']); ?>"/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="surname">Surname: </label></td>
				<td><input name="surname" type="text" size="20" value="<?php echo htmlspecialchars($userAr['surname']); ?>"/></td>
			</tr>
		</table>
		
		<br/>
		<label for="password">Change password?</label><input id="checkboxPass" name="password[]" type="checkbox" value=""/><br/>
		<div id="passwordBox" style="border: 1px #cccccc dashed; padding:3px">
		<table>
			<tr>
				<td style="text-align:right"><label for="oldpass">Old password: </label></td>
				<td><input name="oldpass" type="password" size="20" value=""/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="newpass1">New password: </label></td>
				<td><input name="newpass1" type="password" size="20" value=""/></td>
			</tr>
			<tr>
				<td style="text-align:right"><label for="newpass2">New password (again): </label></td>
				<td><input name="newpass2" type="password" size="20" value=""/></td>
			</tr>
		</table>
		</div>
		<script type="text/javascript">
			//connected with passwordBox div
			var checkboxPass = document.getElementById("checkboxPass");
			var passwordBox = document.getElementById("passwordBox");
			function showHidePasswordBox()
			{
				if(checkboxPass.checked)
				{
					passwordBox.style.visibility="visible";
				}
				else
				{
					passwordBox.style.visibility="hidden";
				}
			}
			showHidePasswordBox(); //hide initially
			checkboxPass.onclick=showHidePasswordBox;
		</script>
		<input name="passwordCountCheck" type="hidden" value="true" />
		<br/>
		<input type="submit" name="editProfile" value="Save"/>
	</fieldset>
	</form>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">ProfilePage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>