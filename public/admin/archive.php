<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a page for AdminPages on which you may archivize the database or see sql existing backup files
//Note (1): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.10.07

include("login_check.php");

include("functions.php");
$bckPath="bck";
if(!is_dir($bckPath))
{
	if(!mkdir($bckPath))
	{
		header("Location: index.php?message=".urlencode('<p class="error">Could not create <b>'.$bckPath.'</b>!</p>'));
	}
	chmod($bckPath,0777);
}
?>
<?php
	
	$file=stripslashes($_GET["filename"]);

	if($_GET["action"]=="download")
	{
		if(is_file("$bckPath/$file"))
		{
			header("Content-type: text/plain; charset=utf-8");
			header('Content-Disposition: attachment; filename="'.$file.'"');
			readfile("$bckPath/$file");
			exit();
		}
		else
		{
			$action="show";
			$message.='<p class="error">Download failed! File <b>'.$file.'</b> not found.</p>';
		}
	}
	else if($_GET["action"]=="delete")
	{
		if(is_file("$bckPath/$file"))
		{
			if(@unlink("$bckPath/$file"))
			{
				$file=null;
				$message.='<p class="success">File deleted.</p>';
			}
			else
			{
				$message.='<p class="error">Deletion failed! Could not delete the file</p>';
			}
		}
		else
		{
			$message.='<p class="error">Deletion failed! file <b>'.$file.'</b> not found.</p>';
		}
	}
	
	//preventing refresh
	if($message && !$_GET["message"])
	{
		$location="$filename";
		$location.="?message=".urlencode($message);
		header("Location: $location");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin.js"></script>
<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Archive</h1>

	<?php
		//ako ima bilo koja poruka
		$message=$_GET["message"]?stripslashes($_GET["message"]):$message;
		
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
	<p>On this page you may make an archive of the database. The arhive will be saved in backup folder as <b>.sql</b> file (utf-8).</p>
	<p>Below you may see and download the <b>.sql</b> files saved in backup folder. You may also delete a file.<p>
	<br/>
<?php	
	if($files=getFiles($bckPath,array("sql")))
	{
		echo '<ul>'."\n";
		foreach($files as $file)
		{
			echo '<li><a href="'."$filename?action=download&filename=".urlencode($file).'">'.$file.'</a>&nbsp; [ <a href="'.$filename.'?action=delete&filename='.urlencode($file).'" onclick="linkConfirm(\'Are you sure you want to delete the file?\',this.href);return false">delete?</a> ]</li>'."\n";
		}
		echo '</ul>'."\n";
	}
	else
	{
		echo '<p class="info">No backup files yet.</p>'."\n";
	}
?>
	<p>You can make a new archive by clicking <a href="<?php echo "backupDB/backupDB.php?StartBackup=complete&nohtml=1"; ?>">here</a>.</p>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">ArchivePage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>