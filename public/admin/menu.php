<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info (1): This is a page for AdminPages on which you can manage main menus (shown on each page)
//Info (2): JavaScript is required, and there are some separators used in order \n\n\n   \t\t   \n\n   \n   \t
//Info (3): externalLink is marked as "L" on the first char, wheras pageID is simply an integer
//Note (1): NO CHECKING IF TABLES >>>mainpages<<< >>>menupages<<< >>>menus<<< EXIST. IT MUST EXIST FOR THE PAGE TO WORK
//Note (2): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.27

include("login_check.php");

include("functions.php");
$separator="-";
?>
<?php
	include("db_data.php");
	if($connection=@mysql_connect($server,$username,$password))
	{
		$db_select= @mysql_select_db($database);
		mysql_query("SET CHARACTER SET 'utf8'");
		if($db_select)
		{	
			$message="";
			$menuID=$_GET["menuID"];
			
			//deletes all menus content, and fills it with new data from post (editSubmit AND editAll),
			function saveMenus($itemOrder,$menuID)
			{
				//check if user has javascript on
				if($_POST["javascriptCheck"]!="yes") return false;
			
				$queryDelete="DELETE FROM menupages WHERE menuID=$menuID";
				if(!@mysql_query($queryDelete))
				{
					return false;
				}
				if($itemOrder) //check if not empty
				{
					$queryInsert="INSERT INTO menupages VALUES ";
					
					$items=explode("\n",$itemOrder);
					$itemCounter=0;
					foreach($items as $itemNum=>$item)
					{
						list($itemName,$value)=explode("\t",$item);
						if(substr($value,0,1)=="L") //externalLink (L)
						{
							$externalLink=substr($value,1);
							$pageID="NULL";
						}
						else
						{
							$externalLink="NULL";
							$pageID=$value;
						}
						$externalLink=addslashes($externalLink);
						$itemName=addslashes($itemName);
						$orderNumber=$itemNum+1;
						
						if($pageID)
						{
							$queryInsert.="($menuID,$pageID,'$externalLink','$itemName',$orderNumber),";
							$itemCounter++;
						}
					}
					
					$queryInsert=rtrim($queryInsert,",");
					if($itemCounter==0 || @mysql_query($queryInsert))
					{
						return true;
					}
					else
					{
						return false;
					}
					
				}
				return true;
			}
		
			if($_POST["editSubmit"])
			{
				$menuID=$_POST["menuID"];
				$itemOrder=stripslashes($_POST["itemOrder"]);
				$itemOrder=strtr($itemOrder,array("\r"=>""));
				
				if(saveMenus($itemOrder,$menuID))
				{
					$message.='<p class="success">Menu data successfully updated</p>';
				}
				else
				{
					$message.='<p class="error">There were problems with updating menu!</p>';
				}
			}
			else if($_POST["editAll"])
			{	
				$itemOrderAll=stripslashes($_POST["itemOrderAll"]);
				$itemOrderAll=strtr($itemOrderAll,array("\r"=>""));
				$menus=explode("\n\n\n",$itemOrderAll);
				
				$error=0;
				
				foreach($menus as $menu)
				{
					list($menuID,$itemOrder)=explode("\t\t",$menu);
					
					$error+=saveMenus($itemOrder,$menuID)?0:1;
				}
				
				if(!$error)
				{
					$message.='<p class="success">'.count($menus).' menus successfully updated</p>';
				}
				else
				{
					$message.='<p class="error">There were problems with updating menus ('.$error.')!</p>';
				}
			}
			
			//preventing refresh
			if($message && !$_GET["message"])
			{
				$location="$filename";
				$location.=$_GET["menuID"]?"?menuID=$menuID&":"?";
				$location.="message=".urlencode($message);
				header("Location: $location");
			}
			
			//if is show (must be after refreh prevention)
			$where=$menuID?"WHERE menuID=$menuID":"";
			$query="SELECT menuID,name,language FROM menus NATURAL JOIN languages $where";
			if(!($menus= @mysql_query($query)))
			{
				$message.='<p class="error">Database error (3). Try again later.</p>';
			}
		}
		else
		{
			$message.='<p class="error">Database error (2). Try again later.</p>';
		}
	}
	else
	{
		$message.='<p class="error">Database error (1). Try again later.</p>';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin-Menu.php.js"></script>
<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Main Menus</h1>

	<?php
		//ako ima bilo koja poruka
		$message=$_GET["message"]?stripslashes($_GET["message"]):$message;
		
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	
	<div id="javascriptCheck"><p class="error">JavaScript MUST be on in order to be able to make ANY changes!</p></div>
	<script type="text/javascript">
		var javascriptCheck=document.getElementById("javascriptCheck");
		javascriptCheck.innerHTML='';
	</script>
	
	<br/>
	<p>On this page you can <b>manage main menus</b> (shown on each page).</p>
	<p>Below is the list of existing menus. For each menu you may <b>see/edit/remove/add</b> menu items. Each menu item consists of the visible on the menu <b>name</b> and the <b>address</b>, which can be chosen from the existing pages or written as some other external link.</p>
	<p>If you want to display only <b>one menu</b> press on its name.</p>
	
	<p><b>Note:</b> Changes made to <b>items</b> are saved <b>automatically</b>, however, when you are done with <b>ALL</b> editing, press the <b>Update</b> button for corresponding menu. If you edit <b>more than one</b> menus at the time</b>, save changes by pressing <b>Update All</b> button. If an item is colored <b>red</b>, it consists of <b>incorrect</b> data and it will be <b>ignored</b> when updating.</p>
	
	<ul>
<?php
	//get only MaiN pages list
	$pagesArray=array();
	$query="SELECT pageID,filename,language FROM mainpages NATURAL JOIN languages ORDER BY pageID";
	if($pages=@mysql_query($query))
	{
		while($row=@mysql_fetch_array($pages,MYSQL_ASSOC))
		{
			$pagesArray[$row["pageID"]]="{$row["filename"]} ({$row["language"]})";
		}
	}
	
	//generate editableDIV
	$menuEditDiv='';
	$menuEditDiv.='<table>
						<tr>
							<td style="text-align:right"><label for="itemName">Item name: </label></td>
							<td><input name="itemName" type="text" size="30" value=""/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="pageSelect">Page: </label></td>
							<td>
					<select name="pageSelect">
						<option value="-1">Choose page...</option>'."\n";
	foreach($pagesArray as $pageNumb=>$pageName)
	{
		$menuEditDiv.='<option value="'.$pageNumb.'">'.htmlspecialchars($pageName).'</option>'."\n";
	}
	//watch out when moving externalLink option (948859)
	$menuEditDiv.='		<option value="">Use external link...</option>
					</select>
					<br/><input name="externalLink" type="text" value=""/>
					</td>
					</tr>
					<tr>
					<td></td>
					<td>
						<div></div>					
					</td>
					</tr>
					</table>'."\n";
	
	
//generating editable menus
	while($row=@mysql_fetch_array($menus,MYSQL_ASSOC))
	{
		echo "<li>\n";
		
		//for each MENU
		$menuItems=array(); $count=0;
		$query="SELECT menupages.pageID,itemName,externalLink,filename FROM languages NATURAL JOIN menus NATURAL JOIN menupages JOIN mainpages WHERE (menupages.pageID=mainpages.pageID OR menupages.pageID IS NULL) AND menupages.menuID={$row["menuID"]} GROUP BY orderNumber ORDER BY orderNumber";
		if($menu=@mysql_query($query))
		{
			while($rowMeni=@mysql_fetch_array($menu,MYSQL_ASSOC))
			{
				$menuItems[$count]["pageID"]=$rowMeni["pageID"];
				$menuItems[$count]["externalLink"]=stripcslashes($rowMeni["externalLink"]);
				$menuItems[$count]["itemName"]=stripslashes($rowMeni["itemName"]);
				$count++;
			}
		}		
		$selectAreaSize=(count($menuItems)>7)?count($menuItems):7;
		
		echo '<h3><a href="'."$filename?menuID={$row["menuID"]}".'">'."{$row["name"]} (<b>{$row["language"]}</b>)".'</a></h3>'."\n";
		
		echo '<form name="editForm" method="post" action="'.($menuID?"$filename?menuID=$menuID":$filename).'">
				<fieldset style="width:70%">
				<table>
					<tr><td>
							[ &nbsp;<a href="#">up</a>&nbsp; | &nbsp;<a href="#">down</a>&nbsp; | &nbsp;<a href="#">remove</a>&nbsp; | &nbsp;<a href="#">add</a>&nbsp; ]<br/><br/>
							<select name="selectArea" id="selectArea" style="width:300px" size="'.$selectAreaSize.'">';
		
		foreach($menuItems as $orderNumber=>$properties)
		{
			$value=$properties["pageID"]?$properties["pageID"]:"L".$properties["externalLink"]; //if pageID is null then externalLink (L)
			echo '<option value="'.htmlspecialchars($value).'">'.htmlspecialchars($properties["itemName"]).'</option>'."\n";
		}		
		
						echo '</select>
								<input type="hidden" name="menuID" value="'.$row["menuID"].'"/>
								<input type="hidden" name="itemOrder" value=""/>
								<input type="hidden" name="javascriptCheck" value=""/>
								<br/>
								<br/>
								<input type="submit" name="editSubmit" value="Update '."{$row["name"]} ({$row["language"]})".'"/>
								<br/>
							</td>
							<td style="width:10px"></td>
							<td>
								<div style="border:1px solid #bbbbbb;padding:5px;visibility:hidden">'.$menuEditDiv.'</div>
										</td>
									</tr>
							</table>
							</fieldset>
							</form>'."\n";
		
		echo "</li>\n";	
	}
?>
	</ul>
<?php
if(!$menuID)
{
?>
	<form name="editAll" method="post" action="<?php echo $filename; ?>">
		<input name="itemOrderAll" type="hidden" value=""/>
		<input name="javascriptCheck" type="hidden" value=""/>
		<input name="editAll" type="submit" value="Update All"/>
	</form>
<?php
}
else
{
?>
	<input type="button" value="Return" onclick="document.location='<?php echo $filename; ?>'"/>
<?php
}
?>
	<script type="text/javascript">
		var indexEdited=Array();
		var isBlock = false;
		var editForm=document.getElementsByName("editForm"); //get more Forms
		var selectArea=Array(); //options
		var menuEditDiv=Array(); //editDiv
		var menuID=Array() //info about menuID
		var itemOrder=Array(); //information to be send
		
		//for each menu, prepare event handling
		for(var i=0;i<editForm.length;i++)
		{
			selectArea[i]=editForm[i].getElementsByTagName("select")[0]; 
			menuEditDiv[i]=editForm[i].getElementsByTagName("div")[0];
			menuID[i]=editForm[i].getElementsByTagName("input")[0];
			itemOrder[i]=editForm[i].getElementsByTagName("input")[1];
			indexEdited[i]=-1;
			
			menuEditDiv[i].getElementsByTagName("select")[0].onchange= new Function("showHideExternal(menuEditDiv["+i+"]); return false;");
						
			editForm[i].getElementsByTagName("a")[0].onclick= new Function("moveUp(selectArea["+i+"],indexEdited,"+i+"); return false;");
			editForm[i].getElementsByTagName("a")[1].onclick= new Function("moveDown(selectArea["+i+"],indexEdited,"+i+"); return false;");
			editForm[i].getElementsByTagName("a")[2].onclick= new Function("removeItem(selectArea["+i+"],menuEditDiv["+i+"],indexEdited,"+i+"); return false;");
			editForm[i].getElementsByTagName("a")[3].onclick= new Function("addItem(selectArea["+i+"],menuEditDiv["+i+"],indexEdited,"+i+"); return false;");
			
			selectArea[i].onclick= new Function("editItem(selectArea["+i+"],menuEditDiv["+i+"],indexEdited,"+i+"); return false;");
			
			editForm[i].onsubmit= new Function("saveItems(selectArea["+i+"],menuEditDiv["+i+"],itemOrder["+i+"],indexEdited,"+i+");");
		
			editForm[i].getElementsByTagName("input")[2].value="yes"; //javascript check
		}
		
		//submiting all at the time
		function saveAll(editForm,editObj,selectArea,indexEdited)
		{
			var editAllForm=document.getElementsByName("editAll")[0];
			var itemAllOrder=editAllForm.getElementsByTagName("input")[0];
			itemAllOrder.value="";
			
			for(var i=0;i<editForm.length;i++)
			{
				menuID=editForm[i].getElementsByTagName("input")[0];
				itemOrderSingle=editForm[i].getElementsByTagName("input")[1];
				saveItems(selectArea[i],editObj[i],itemOrderSingle,indexEdited,i);
				itemAllOrder.value+=menuID.value+"\t\t"+itemOrderSingle.value;
				itemAllOrder.value+=(i!=(editForm.length-1))?"\n\n\n":"";
			}
			editAllForm.getElementsByTagName("input")[1].value="yes"; //javascript check
		}
		
		if(document.getElementsByName("editAll").length!=0)
		{
			document.getElementsByName("editAll")[0].onsubmit=new Function("saveAll(editForm,menuEditDiv,selectArea,indexEdited); return confirm('Are you sure you want to save ALL menus at the time?');");	
		}
	</script>
	
<?php
//close at the end
@mysql_close($connection);
?>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">MenuPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>