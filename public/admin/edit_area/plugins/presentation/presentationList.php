<?php
include("../../../functions.php");

$fileFilter=array("ppt","pptx","pps","ppsx"); //for all types use null
$presentationsPath="../../../../presentations";
$response="";

if($names=getFiles($presentationsPath,$fileFilter))
{
	foreach($names as $name)
	{
		$response.= pathinfo($name,PATHINFO_FILENAME)."\n";
	}
	$response=rtrim($response,"\n");
}
echo $response;
?>