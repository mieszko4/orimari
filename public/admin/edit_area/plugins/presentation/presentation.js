var html="";
//returns browser compatible xmlHttp
function createXmlHttp()
{
	var xmlHttp;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			try
			{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
				return false;
			}
		}
	}
	return xmlHttp;
}

var EditArea_presentation= { 	 	
	init: function(){
	}
	
	,get_control_html: function(ctrl_name){
		switch(ctrl_name){
			case "presentation_select":
				var html= "<select id='presentation_select' onchange='javascript:editArea.execCommand(\"presentation_select_change\")' fileSpecific='no'>\n"
					+"<option value='-1'>{$presentation_select}</option>\n"
					+"</select>\n";
				
				var xmlHttp=createXmlHttp();

				function checkState()
				{
					if(xmlHttp.readyState==4)
					{
						var presentationSelect=document.getElementById("presentation_select");
						
						var names = xmlHttp.responseText.split("\n");
						for(var i=0;i<names.length;i++)
						{
							optionElement=document.createElement('option');
							document.all?presentationSelect.add(optionElement,i+1):presentationSelect.add(optionElement,presentationSelect[i+1]);
							presentationSelect[i+1].text=names[i];
							presentationSelect[i+1].value=names[i];
						}
					}
				}
				
				xmlHttp.onreadystatechange = checkState;				
				xmlHttp.open("GET","edit_area/plugins/presentation/presentationList.php",true);
				xmlHttp.send(null);

				return html;
		}
		return false;
	}
	
	,onload: function(){
	}
	
	,execCommand: function(cmd, param){
		switch(cmd)
		{
			case "presentation_select_change":
				var presentationSelect= document.getElementById("presentation_select");
				var selection = Array();
				var presentationsPath="../presentations";
				
				if(presentationSelect.value!=-1)
				{
					var response="";
					var textAreaValue=parent.editAreaLoader.getValue(editArea.id);
					
					var presentation='<a href="'+presentationsPath+'/'+presentationSelect.value+'.ppt">';
					presentation +='<img src="'+presentationsPath+'/'+presentationSelect.value+'.jpg"';
					presentation +='alt="Download presentation" style="border-style: solid; width: 200px;"/></a>';
					selection = parent.editAreaLoader.getSelectionRange(editArea.id);
					
					response+=textAreaValue.substring(0,selection["start"]);
					response+=presentation;
					response+=textAreaValue.substring(selection["end"]);
					
					parent.editAreaLoader.setValue(editArea.id,response); //set new text
					parent.editAreaLoader.setSelectionRange(editArea.id, selection["start"], selection["start"]+presentation.length);
					presentationSelect.options[0].selected=true;					
				}
				return false;
		}
		// Pass to next handler in chain
		return true;
	}
};

editArea.add_plugin("presentation", EditArea_presentation);
