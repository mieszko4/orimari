<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a login page for AdminPages; it uses JavaScript for secure login (possible to login without JavaScript too but not secure); requires table >>>usersadmin<<<
//Note (1): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.10.07

$filename=basename($_SERVER["SCRIPT_NAME"]);
if($filename=="admin")
{
	$filename="index.php";
}
?><?php
session_start();
if($_GET["action"]=="logout" && isset($_SESSION["usernameAdmin"]))
{
	session_unset();
	session_destroy();
	
	session_start(); //after loging out
	$message.='<p class="success">You have successfully logOut.</p>';
}
else if($_GET["action"]=="passchange" && isset($_SESSION["usernameAdmin"]))
{
	session_unset();
	session_destroy();
	
	session_start(); //after loging out
	$message.='<p class="success">Password succesfully changed! Please login again with your new password.</p>';
}
else if($_POST["loginSubmit"])
{
	$challenge=$_SESSION["challenge"];
	
	include("db_data.php");
	if($connection = @mysql_connect($server,$username,$password))
	{
		$db_select= @mysql_select_db($database);
		mysql_query("SET CHARACTER SET 'utf8'");
		if($db_select)
		{
			$username=htmlentities($_POST["username"],ENT_QUOTES,"utf-8");
			$password=htmlentities($_POST["password"],ENT_QUOTES,"utf-8");
			
			$authorized=false;
			if(strlen($password)==32) //if hashed
			{
				$query = "SELECT * FROM usersadmin WHERE MD5(CONCAT('$challenge',username))='$username' AND MD5(CONCAT('$challenge',password))='$password'";
			}
			else //if javascript not used (not hashed)
			{
				$query = "SELECT * FROM usersadmin WHERE username='$username' AND password='".md5($password)."'";
			}
			
			if($users=@mysql_query($query))
			{
				if($row=@mysql_fetch_array($users,MYSQL_ASSOC))
				{
					$_SESSION["usernameAdmin"]=$row["username"];
					header("Location: $filename");
				}
				else
				{
					session_unset();
					$message.='<p class="error">Username and password not valid!</p>';
				}
			}
			else
			{
				$message.='<p class="error">Database error (3). Try again later.</p>';
			}
		}
		else
		{
			$message.='<p class="error">Database error (2). Try again later.</p>';
		}
	}
	else
	{
		$message.='<p class="error">Database error (1). Try again later.</p>';
	}
	@mysql_close($connection);
}
//generate challenge
if(empty($_SESSION["usernameAdmin"]))
{
	session_unset();

	srand();
	$_SESSION["challenge"]="";
	for($i=0;$i<80;$i++)
	{
		$_SESSION["challenge"].=dechex(rand(0,15));
	}
}

//check redirection
if($_SESSION["usernameAdmin"] && $_GET["redirect"])
{

	header("Location: ".$_GET["redirect"]);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="md5.js"></script>
<script type="text/javascript">
	var challenge='<?php echo $_SESSION["challenge"]; ?>'; //generated on server
	function processLogin(loginForm)
	{
		if(loginForm)
		{
			loginForm.username.style.visibility="hidden";
			loginForm.password.style.visibility="hidden";
			
			loginForm.username.value=hex_md5(challenge+loginForm.username.value);
			loginForm.password.value=hex_md5(challenge+hex_md5(loginForm.password.value));
			
			return true;
		}
		else
		{
			return false;
		}		
		
	}
</script>

<title>Orimari AdminPages<?php echo empty($_SESSION["usernameAdmin"])?" - Login":""; ?></title>
</head>

<body>
<?php
if(empty($_SESSION["usernameAdmin"]))
{
?>
	<div class="centerDivOut">
		<div class="centerDivIn">
			<div class="loginDiv">
				<p style="font-size:20px">Welcome to <a style="color:orange">Orimari</a> AdminPages!</p>
				<br/>			
				<div id="javascriptCheck">
					<p class="info"><b>Warning!</b> JavaScript is turned off, which makes this login to be <b>unsecured</b>!</p>
					<p class="info">Please turn on <b>JavaScript</b> and refresh this page.</p><br/>
				</div>
				<script type="text/javascript">
					var javascriptCheck=document.getElementById("javascriptCheck");
					javascriptCheck.innerHTML="";
				</script>
				<?php echo $message; ?>
				<p>Please enter your username and password.</p>				
				<form onsubmit="return processLogin(this)" method="post" action="<?php echo $_GET["redirect"]?"$filename?redirect={$_GET["redirect"]}":"$filename" ;?>">
				<div class="centerDivOut">
					<table id="loginTable">
						<tr>
							<td style="width:30%;text-align:right"><label for="username">Username: </label></td>
							<td><input name="username" type="text" size="20" value=""/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="password">Password: </label></td>
							<td><input name="password" type="password"  size="20" value=""/></td>
						</tr>
					</table>
					<br/>
					<input type="submit" name="loginSubmit" value="Login"/>
				</div>
				</form>
			</div>
		</div>
	</div>
<?php
}
else
{
?>
	<div class="mainContentDiv">
		<h1>Welcome <a style="color:#00aa00"><?php echo $_SESSION["usernameAdmin"]; ?></a>!</h1>
		
		<div id="javascriptCheck" class="error">
			<p class="info"><b>Warning!</b> JavaScript is turned off; some options will not be enabled. Please turn on <b>JavaScript</b>.</p>
		</div>
		<script type="text/javascript">
			var javascriptCheck=document.getElementById("javascriptCheck");
			javascriptCheck.innerHTML="";
		</script>	
		
		<br/>
		<p>Your options are:</p>
		<ul>		
			<li><a href="question.php">Manage questions from <b>"Zadaj pytanie"</b> form</a></li>
			<li><a href="presentation.php">Manage <b>presentations</b> page</a></li>
			<li><a href="gallery.php">Manage <b>galleries</b> page</a></li>
			<li><a href="movie.php">Manage <b>movies</b> page</a></li>
			<li><a href="upload.php"><b>Upload</b> any type of file</a></li>
		</ul>
		<ul>
			<li><a href="menu.php">Manage <b>main menus</b></a></li>
			<li ><a href="page.php"><b>Pages</b> manager</a></li>
			<li ><a href="page2.php"><b>Pages</b> manager (TinyMCE)</a></li>
		</ul>
		<ul>
			<li><a href="archive.php"><b>Archivize</b> database</a></li>
			<li><a href="settings.php">Manage <b>settings</b> page</a></li>
			<li><a href="profile.php">Manage <b>profile</b> page</a></li>
			<li><a href="query.php">Make a user-defined <b>query</b></a></li>
			<li><a href="list.php">See <b>list of all pages</b> on the server<a></li>
			<li><a href="files.php">Get access to source <b>files</b></a></li>
			<li><a href="<?php echo $filename; ?>?action=logout"><b>LogOut</b></a></li>
		</ul>
	</div>
		
		
	<div class="welcomeMenu">
			Welcome <?php echo $_SESSION["usernameAdmin"]; ?>!&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="<?php echo $filename; ?>?action=logout">LogOut</a>
	</div>	
<?php
}
?>
</body>
</html>
