<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This page shows the list of ALL FILES, which exists on $serverName. They are shown in a tree structure, in brackets their direct links are given
//Note (1): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.10.07
include("login_check.php");

include("functions.php");
?>
<?php
	if($_GET["action"]=="delete")
	{
		$file=stripslashes($_GET["filename"]);
		
		if(is_file($file))
		{
			if(@unlink($file))
			{
				$file=null;
				$message.='<p class="success">File deleted.</p>';
			}
			else
			{
				$message.='<p class="error">Deletion failed! Could not delete the file</p>';
			}
		}
		else
		{
			$message.='<p class="error">Deletion failed! file <b>'.$file.'</b> not found.</p>';
		}
	}
	
	//preventing refresh
	if($message && !$_GET["message"])
	{
		$location="$filename";
		$location.="?message=".urlencode($message);
		header("Location: $location");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin.js"></script>


<title>AdminPages Orimari</title>

</head>

<body>

<div class="mainContentDiv">
	<h1>File List</h1>
	
	<?php
		//ako ima bilo koja poruka
		$message=$_GET["message"]?stripslashes($_GET["message"]):$message;
		
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
	<p>Below you may see the list of all files and folders (except for admin and files) beginning from <b><?php echo "http://{$_SERVER["SERVER_NAME"]}/"; ?></b>.</p>
	<p>In brackets there are shown direct links to the files. You may also delete a file.</p>
	<br/>
<?php
//todo recursion!

function showFolders($path)
{
	if($files=getFiles($path))
	{
		echo '<ul>'."\n";
		foreach($files as $file)
		{
			if(is_dir("$path/$file"))
			{
				if(!in_array($file,array("admin","files"))) //banned list
				{
					echo '<li>'."\n";
					echo '<p style="font-size:14px;border-bottom:1px solid #cccccc"><b>'.$file.'</b></p>'."\n";
					showFolders("$path/$file");
				}
			}
			else
			{
				$ext=pathinfo($file,PATHINFO_EXTENSION);
				echo '<li><img src="icons/'.$ext.'.gif" alt="['.$ext.']" style="border:none"/> '.$file.' <a style="font-size:9px">['."$path/$file".']</a>&nbsp; [ <a href="'.$filename.'?action=delete&filename='.urlencode("$path/$file").'" onclick="linkConfirm(\'Are you sure you want to delete the file?\',this.href);return false">delete?</a> ]</li>'."\n";
			}
		}
		echo '</ul>'."\n";
	}
	else
	{
		echo '<p class="info">Folder empty.</p>';
	}
}

showFolders("..");
	
	
?>	
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">FilesListPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>