<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a page for AdminPages on which you can set up some variables for the page
//Note (1): NO CHECKING IF TABLE >>>pagesettings<<< EXIST. IT MUST EXIST FOR THE PAGE TO WORK
//Note (2): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.25

include("login_check.php");

include("functions.php");
?>
<?php
	include("db_data.php");
	if($connection=@mysql_connect($server,$username,$password))
	{
		$db_select= @mysql_select_db($database);
		mysql_query("SET CHARACTER SET 'utf8'");
		if($db_select)
		{		
			$id=$_GET["id"];
			$action="show"; //default action
			
			if($_GET["action"]=="edit")
			{
				$action="edit";
				
				//make changes if any
				if($_POST["editSetting"]) //TODO
				{
					//dont use stripslashes
					$value=$_POST["value"]?"'{$_POST["value"]}'":"NULL";
					
					$query="UPDATE pagesettings SET value=$value WHERE settingID=$id LIMIT 1";
					if(@mysql_query($query))
					{
						$message.='<p class="success">Entry successfully saved!</p>';
					}
					else
					{
						$message.='<p class="error">Entry could not be saved!</p>';
					}
				}
				
				
				//display
				$query="SELECT property,value,language FROM pagesettings NATURAL JOIN languages where settingID=$id LIMIT 1";
					if($setting=@mysql_query($query))
					{
						if(!($settingData = @mysql_fetch_array($setting,MYSQL_ASSOC)))
						{
							$message.='<p class="error">Entry with this id does not exist!</p>';
						}
					}
					else
					{
						$message.='<p class="error">Database error (3). Try again later.</p>';
					}
			}
			else if($_GET["action"]=="delete")
			{
				$message.='<p class="info">Deletion is disabled.</p>';
			}
			
			//preventing refresh
			if($message && !$_GET["message"])
			{
				$location="$filename?action=$action";
				$location.=$id?"&id=".urlencode($id):"";
				$location.="&message=".urlencode($message);
				header("Location: $location");
			}
			
			//if is show (must be after refreh prevention
			if($action=="show")
			{
				$query="SELECT settingID AS ID,property,value,language FROM pagesettings NATURAL JOIN languages";
				if(!($settings= @mysql_query($query)))
				{
					$message.='<p class="error">Database error (3). Try again later.</p>';
				}
			}
		}
		else
		{
			$message.='<p class="error">Database error (2). Try again later.</p>';
		}
	}
	else
	{
		$message.='<p class="error">Database error (1). Try again later.</p>';
	}
	@mysql_close($connection);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin.js"></script>
<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Page Settings</h1>

	<?php
		//ako ima bilo koja poruka
		$message=$_GET["message"]?stripslashes($_GET["message"]):$message;
		
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
<?php
switch($action)
{
case "edit":
?>
	<p>On this page you may edit <b><?php echo "{$settingData["property"]} ({$settingData["language"]})"; ?></b> setting.</p>
	<br/>
	<form  name="editSetting" method="post" action="<?php echo "$filename?action=edit&id=$id"; ?>">
	<fieldset>
	<table>
		<tr>
			<td style="text-align:right"><label for="value">Value: </label></td>
			<td><textarea cols="40" rows="7" name="value"><?php echo htmlspecialchars($settingData["value"]); ?></textarea></td>
		</tr>
	</table>
	<input type="submit" name="editSetting" value="Save"/> <input type="button" value="Return" onclick="document.location='<?php echo $filename; ?>'"/>
	</fieldset>
	</form>
	
<?php
break;
case "show":
default:
?>
	<p>On this page you can manage settings for the page.</p>
	<p>Some of the settings are <b>Description</b> and <b>Keywords</b> which are used to register pages at the search engines.</p>
	<br/>
<?php
	list($tableResponse,$idList,$summary)=printTable($settings,array("filename"=>$filename));
	echo $tableResponse;
?>
<?php
break;
}
?>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">SettingsPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>