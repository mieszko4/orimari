<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: On this page you may manage PowerPoint presentation files with a picture for showing them as the link (folder $serverName/presentations)
//Note (1): You may want to delete/change all CHMOD() according to your server options
//Note (2): Save this file as UTF-8 without BOM, so it wont interfere with session check 
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.23

include("login_check.php");

include("imageSmartResize.php");
include("functions.php");
$presentationsFolder="presentations"; //folder to store presentations

$presentationsPath="../$presentationsFolder";
if(!is_dir($presentationsPath))
{
	if(!mkdir($presentationsPath))
	{
		header("Location: index.php?message=".urlencode('<p class="error">Could not create <b>'.$presentationsFolder.'</b>!</p>'));
	}
	chmod($presentationsPath,0777);
}

$maxWidth=330; //resizing in pixels for pictures
$maxHeight=330;
$warningSize=10*1048576; //in bytes
$fileFilter=array("ppt","pptx","pps","ppsx"); //for all types use null
$prefix="ppt"; //used to mark files in tmp folder
$compressionLevel=95; //0->100
?>
<?
	$message="";
	$presentationFile=stripslashes($_GET["presentationFile"]);
	$presentationName=pathinfo($presentationFile,PATHINFO_FILENAME);
	$presentationExt=strtolower(pathinfo($presentationFile,PATHINFO_EXTENSION));
	
	$picture=is_file("$presentationsPath/$presentationName.jpg");
	$tmpFile=getFromTemp($prefix);
	
	
	if($_GET["action"]=="edit")
	{
		$action="edit";
		
		if(is_file("$presentationsPath/$presentationFile"))
		{
			//save changes
			if($_POST["editPresentation"])
			{
				$presentationFileNew=stripslashes($_POST["presentationFileNew"]);
				$presentationExtNew=strtolower(pathinfo($presentationFileNew,PATHINFO_EXTENSION));
				//ignore extension
				$presentationNameNew=($presentationExt==$presentationExtNew)?pathinfo($presentationFileNew,PATHINFO_FILENAME):$presentationFileNew;
				$presentationFileNew="$presentationNameNew.$presentationExt";
				
				if(!is_file("$presentationsPath/$presentationFileNew") || $presentationName==$presentationNameNew)
				{
				
					//add or delete photo 
					if($_POST["radioPicture"]=="delete")
					{
						if(@unlink("$presentationsPath/$presentationName.jpg"))
						{
							$message.='<p class="success">Picture removed.</p>';
						}
						else
						{
							$message.='<p class="error">Could not remove the picture.</p>';
						}
					}				
					else if($_POST["checkboxPicture"] || $_POST["radioPicture"]=="change")
					{
						$picture=$_FILES["picture"];
						if(is_uploaded_file($picture['tmp_name']))
						{
						
							list($width_orig, $height_orig, $type_orig) = getimagesize($picture['tmp_name']);
							if(in_array($type_orig,array(IMAGETYPE_GIF,IMAGETYPE_JPEG,IMAGETYPE_PNG)))
							{
								if($maxWidth>$width_orig) $maxWidth=$width_orig;
								if($maxHeight>$height_orig) $maxHeight=$height_orig;
								
								$image_p=smart_resize_image($picture["tmp_name"],$maxWidth,$maxHeight,true,'return',false,false);
								
								if(@imagejpeg($image_p,"$presentationsPath/$presentationName.jpg",$compressionLevel))
								{
									$message.='<p class="success">New picture added.</p>';
								}
								
							}
							else
							{
								$message.='<p class="error">Specified file is not a valid picture file.</p>';
							}
							unlink($picture["tmp_name"]);
						}
						else
						{
							$message.='<p class="error">Could not add new picture!</p>';
						}
					}
					
					//rename
					if($presentationName!=$presentationNameNew)
					{
						if(rename("$presentationsPath/$presentationFile","$presentationsPath/$presentationFileNew"))
						{
							if(is_file("$presentationsPath/$presentationName.jpg"))
							{
								if(rename("$presentationsPath/$presentationName.jpg","$presentationsPath/$presentationNameNew.jpg"))
								{
									$message.='<p class="success">Presentation renamed.</p>';
									$message.='<p class="info">Refresh all pages which used the old name of this presentaton.</p>';
								}
								else
								{
									$message.='<p class="error">There were problems with renaming presentation (picture)!</p>';
								}
							}
							$presentationFile=$presentationFileNew;
						}
						else
						{
							$message.='<p class="error">There were problems with renaming presentation!</p>';
						}
					}
				}
				else
				{
					$message.='<p class="error">Presentation <b>'.$presentationFileNew.'</b> exists already!</p>';
				}
			}
		}
		else
		{
			$action="show";
			$message.='<p class="error">Presentation <b>'.$presentationFile.'</b> not found!</p>';
		}
	}
	else if($_GET["action"]=="delete")
	{
		$action="show";
		
		if($_GET["tmpFile"]=="yes")
		{
			if(deleteTmpFile($prefix))
			{
				$tmpFile=null;
				$message.='<p class="success">Temporary file deleted.</p>';
			}
			else
			{
				$message.='<p class="error">Could not delete temporary file! There is no possibility to upload new files until the temporary file is deleted or renamed!</p>';
			}
		}
		else if(is_file("$presentationsPath/$presentationFile"))
		{
			if(unlink("$presentationsPath/$presentationFile"))
			{
				$presentationFile=null;
				$message.='<p class="success">Presentation deleted.</p>';
				
				if(is_file("$presentationsPath/$presentationName.jpg") && unlink("$presentationsPath/$presentationName.jpg"))
				{
					$message.='<p class="success">Picture deleted.</p>';
				}
			}
			else
			{
				$message.='<p class="error">Deletion failed! Could not delete the presentation</p>';
			}
		}
		else
		{
			$message.='<p class="error">Deletion failed! Presentation <b>'.$presentationFile.'</b> not found.</p>';
		}
	}
	else if($_POST["addPresentation"])
	{
		$action="show";
		
		$presentationFile=stripslashes($_POST["presentationFile"]);
		
		//if there is a temp waiting
		if($tmpFile)
		{
			$presentation=array("tmp_name"=>dirname($tmpFile)."/{$prefix}_".basename($tmpFile),"name"=>basename($tmpFile));
		}
		else
		{
			$presentation=$_FILES["presentation"];
		}
		
		if(is_uploaded_file($presentation["tmp_name"]) || is_file($presentation["tmp_name"]))
		{
			$presentationExt=strtolower(pathinfo($presentation["name"],PATHINFO_EXTENSION));
			$presentationExtForm=strtolower(pathinfo($presentationFile,PATHINFO_EXTENSION));
			//ignore extension
			$presentationName=($presentationExt==$presentationExtForm)?pathinfo($presentationFile,PATHINFO_FILENAME):$presentationFile;
			$presentationFile=$presentationName?"$presentationName.$presentationExt":$presentation["name"]; //if not given, use default
			$presentationName=pathinfo($presentationFile,PATHINFO_FILENAME); //if name changes
		
			if(!$fileFilter || in_array($presentationExt,$fileFilter))
			{
				if(!is_file("$presentationsPath/$presentationFile"))
				{					
					//dont use move because of tmp file
					if(@copy($presentation["tmp_name"],"$presentationsPath/$presentationFile")) 
					{
						$message.='<p class="success">Presentation added.</p>';
						
						$picture=$_FILES["picture"];
						if(is_uploaded_file($picture["tmp_name"]))
						{
						
							list($width_orig, $height_orig, $type_orig) = getimagesize($picture['tmp_name']);
							
							if(in_array($type_orig,array(IMAGETYPE_GIF,IMAGETYPE_JPEG,IMAGETYPE_PNG)))
							{
								if($maxWidth>$width_orig) $maxWidth=$width_orig;
								if($maxHeight>$height_orig) $maxHeight=$height_orig;
								
								$image_p=smart_resize_image($picture["tmp_name"],$maxWidth,$maxHeight,true,'return',false,false);
								
								if(@imagejpeg($image_p,"$presentationsPath/$presentationName.jpg",$compressionLevel))
								{
									$message.='<p class="success">Picture added.</p>';
								}
							}
							else
							{
								$message.='<p class="error">Specified file is not a valid picture file.</p>';
							}
							unlink($picture["tmp_name"]);
						}
						unlink($presentation["tmp_name"]);
						$presentationFile=null; //dont show in form
					}
					else
					{
						$message.='<p class="error">Could not move presentation file to presentations folder!</p>';
					}
					
				}
				else
				{
					$message.='<p class="error">Name <b>'.$presentationFile.'</b> already exists! Please change the name.</p>';
					saveToTemp($presentation,$prefix);
					$presentationFile="{$presentationName}_".time().".$presentationExt";
				}
			}
			else
			{
				$presentationFile=null; //dont show in form
				$message.='<p class="error">Presentation file not uploaded! Allowed file types are: '.implode(", ",$fileFilter).'!</p>';
			}
		}
		else
		{
			$message.='<p class="error">No file uploaded!</p>';
		}

	}
	
	//preventing refresh
	if($message && !$_GET["message"])
	{
		$location="$filename?action=$action";
		$location.=$presentationFile?"&presentationFile=".urlencode($presentationFile):"";
		$location.="&message=".urlencode($message);
		header("Location: $location");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="adminOrimari.ico"/>
<link rel="stylesheet" href="admin.css" type="text/css" />
<script type="text/javascript" src="admin.js"></script>

<title>AdminPages Orimari</title>
</head>

<body>

<div class="mainContentDiv">
	<h1>Presentations</h1>

	<?php 
		$message=stripslashes($_GET["message"]);
		if($message)
		{
			echo "$message<br/>";
		}
	?>
	<br/>
<?php
switch($action)
{
case "edit":
?>
	<p>On this page you are managing <b><?php echo $presentationFile;?></b>.<br/>
	Presentation's link is: <b><?php echo "$presentationsPath/$presentationFile"; ?></b></p>
	
	<p>You may change the name or add/delete/change the picture.<br/>
	You may also <a href="<?php echo "$filename?action=delete&presentationFile=".urlencode($presentationFile); ?>" onclick="linkConfirm('Are you sure you want to delete entire presentation?',this.href);return false">delete</a> the entire presentation.</p>
	
	<form  name="editPresentation" method="post" action="<?php echo "$filename?action=edit&presentationFile=$presentationFile";?>" enctype="multipart/form-data">
		<fieldset>
		<table>
			<tr>
				<td style="width:30%;text-align:right"><label for="presentationFileNew">Name: </label></td>
				<td><input name="presentationFileNew" type="text" size="20" value="<?php echo htmlspecialchars($presentationName); ?>"/></td>
			</tr>
		</table>
		<br/>
	<?php
	if($picture)
	{
	?>
		<table>
			<tr>
				<td style="width:30%;text-align:right">Presentation has a picture: </td>
				<td><img src="<?php echo "$presentationsPath/$presentationName.jpg?".time(); ?>" alt="" style="max-height:50px"/></td>
			</tr>
			<tr>
				<td style="text-align:right">Leave picture: </td>
				<td><input type="radio" name="radioPicture" value="leave" checked="checked" onclick="showHidePictureBox()"/></td>
			</tr>
			<tr>
				<td style="text-align:right">Delete picture: </td>
				<td><input type="radio" name="radioPicture" value="delete" onclick="showHidePictureBox()"//></td>
			</tr>
			<tr>
				<td style="text-align:right">Change picture: </td>
				<td><input type="radio" name="radioPicture" id="checkerPicture" value="change"/></td>
			</tr>		
		</table>
	<?php
	}
	else //no Jpeg
	{
	?>
		<label for="checkboxPicture">Add picture?</label><input id="checkerPicture" name="checkboxPicture[]" type="checkbox"/><br/>
	<?php
	}
	?>
		<div id="pictureBox" style="border: 1px #cccccc dashed; padding:3px">
			<table>
				<tr>
					<td style="text-align:right"><label for="picture">New picture: </label></td>
					<td><input type="file" name="picture" id="picture"/></td>
				</tr>
			</table>
		</div>
		<script type="text/javascript">
			//connected pictureBox div
			var checkerPicture = document.getElementById("checkerPicture");
			var pictureBox = document.getElementById("pictureBox");
			
			function showHidePictureBox()
			{
				if(checkerPicture.checked)
				{
					pictureBox.style.visibility="visible";
				}
				else
				{
					pictureBox.style.visibility="hidden";
				}
			}
			showHidePictureBox(); //hide on start
			checkerPicture.onclick=showHidePictureBox;	
		</script>
					
		<br/>
		<input type="submit" name="editPresentation" value="Save"/> <input type="button" value="Return" onclick="document.location='<?php echo $filename; ?>'"/>
		</fieldset>
		</form>
<?php
break;
case "show":
default:
?>
	<p>On this page you may see/add/delete <b>PowerPoint presentations</b>.<br/>
	You may also upload a picture which will be shown as the link on the page for presentation download.</p>
	<p>Presentations are located in folder: <b><?php echo "http://".$_SERVER["SERVER_NAME"]."/".$presentationsFolder."/"; ?></b>.<br/>
	<b>Note:</b> The upload limit is 20mb and the presentation will <b>not</b> be compressed; please compress the presentation before upload and be <b>patient</b> while uploading a huge file!</p>
	<br/>
	
	<table class="display">
		<thead>
		<tr>
			<td>Presentation filename</td><td>Static picture</td><td></td>
		</tr>
		</thead>
		<tbody>
			<tr class="addNew">
				<td>Add a new presentation:</td>
				<td>
				<form enctype="multipart/form-data" action="<?php echo $filename; ?>" method="post">
					<table>
						<tr>
							<td style="width:30%;text-align:right"><label for="presentationFile" <?php echo $tmpFile?'class="info"':""; ?>>Name: </label></td>
							<td><input name="presentationFile" type="text" size="20" value="<?php echo $presentationName?htmlspecialchars($presentationName):($tmpFile?pathinfo($tmpFile,PATHINFO_FILENAME)."_".time():""); ?>"/></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="presentation">Presentation: </label></td>
							<td><?php echo $tmpFile?'<p><b>'.basename($tmpFile).'</b></p>':'<input name="presentation" type="file" value="Browse..." />'; ?></td>
						</tr>
						<tr>
							<td style="text-align:right"><label for="picture">Picture: </label></td>
							<td><input name="picture" type="file" value="Browse..."/></td>
						</tr>
					</table>
					<br/>
					<input type="submit" name="addPresentation" value="Add"/>
				</form>
				</td>
				<td>
					<?php echo $tmpFile?'<p class="info">There is a file on the server which needs to be <b>renamed</b> in order to be saved. <a href="'.$filename.'?action=delete&tmpFile=yes" onclick="linkConfirm(\'Are you sure you want to delete the temporary file?\',this.href);return false">Delete the file?</a></p>':''; ?>
					<p><b>Note: </b> Name field is the name of the presentation and picture files which will be created on the server. If it is left empty the original name of the presentation file will be saved instead.</p>
					<p><b>Picture</b> appears instead of the link for presentation download on the page. It is not obligatory.</p>
				</td>
			</tr>		
<?php
//show all presentation files from the folder
	if($theFiles=getFiles($presentationsPath,$fileFilter))
	{
		foreach($theFiles as $presentationFile)
		{
			$presentationName=pathinfo($presentationFile,PATHINFO_FILENAME);
			
			if(is_file("$presentationsPath/$presentationFile"))
			{
				echo "<tr>\n";
				
				echo '<td><a href="'.$filename.'?action=edit&presentationFile='.urlencode($presentationFile).'"><b>'.$presentationFile.'</b><br/>[ manage ]</a></td>'."\n";
				if(is_file("$presentationsPath/$presentationName.jpg"))
				{
					echo '<td><img src="'."$presentationsPath/$presentationName.jpg?".time().'" alt="" style="max-height:150px"/>';
				}
				else
				{
					echo '<td><p class="info">No picture specified for this presentation.</p>';
				}
				
				$presentationSize=filesize("$presentationsPath/$presentationFile");
				printf("<p><b>FileSize:</b> %.2fMB </p>\n",$presentationSize/1048576);
				
				//SizeWarning
				if($presentationSize>$warningSize)
				{
					printf("<p class=\"info\">The presentation exceeds %.2fMB! Consider compressing the file and upload it again.</p>\n",$warningSize/1048576);
				}
				
				echo '<p>[<a href="'."$presentationsPath/$presentationFile".'"> download </a>| <a href="'.$filename.'?action=delete&presentationFile='.urlencode($presentationFile).'" onclick="linkConfirm(\'Are you sure you want to delete entire presentation?\',this.href);return false"> delete </a>]</p></td>'."\n";
				echo "<td></td>\n";
				echo "</tr>\n";
			}
		}	
	}
?>	
		</tbody>
	</table>
<?php
break;
}
?>
</div>


<div class="welcomeMenu">
	Welcome <?php echo $_SESSION["usernameAdmin"]; ?>!&nbsp;|&nbsp;<a href="<?php echo $filename; ?>">PresentationsPage</a>&nbsp;|&nbsp;<a href="index.php">MainPage</a>&nbsp;|&nbsp;<a href="index.php?action=logout">LogOut</a>
</div>
</body>
</html>