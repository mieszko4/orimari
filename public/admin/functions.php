<?php
//Info-Main: Part of AdminPages. Must be stored in $serverName/admin folder
//Info: This is a set of common functions used in AdminPages
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.09.25

//Returns: an array of files and directories in $path
//$extArray is the array of file types to be filtered
function getFiles($path,$extArray=false)
{
	$theFiles=null;
	if($extArray && !is_array($extArray))
	{
		$extArray=array($extArray);
	}
	
	if (is_dir($path))
	{
		$entries = scandir($path);
		foreach ($entries as $entry)
		{
			if ($entry!='.' && $entry!='..' && $entry!='')
			{
				if($extArray)
				{	
					$ext=strtolower(pathinfo($entry,PATHINFO_EXTENSION));
					if(in_array($ext,$extArray))
					{
						$theFiles[]= $entry;
					}
				}
				else
				{
					$theFiles[]= $entry;
				}
			}
		}
	}
	return $theFiles;
}

//Completaly removes some folder/file (incl. subfolders/files)
function deltree($path)
{
	if (is_dir($path))
	{
		$entries = scandir($path);
		foreach ($entries as $entry)
		{
			if ($entry!='.' && $entry!='..' && $entry!='')
			{
				deltree("$path/$entry");
			}
		}
		rmdir($path);
	}
	else
	{
		unlink($path);
	}
	return true;
}

//Moves all files from $path (incl. subfolders) to $root
//New files are renamed as "file%03$count.$ext", where $count is the number next file
//Returns: number of moved files
//Note: DIRectories will NOT be DELETED
function moveToSingle($path,$root,$count=0)
{
	if (is_dir($path))
	{
		$entries = scandir($path);
		foreach ($entries as $entry)
		{
			if ($entry!='.' && $entry!='..' && $entry!='')
			{
				$count=moveToSingle("$path/$entry",$root,$count);
			}
		}
	}
	else //is file
	{
		//split into path and filename
		$filename=basename($path);
		$ext=strtolower(pathinfo($filename,PATHINFO_EXTENSION));
		
		$newFileName=sprintf("file%03d.%s",$count,$ext);
		rename($path,"$root/$newFileName");
		$count++;
	}
	return $count;
}

//Saves file given as filehandler $fh to temporary file $tmpDir as $fh["name"]
function saveToTemp($fh,$prefix="",$tmpDir="tmp")
{
	if(!is_dir($tmpDir))
	{
		if(!@mkdir($tmpDir) || !@chmod($tmpDir,0777))
		{
			return false;
		}
	}
	
	if(@copy($fh["tmp_name"],"$tmpDir/{$prefix}_".basename($fh["name"])))
	{
		return true;
	}
	else
	{
		return false;
	}
}

//Gets a temporary file temp with the first extension found in $extArray
function getFromTemp($prefix="",$tmpDir="tmp")
{
		
	if($files=getFiles($tmpDir,null))
	{
		foreach($files as $file)
		{
			//get the first found
			if(is_file("$tmpDir/$file"))
			{
				if(strstr($file,"{$prefix}_"))
				{
					//without prefix
					return "$tmpDir/".substr($file,strlen($prefix)+1);
				}
			}
		}
	}
	return false; //otherwise
}

//Deletes a temporary file temp with the first extension foun din $extArray
function deleteTmpFile($prefix,$tmpDir="tmp")
{
	if($files=getFiles($tmpDir,null))
	{
		foreach($files as $file)
		{
			//delete the first found
			if(is_file("$tmpDir/$file"))
			{
				if(strstr($file,"{$prefix}_") && unlink("$tmpDir/$file"))
				{
					return true;
				}
			}
		}
	}
	return false; //otherwise
}

//Proccesses zip and picture (jpg,gif,png) file $fh. Moves to tmp, resizes, renames as image%03d.jpg, and saves as .jpg in $galleryDir
//Function used in admin/gallery.php. RETURNS $message
function processPictures($fh,$galleriesPath,$galleryDir,$maxWidth,$maxHeight,$thumbnailHeight=100,$compressionLevel=95)
{
	include("pclzip-2-6/pclzip.lib.php");		
	include("imageSmartResize.php");
	
	$fhExt=strtolower(pathinfo($fh["name"],PATHINFO_EXTENSION));
	$galleryPath="$galleriesPath/$galleryDir";
	$message="";
	
	//create tmp
	if(!is_dir("$galleryPath/tmp"))
	{
		if(!@mkdir("$galleryPath/tmp"))
		{
			$message.='<p class="error">Could not process the file (tmpCreate)!</p>';
			return $message;
		}
		chmod("$galleryPath/tmp",0777);
	}
	
	//move all to tmp
	if($fhExt=="zip")
	{
		$zip = new PclZip($fh['tmp_name']);
		$zip->extract(PCLZIP_OPT_PATH, "$galleryPath/tmp");
		unlink($fh['tmp_name']);
		moveToSingle("$galleryPath/tmp","$galleryPath/tmp");
	}
	else //single picture
	{
		rename($fh['tmp_name'],"$galleryPath/tmp/{$fh['name']}");
	}
	
	//get the intial counter
	$counter=1;
	if($gallery=getFiles($galleryPath,"jpg"))
	{
		list($counter)=sscanf($gallery[count($gallery)-1],"image%d");
		$counter++;
	}
	
	//move from tmp to $galleryDir
	if(!is_dir("$galleryPath/mini"))
	{
		if(!@mkdir("$galleryPath/mini"))
		{
			$message.='<p class="error">Could not process the file (miniCreate)!</p>';
			return $message;
		}
		chmod("$galleryPath/mini",0777);
	}
	
	$counterBigPics=0;
	$counterSmallPics=0;
	if($tmpFiles=getFiles("$galleryPath/tmp"))
	{
		foreach($tmpFiles as $file)
		{
			if(is_file("$galleryPath/tmp/$file")) //directory prevention
			{
				list($width_orig, $height_orig, $type_orig) = getimagesize("$galleryPath/tmp/$file");
				if(in_array($type_orig,array(IMAGETYPE_GIF,IMAGETYPE_JPEG,IMAGETYPE_PNG)))
				{
					$newFile=sprintf("image%03d.jpg",$counter);
					$counter++;
					
					//saving BIG
					if($maxWidth>$width_orig) $maxWidth=$width_orig;
					if($maxHeight>$height_orig) $maxHeight=$height_orig;
					
					$image_p=smart_resize_image("$galleryPath/tmp/$file",$maxWidth,$maxHeight,true,'return',false,false);
					if(@imagejpeg($image_p,"$galleryPath/$newFile",$compressionLevel))
					{
						$counterBigPics++;
					}
					
					//saving SMALL
					if($thumbnailHeight>$height_orig) $thumbnailHeight=$height_orig;
					
					$image_p=smart_resize_image("$galleryPath/tmp/$file",0,$thumbnailHeight,true,'return',false,false);
					if(@imagejpeg($image_p,"$galleryPath/mini/$newFile",$compressionLevel))
					{
						$counterSmallPics++;
					}				
				}
			}
		}
	}
	
	//delete tmp
	deltree("$galleryPath/tmp");
	
	if($counterBigPics==$counterSmallPics)
	{
		$message.=$counterBigPics?'<p class="success">Number of added photos is: '.$counterBigPics.'</p>':'<p class="info">No photos added.</p>';
	}
	else
	{
		$message.='<p class="error">There were some errors while processing the file '."($counterBigPics-$counterSmallPics)".' !</p>';
	}
	
	return $message;
}

//Return a string with table $table created from mysql query response AND array of $id (get it as list($tableResponse,$idList);
//if options column is specified action=edit and action=delete will beadded, it has to be as array("filename"=>$filename) and $row["ID"] must exist
function printTable($table,$optionsColumn=null)
{
	$tableResponse="";
	$idList=array();
	$summary=array();
	if(($rowsNumber=@mysql_num_rows($table))>0)
	{
		//THEAD
		$row = @mysql_fetch_array($table,MYSQL_ASSOC);
		$tableResponse.= '
		<table class="display">
		<thead>
			<tr>';
		foreach($row as $key=>$value)
		{
			$tableResponse.= "<td>$key</td>";
		}
		$tableResponse.=$optionsColumn?'<td>Options</td>':"";
		$tableResponse.= '</tr>
		</thead>
		<tbody>';
		
		//TBODY
		$odd=true;
		do
		{
			$tableResponse.=$odd?"\n<tr>":"\n".'<tr style="background-color:#ffffcf">';
			$odd=!$odd;
			
			foreach($row as $key=>$value)
			{
				$value=htmlspecialchars($value);
				$tableResponse.= "<td>$value</td>";
			}
			if($optionsColumn && $row["ID"])
			{
				$tableResponse.= '<td><a href="'.$optionsColumn["filename"].'?action=edit&id='.$row["ID"].'">Edit</a><br/><a href="'.$optionsColumn["filename"].'?action=delete&id='.$row["ID"].'" onclick="linkConfirm(\'Are you sure you want to delete this entry?\',this.href);return false">Delete</a></td>';
				$idList[]=$row["ID"];
				$summary[]=substr($row["question"],0,30);
			}
			$tableResponse.= "</tr>";
		}
		while($row = @mysql_fetch_array($table,MYSQL_ASSOC));
		
		$tableResponse.= '
		</tbody>
		</table>';
	}
	else
	{
		$tableResponse.= '<p class="info">Query resulted with 0 rows.</a>.</p>';
	}
	
	return array($tableResponse,$idList,$summary);
}

function showFlvMovie($movieName,$language=false,$isHr=true,$width,$height,$backgroundColor="#ffffff")
{
	$moviesPath="../movies"; //movies path
	$separator=">";
	
	$containerID="movieContainer".microtime(); //for unique id
	$containerID=strtr($containerID," .","__");
	
	if(is_file("$moviesPath/$movieName.flv"))
	{
		//get info file
		if(is_file("$moviesPath/$movieName.info"))
		{
			$lines=file("$moviesPath/$movieName.info");
			$movieInfo=null;
			foreach($lines as $line)
			{
				list($property,$value)=@split($separator,rtrim($line));
				$movieInfo[$property]=htmlspecialchars($value);
			}
			
			$widthF=$movieInfo["width"];
			$heightF=$movieInfo["height"];
		}
		$width=$width?$width:$widthF;
		$width=$width?$width:330;
		$height=$height?$height:$heightF;
		$height=$height?$height:300;
		
		if($isHr) echo '<hr class="horizontal"/>'."\n";
		
		echo '<div class="movieContent">'."\n";
		if($language=='pl')
		{
			echo '<div id="'.$containerID.'">W celu obejrzenia filmu potrzebne jest włączenie języka <a>JavaScript</a> oraz wtyczki <a href="http://www.macromedia.com/go/getflashplayer">Adobe Flash Player</a>.</div>'."\n";
		}
		else //en
		{
			echo '<div id="'.$containerID.'">In order to see the movie you have to turn on <a>JavaScript</a> and <a href="http://www.macromedia.com/go/getflashplayer">Adobe Flash Player</a>.</div>'."\n";
		}
		
		echo '
			<script type="text/javascript" src="../files/flashPlayer/swfobject.js"></script>
		
			<script type="text/javascript">
				var s1 = new SWFObject("../files/flashPlayer/player.swf","ply","'.$width.'","'.$height.'","9","#FFFFFF");
				s1.addParam("allowfullscreen","true");
				s1.addParam("allowscriptaccess","always");
		';
				if(is_file("$moviesPath/$movieName.jpg"))
				{
					echo 's1.addParam("flashvars","file=../../movies/'.$movieName.'.flv&image=../movies/'.$movieName.'.jpg");';
				}
				else
				{
					echo 's1.addParam("flashvars","file=../../movies/'.$movieName.'.flv");';
				}
		echo 's1.write("'.$containerID.'");
				</script>
			</div>
		';
	}
}

//Creates a gallery for lighbox
//$galleryDir is obligatory ($serverName/photos/$galleryDir), $language is a 2char string defining language information (pl, en)
function showGallery($galleryDir,$language='en',$isHr=true)
{
	$galleriesPath="../photos"; //galleries directory
	$separator=">";
	
	$galleryDir="$galleriesPath/$galleryDir";
	$theFiles=null;
	if (is_dir($galleryDir))
	{
		$entries = scandir($galleryDir);
		foreach ($entries as $entry)
		{
			if ($entry!='.' && $entry!='..' && $entry!='')
			{
				$theFiles[]= $entry;
			}
		}
		$theFiles = preg_grep("/jpg$/",$theFiles);
		if(count($theFiles)>0)
		{
			//get info $galleryName and $galleryInfo
			if(is_file("$galleryDir/gallery.info"))
			{
				$lines=file("$galleryDir/gallery.info");
				$galleryInfo=null;
				foreach($lines as $lineNum => $line)
				{
					list($property,$value)=split($separator,rtrim($line));
					$galleryInfo[$property]=htmlspecialchars($value);
				}
				
				$gName=$galleryInfo["$language-name"];
				$gInfo=$galleryInfo["$language-info"];
			}
			
			
			if($isHr) echo "\n".'<hr class="horizontal"/>'."\n";
			
			echo '<div class="photosContent"><p style="text-align:left">'."\n";
			foreach($theFiles as $file)
			{
				echo '<a href="'.$galleryDir.'/'.$file.'" rel="lightbox['.$galleryDir.']" title="'.$gName.'"><img src="'."$galleryDir/mini/$file".'" alt="'.$file.'"/></a>'."\n";
			}			
			echo '</p>'."\n";
			
			if($gInfo) echo '<p style="margin-left:50px">'.$gInfo.'</p>'."\n";
			
			echo '<div/>'."\n";
		}
	}
}

//Deletes all subpages from the database starting from $pageID
//Returns: number of deleted pages or -1 if error
function deletePage($pageID)
{
	$deletedPages=0;
	
	$query="SELECT pageID FROM pages WHERE pageIDHolder=$pageID";
	if($page=@mysql_query($query))
	{
		while($rowPage=@mysql_fetch_array($page,MYSQL_ASSOC))
		{
			$deletedPages+=deletePage($rowPage["pageID"]);
			if($deletedPages==-1)
			{
				return -1;
			}
		}
		
		//main or sub?
		$query="SELECT pageIDHolder FROM pages WHERE pageID=$pageID AND pageIDHolder IS NULL LIMIT 1";
		if($page=@mysql_query($query))
		{
			if($rowPage=@mysql_fetch_array($page,MYSQL_ASSOC))
			{
				//main
				$query="DELETE FROM mainpages WHERE pageID=$pageID";
				if(@mysql_query($query))
				{
					$query="DELETE FROM menupages WHERE pageID=$pageID";
					if(@mysql_query($query))
					{
						//OK
					}
					else
					{
						$message.="Could not query for $pageID (menupages)";
						return -1;
					}
				}
				else
				{
					$message.="Could not query for $pageID (mainpages)";
					return -1;
				}
			}
			else
			{
				//sub		//delete unexisting links too
				$query="DELETE FROM submenupages WHERE pageIDHolder=$pageID OR pageIDLink=$pageID";
				if(@mysql_query($query))
				{
					//OK
				}
				else
				{
					$message.="Could not query for $pageID (submenupages)";
					return -1;
				}
			}
			
			$query="DELETE FROM pages WHERE pageID=$pageID";
			if(@mysql_query($query))
			{
				//OK
				$deletedPages++;
			}
			else
			{
				$message.="Could not query for $pageID (pages)";
				return -1;
			}
		}
		else
		{
			$message.="Could not query for $pageID (main/sub)";
			return -1;
		}	
	}
	else
	{
		$message.="Could not query for $pageID (0)";
		return -1;
	}
	
	return $deletedPages; //all went fine
}
?>