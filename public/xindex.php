<?php
//Info-Main: Part of Orimari.com Must be stored in $serverName/ folder (2008.09.30:mieszko4GmailCom)
//script finds the most close language and redirects it to: >>>$language/index.php<<< through Location header
$availableLanguages = array("en","pl");
$defaultLanguage = "en";

$language=$defaultLanguage;
if( isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]) )
{
	$acceptLanguages = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
	$acceptLanguages = str_replace(' ','',$acceptLanguages);
	$acceptLanguages = explode(",", $acceptLanguages);
	
	foreach ($acceptLanguages as $languageAc)
	{
		$languageAc=substr($languageAc,0,2); //get only major
		
		foreach($availableLanguages as $languageAv)
		{
			if($languageAv==$languageAc)
			{
				$language=$languageAc;
				break(2);
			}
		}
	}
}

//get all settings
include("files/db_data.php");
if($connection = @mysql_connect($server,$username,$password))
{
	if($db_select= @mysql_select_db($database))
	{
		mysql_query("SET CHARACTER SET 'utf8'");
		
		//description and keywords
		$query="SELECT property,value FROM pagesettings NATURAL JOIN languages WHERE language='$language' LIMIT 2";
		$settings=@mysql_query($query);
		$settingsArray=array();
		while($row=@mysql_fetch_array($settings,MYSQL_ASSOC))
		{
			$settingsArray[$row["property"]]=$row["value"];
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta http-equiv="Refresh" content="7; URL=<?php echo "$language/index.php"; ?>"/>

<meta name="Description" content="<?php echo htmlspecialchars($settingsArray["Description"]); ?>"/>
<meta name="Keywords" content="<?php echo htmlspecialchars($settingsArray["Keywords"]); ?>"/>
	
<title>Orimari</title>

<link rel="shortcut icon" href="files/orimari.ico"/>

<style type="text/css">
	table {border-style:none;width:100%;}
	body{background-color:#999999;background-image:url('files/xmas/xsnow.png'); background-repeat:repeat;font-size:30px;font-style:italic;color:#ffffff;padding-top:60px}
	img{border-style:none}
</style>

</head>

<body>

<table>
	<tr>
	
<?php
if($language=="pl")
{
?>
	<td style="text-align:right"><a href="pl/index.php"><img src="files/xmas/xtree.png" alt=""/></a></td>
	<td style="text-align:left">
		Zdrowych, pogodnych, radosnych <br />
		Świąt Bożego Narodzenia<br />
		oraz <br />
		Szczęśliwego Nowego Roku<br /><br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; życzy<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fundacja 
		ORIMARI
	</td>
<?php
}
else //english default
{
?>
	<td style="text-align:right"><a href="en/index.php"><img src="files/xmas/xtree.png" alt=""/></a></td>
	<td style="text-align:left">
		Merry Christmas <br />
		and <br />
		Happy New Year!<br /><br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ORIMARI Foundation
	</td>
<?php
}
?>
	</tr>
</table>

</body>

<script type="text/javascript">
//	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
//	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
//	var pageTracker = _gat._getTracker("UA-4569524-1");
//	pageTracker._initData();
//	pageTracker._trackPageview();
</script>
</html>
