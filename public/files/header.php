<?php //Info-Main: Part of Orimari.com Must be stored in $serverName/files folder (2008.09.25:mieszko4GmailCom) ?>
<?php
preg_match("/[^\/]*\/([^\/]*)\/([^\/]*)$/",$_SERVER["SCRIPT_NAME"],$matches);
$filename = $matches[2];
$filename = ($filename=='')?'index.php':$filename;
$language = $matches[1];
if($_GET['id']) //it is actually orderNumber of submenupages
{
	$id=$_GET['id'];
}
else
{
	$id=0;
}
if($_GET['subid']) //it is actually orderNumber of subsubmenupages
{
	$subid=$_GET['subid'];
}
else
{
	$subid=0;
}
include("functions.php");
//get all needed tables
include("db_data.php");
if($connection = @mysql_connect($server,$username,$password))
{
	if($db_select= @mysql_select_db($database))
	{
		mysql_query("SET CHARACTER SET 'utf8'");
		
		//description and keywords
		$query="SELECT property,value FROM pagesettings NATURAL JOIN languages WHERE language='$language' LIMIT 2";
		$settings=@mysql_query($query);
		$settingsArray=array();
		while($row=@mysql_fetch_array($settings,MYSQL_ASSOC))
		{
			$settingsArray[$row["property"]]=$row["value"];
		}
		
		//mainMenu
		//CONSIDER SIMPLIFY
		$query="SELECT menupages.pageID,externalLink,itemName,filename,languages2.language,orderNumber
			FROM languages NATURAL JOIN menus NATURAL JOIN menupages
			JOIN mainpages NATURAL JOIN languages AS languages2
			WHERE menus.name='mainMenu' AND languages.language='$language'
			AND (menupages.pageID=mainpages.pageID OR menupages.pageID IS NULL) GROUP BY orderNumber
			ORDER BY orderNumber";
		$mainMenu=@mysql_query($query);
		
		//bottomMenu
		//CONSIDER SIMPLIFY
		$query="SELECT menupages.pageID,externalLink,itemName,filename,languages2.language,orderNumber
			FROM languages NATURAL JOIN menus NATURAL JOIN menupages
			JOIN mainpages NATURAL JOIN languages AS languages2
			WHERE menus.name='bottomMenu' AND languages.language='$language'
			AND (menupages.pageID=mainpages.pageID OR menupages.pageID IS NULL) GROUP BY orderNumber
			ORDER BY orderNumber";
		$bottomMenu=@mysql_query($query);
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language; ?>" lang="<?php echo $language; ?>">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="Description" content="<?php echo htmlspecialchars($settingsArray["Description"]); ?>"/>
	<meta name="Keywords" content="<?php echo htmlspecialchars($settingsArray["Keywords"]); ?>"/>
	<title>Orimari</title>
	
	<link rel="shortcut icon" href="../files/orimari.ico"/>
	<link rel="stylesheet" href="../files/orimari.css" type="text/css"/>
	
	<script type="text/javascript" src="../files/jpgRotator/ufo.js"></script>
	
	<script type="text/javascript" src="../files/lightbox/prototype.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="../files/jquery.cookie.js"></script>
	<script type="text/javascript">
		jQuery.noConflict();
		jQuery(function() {
			if (jQuery.cookie('cookie-policy') == 'true') {
				jQuery('#cookies-policy').hide();
			}
			
			jQuery('#cookies-policy .close').on('click', function (e) {
				e.preventDefault();
				jQuery('#cookies-policy').hide();
				jQuery.cookie('cookie-policy', 'true', {expires: 365, path: '/'});
			});
		});
	</script>
	<script type="text/javascript" src="../files/lightbox/scriptaculous.js?load=effects"></script>
	<script type="text/javascript" src="<?php echo ($language=="pl")?"../files/lightbox/lightbox_pl.js":"../files/lightbox/lightbox_en.js" ?>"></script>
	<link rel="stylesheet" href="../files/lightbox/lightbox.css" type="text/css" media="screen" />
</head>

<body>
<div class="mainDiv">
	<table class="mainTable">
		<tr id="cookies-policy">
			<td colspan="2">
				<?php if ($language == 'pl') { ?>
					<p>Wykorzystujemy pliki cookies do zapamiętania ustawień i preferencji użytkownika oraz w celu korzystania z usługi <a href="http://www.google.com/analytics/" target="_blank">Google Analytics</a>. Jeśli się nie zgadzasz, możesz w swojej przeglądarce wyłączyć zgodę na ich przechowywanie.</p>
					<p style="text-align:right"><a href="#" class="close">Zamknij</a></p>
				<?php } else { ?>
					<p>Cookies are used</p>
				<?php } ?>
			</td>
		</td>
		<tr class="navRow">
			<td class="navCellLeft"><a href="index.php"><img src="../files/nav/homepage.png" alt="Home" title="Home"/></a></td>
			<td class="navCellRight"><a href="../pl/<?php echo file_exists("../pl/$filename")?$filename:"index.php"; ?>"><img src="../files/nav/polski.png" alt="polski" title="polski"/></a><img src="../files/nav/seperator.png" alt=" | "/><a href="../en/<?php echo file_exists("../en/$filename")?$filename:"index.php"; ?>"><img src="../files/nav/english.png" alt="english" title="english"/></a></td>
		</tr>
		<tr>
			<td class="rotCellLeft"><img src="../files/orimariLogoBig.jpg" alt="ORIMARI" /></td>
			<td class="rotCellRight" <?php if($spotName && $runSpot){ echo 'style="padding-left:208px;"'; } /*CHANGE THIS HACK*/ ?>>
<?php if(!$spotName) //default action
{
?>
				<div id="jpgrotator"></div>
				<script type="text/javascript">
					var jpgrotatorDiv = document.getElementById("jpgrotator");
					jpgrotatorDiv.innerHTML='<img src="../files/orimariRotator.jpg" alt=""/>';
					var FO = { movie:"../files/jpgRotator/jpgrotator.swf?file=../files/jpgRotator/jpgrotator.xml",width:"710",height:"222",majorversion:"7",build:"0",bgcolor:"#C7C7C7" };
					UFO.create(FO, "jpgrotator");
				</script>
				<noscript>
					<object type="application/x-shockwave-flash" width="710" height="221" data="../files/jpgRotator/jpgrotator.swf?file=../files/jpgRotator/jpgrotator.xml" >
	  					<param name="movie" value="../files/jpgRotator/jpgrotator.swf?file=../files/jpgRotator/jpgrotator.xml" />
	  					<param name="bgcolor" value="#C7C7C7" />
	  				</object>
				</noscript>
<?php
}
else
{
	echo '<div id="spotbar"><img src="../movies/'.$spotName.'.jpg" alt="" /></div>'."\n";
	if($runSpot)
	{
		echo '		<script type="text/javascript" src="../files/flashPlayer/swfobject.js"></script>
					<script type="text/javascript">
						//var s1 = new SWFObject("../files/flashPlayer/player.swf","ply","710","222","9","#ffffff"); //OLD VERSION CHANGE!
						var s1 = new SWFObject("../files/flashPlayer/player.swf","ply","296","222","9","#ffffff");
						s1.addParam("allowfullscreen","false");
						s1.addParam("allowscriptaccess","always");
						
			                        s1.addParam("bgcolor","#ffffff");
						s1.addParam("screencolor","#ffffff");
						s1.addParam("lightcolor","#ffffff");
						s1.addParam("frontcolor","#ffffff");
						s1.addParam("align","center");
						
						
						s1.addParam("flashvars","file=../../movies/'.$spotName.'.flv&image=../movies/'.$spotName.'.jpg&controlbar=none&autostart=true&icons=false&displayclick=none");
						s1.write("spotbar");
					</script>
					';
	}
}
?>
			</td>
		</tr>
		<tr class="menuSeperator"><td colspan="2"></td></tr>
		<tr>
			<td colspan="2">
				<div class="mainMenu">
				<?php showMainMenu($mainMenu,$filename); ?>
				</div>
			</td>
		</tr>
		<tr class="horSeperator"></tr>		
		
		<tr>
			<td class="mainContent" colspan="2">
				<!--HEADER ENDS HERE-->