<?php //Info-Main: Part of Orimari.com Must be stored in $serverName/files folder (2008.09.25:mieszko4GmailCom) ?>
<?php
//script finds the most close language and redirects it to: >>>$foundLanguage/index.php<<< through Location header
$availableLanguages = array("en","pl");
$defaultLanguage = "en";

$foundLanguage=$defaultLanguage;
if( isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]) )
{
	$acceptLanguages = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
	$acceptLanguages = str_replace(' ','',$acceptLanguages);
	$acceptLanguages = explode(",", $acceptLanguages);
	
	foreach ($acceptLanguages as $languageAc)
	{
		$languageAc=substr($languageAc,0,2); //get only major
		
		foreach($availableLanguages as $languageAv)
		{
			if($languageAv==$languageAc)
			{
				$foundLanguage=$languageAc;
				break(2);
			}
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>404 Not found - Orimari</title>

<link rel="shortcut icon" href="http://www.orimari.com/files/orimari.ico"/>

<style type="text/css">
	body{background-color:#999999;text-align:center;font-size:20px;font-family:monospace;color:#ffffff;padding-top:60px}
	img{border-style:none}
	a{color:#ffaa00}
</style>
</head>

<body>
	<img src="http://www.orimari.com/files/orimariMain.jpg" alt="ORIMARI" style="height:200px"/>
<?php
if($foundLanguage=="pl")
{
?>
	<p style="margin-top:60px">Strona nie została odnaleziona!</p>
	<p>Przejdź na stronę startową <a href="http://www.orimari.com/pl/">www.orimari.com</a>.</p>
<?php
}
else //english default
{
?>
	<p style="margin-top:60px">The requested URL was not found!</p>
	<p>Go to the initial page <a href="http://www.orimari.com/en/">www.orimari.com</a>.</p>
<?php
}
?>
</body>
</html>
