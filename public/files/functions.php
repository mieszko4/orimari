<?php
//Info-Main: Part of Orimari.com page. Must be stored in $serverName/files folder
//Info: This is a set of common functions used for displaying menus, submenus, galleries, and movies
//Author: Mieszko 4 <mieszko4GmailCom>
//CreationDate: 2008.10.02
//Update: 2008.10.22 htmlspecialchars() removed for menuItems!
//Update: 2009.03.05 movie info added and movie player size bug corrected


//Echoes $subMenu sql resource (pageID, externalLink, and itemName, orderNumber required) //INCLUDES subsubmenu
function showSubMenu($subMenu,$filename,$id,$subid,$isCenter=false,$separator="&nbsp;&nbsp;|&nbsp;&nbsp;")
{
	$subHolder=null;
	$rowNumber=@mysql_num_rows($subMenu);
	
	echo ($rowNumber>0)?($isCenter?'<div id="subMenu" style="text-align:center">':'<div id="subMenu">')."\n":'';

	$i=1;
	$submenuEcho="";
	while($row=@mysql_fetch_array($subMenu,MYSQL_ASSOC))
	{
		$itemPath=$row["pageID"]?"$filename?id={$row["orderNumber"]}":$row["externalLink"]; //choose filename?id=x or external link
		
		if($i==$id || ($row["pageID"]==$row["pageIDHolder"] && $id==0))
		{
			if($row["pageID"]!=$row["pageIDHolder"])
			{	
				$subHolder=$row["pageID"];
			
				$query="SELECT pageIDLink AS pageID, externalLink, itemName, orderNumber FROM submenupages WHERE pageIDHolder={$subHolder} ORDER BY orderNumber";
				$subSubMenu=@mysql_query($query);
				$subRowNumber=@mysql_num_rows($subSubMenu);
			}
			
			if($subRowNumber>0)
			{
				$theActive='<a class="pageOn" href="'.htmlspecialchars($itemPath).'">'.$row["itemName"].'</a>';
				$submenuEcho.=$theActive;
			}
			else
			{
				$submenuEcho.= '<a class="pageOn">'.$row["itemName"].'</a>';
			}
		}
		else
		{
			$submenuEcho.= '<a href="'.htmlspecialchars($itemPath).'">'.$row["itemName"].'</a>';
		}
		
		if($i<$rowNumber) //seperators only in between
		{
			$submenuEcho.= $separator;
		}
		
		$i++;
	}
	
	if($subRowNumber>0) //echo only the active one
	{
		echo $theActive;
	}
	else //echo all
	{
		echo $submenuEcho;
	}
	
	
	if($rowNumber>0)
	{
		//subSubMenu
		if($subRowNumber>0)
		{
			echo "\n<br/><br/>\n".'<div style="font-size:10px;margin-left:30px;margin-top:-10px;margin-bottom:10px">'."\n";

			$i=1;
			while($row=@mysql_fetch_array($subSubMenu,MYSQL_ASSOC))
			{
				$itemPath=$row["pageID"]?"$filename?id={$id}&subid={$row["orderNumber"]}":$row["externalLink"]; //choose filename?id=x&subid=y or external link
				
				if($i==$subid || ($row["pageID"]==$subHolder && $subid==0))
				{					
					echo '<a class="pageOn">'.$row["itemName"].'</a>';
				}
				else
				{
					echo '<a href="'.htmlspecialchars($itemPath).'">'.$row["itemName"].'</a>';
				}
				
				if($i<$subRowNumber) //seperators only in between
				{
					echo $separator;
				}
				
				$i++;
			}
			
			echo "\n</div>\n";
		}
		
		
		echo "\n</div>\n";
	}
}

//Echoes $mainMenu sql resource (pageID, externalLink, filename, and itemName required)
function showMainMenu($mainMenu,$filename)
{
	echo '<table class="mainMenu">'."\n";
	echo '<tr>'."\n";

	$rowNumber=@mysql_num_rows($mainMenu);
	$i=1;
	while($row=@mysql_fetch_array($mainMenu,MYSQL_ASSOC))
	{
		$itemPath=$row["pageID"]?"../{$row["language"]}/{$row["filename"]}":$row["externalLink"]; //choose filename or external link
		
		if($filename!=basename($itemPath))
		{
			echo '<td class="mainMenuOff"><a href="'.htmlspecialchars($itemPath).'">'.$row["itemName"].'</a></td>'."\n";
		}
		else
		{
			echo '<td class="mainMenuOn"><a href="'.htmlspecialchars($itemPath).'">'.$row["itemName"].'</a></td>'."\n";
		}
		
		
		if($i<$rowNumber) //seperators only in between
		{
			echo '<td class="mainMenuSeperator"></td>'."\n";
		}
		
		$i++;
	}
	
	echo '</tr>'."\n";
	echo '</table>'."\n";
}

//Echoes $bottomMenu sql resource (pageID, externalLink, filename, and itemName required)
function showBottomMenu($bottomMenu)
{
	while($row=@mysql_fetch_array($bottomMenu,MYSQL_ASSOC))
	{
		$itemPath=$row["pageID"]?"../{$row["language"]}/{$row["filename"]}":$row["externalLink"]; //choose filename or external link
		
		echo '<a href="'.htmlspecialchars($itemPath).'"><img src="../files/square.png" alt="&#9679;"/>&nbsp;'.$row["itemName"].'</a>&nbsp;'."\n";
	}
}

//Echoes $serverName/movies/$movieName.flv
//$movieName is obligatory ($serverName/movies/$galleryDir), $language is a 2char string defining language information (pl, en)
function showFlvMovie($movieName,$language=false,$isHr=true,$width="",$height="",$backgroundColor="#ffffff")
{
	$moviesPath="../movies"; //movies path
	$separator=">";
	
	$containerID="movieContainer".microtime(); //for unique id
	$containerID=strtr($containerID," .","__");
	
	if(is_file("$moviesPath/$movieName.flv"))
	{
		//get info file
		if(is_file("$moviesPath/$movieName.info"))
		{
			$lines=file("$moviesPath/$movieName.info");
			$movieInfo=null;
			foreach($lines as $line)
			{
				list($property,$value)=@split($separator,rtrim($line));
				$movieInfo[$property]=htmlspecialchars($value);
			}
			
			$widthF=$movieInfo["width"];
			$heightF=$movieInfo["height"];
			$info=$movieInfo["$language-info"];
		}
		
		$width=$width?$width:$widthF;
		$width=$width?$width:330;
		$height=$height?$height:$heightF;
		$height=$height?$height:300;
		
		if($isHr) echo '<hr class="horizontal"/>'."\n";
		
		echo '<div class="movieContent">'."\n";
		if($language=='pl')
		{
			echo '<div id="'.$containerID.'">W celu obejrzenia filmu potrzebne jest w��czenie j�zyka <a>JavaScript</a> oraz wtyczki <a href="http://www.macromedia.com/go/getflashplayer">Adobe Flash Player</a>.</div>'."\n";
		}
		else //en
		{
			echo '<div id="'.$containerID.'">In order to see the movie you have to turn on <a>JavaScript</a> and <a href="http://www.macromedia.com/go/getflashplayer">Adobe Flash Player</a>.</div>'."\n";
		}
		
		echo '
			<script type="text/javascript" src="../files/flashPlayer/swfobject.js"></script>
		
			<script type="text/javascript">
				var s1 = new SWFObject("../files/flashPlayer/player.swf","ply","'.$width.'","'.$height.'","9","#FFFFFF");
				s1.addParam("allowfullscreen","true");
				s1.addParam("allowscriptaccess","always");
		';
				if(is_file("$moviesPath/$movieName.jpg"))
				{
					echo 's1.addParam("flashvars","file=../../movies/'.$movieName.'.flv&image=../movies/'.$movieName.'.jpg");';
				}
				else
				{
					echo 's1.addParam("flashvars","file=../../movies/'.$movieName.'.flv");';
				}
		echo 's1.write("'.$containerID.'");
				</script>
		';
		echo $info?'<p>'.$info.'</p>'."\n":'';
		echo '</div>
		';
	}
}

//Creates a gallery for lighbox
//$galleryDir is obligatory ($serverName/photos/$galleryDir), $language is a 2char string defining language information (pl, en)
function showGallery($galleryDir,$language='en',$isHr=true)
{
	$galleriesPath="../photos"; //galleries directory
	$separator=">";
	
	$galleryDir="$galleriesPath/$galleryDir";
	$theFiles=null;
	if (is_dir($galleryDir))
	{
		$entries = scandir($galleryDir);
		foreach ($entries as $entry)
		{
			if ($entry!='.' && $entry!='..' && $entry!='')
			{
				$theFiles[]= $entry;
			}
		}
		$theFiles = preg_grep("/jpg$/",$theFiles);
		if(count($theFiles)>0)
		{
			//get info $galleryName and $galleryInfo
			if(is_file("$galleryDir/gallery.info"))
			{
				$lines=file("$galleryDir/gallery.info");
				$galleryInfo=null;
				foreach($lines as $lineNum => $line)
				{
					list($property,$value)=split($separator,rtrim($line));
					$galleryInfo[$property]=htmlspecialchars($value);
				}
				
				$gName=$galleryInfo["$language-name"];
				$gInfo=$galleryInfo["$language-info"];
			}
			
			
			if($isHr) echo "\n".'<hr class="horizontal"/>'."\n";
			
			echo '<div class="photosContent"><p style="text-align:left">'."\n";
			foreach($theFiles as $file)
			{
				echo '<a href="'.$galleryDir.'/'.$file.'" rel="lightbox['.$galleryDir.']" title="'.$gName.'"><img src="'."$galleryDir/mini/$file".'" alt="'.$file.'"/></a>'."\n";
			}			
			echo '</p>'."\n";
			
			if($gInfo) echo '<p style="margin-left:50px">'.$gInfo.'</p>'."\n";
			
			echo '<div/>'."\n";
		}
	}
}
?>